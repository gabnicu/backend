package com.radcom.qinsight.services.definitions.discovery;

public class ServiceConstants {

    public static final String SERVICE_USERS = "users";
    public static final String SERVICE_NETWORK_ELEMENTS = "networkElements";
    public static final String SERVICE_NETWORK_ELEMENTS_DB_ENTRIES = "networkElements/dbEntries";
    public static final String SERVICE_DASHBOARD_TEMPLATES = "dashboardTemplates";
    public static final String SERVICE_DNS_ERROR_CODES = "dns/errorCodes";
    public static final String SERVICE_DNS_RESPONSE_TIME = "dns/responseTime";
    public static final String SERVICE_ADVANCED_FILTERS = "advfilt";
    public static final String SERVICE_DASHLETS = "dashlets";


    public static final String SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION = "metrics/diameter/errorCodeDistribution";
    public static final String SERVICE_METRICS_DIAMETER_RETRANSMISSION_RATIO = "metrics/diameter/retransmissionRatio";
    public static final String SERVICE_METRICS_DIAMETER_AVERAGE_RESPONSE_TIME = "metrics/diameter/averageResponseTime";
    public static final String SERVICE_METRICS_DIAMETER_PEAK_RESPONSE_TIME = "metrics/diameter/peakResponseTime";
    public static final String SERVICE_METRICS_DIAMETER_RESPONSE_COUNT_BY_TYPE = "metrics/diameter/responseCountByType";
    public static final String SERVICE_METRICS_DIAMETER_REQUEST_COUNT = "metrics/diameter/requestCount";

    public static final String SERVICE_METRICS_TABLE_SOURCE = "metrics/diameter/table/source";
    public static final String SERVICE_METRICS_TABLE_DESTINATION = "metrics/diameter/table/destination";
    public static final String SERVICE_METRICS_TABLE_PAIR = "metrics/diameter/table/pair";

    public static final String SERVICE_METRICS_TABLE_WORST_SOURCE = "metrics/diameter/table/worst/source";
    public static final String SERVICE_METRICS_TABLE_WORST_DESTINATION = "metrics/diameter/table/worst/destination";
    public static final String SERVICE_METRICS_TABLE_WORST_PAIR = "metrics/diameter/table/worst/pair";


    public static final String ENV_VAR_SERVICE_USERS = "ENV_VAR_SERVICE_USERS";
    public static final String ENV_VAR_SERVICE_NETWORK_ELEMENTS = "ENV_VAR_SERVICE_NETWORK_ELEMENTS";
    public static final String ENV_VAR_SERVICE_NETWORK_ELEMENTS_DB_ENTRIES = "ENV_VAR_SERVICE_NETWORK_ELEMENTS_DB_ENTRIES";
    public static final String ENV_VAR_SERVICE_DASHBOARD_TEMPLATES = "ENV_VAR_SERVICE_DASHBOARD_TEMPLATES";
    public static final String ENV_VAR_SERVICE_DNS_ERROR_CODES = "ENV_VAR_SERVICE_DNS_ERROR_CODES";
    public static final String ENV_VAR_SERVICE_DNS_RESPONSE_TIME = "ENV_VAR_SERVICE_DNS_RESPONSE_TIME";
    public static final String ENV_VAR_SERVICE_ADVANCED_FILTERS = "ENV_VAR_SERVICE_ADVANCED_FILTERS";
    public static final String ENV_VAR_SERVICE_DASHLETS = "ENV_VAR_SERVICE_DASHLETS";

    public static final String ENV_VAR_SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION = "ENV_VAR_SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION";
    public static final String ENV_VAR_SERVICE_METRICS_DIAMETER_RETRANSMISSION_RATIO = "ENV_VAR_SERVICE_METRICS_DIAMETER_RETRANSMISSION_RATIO";
    public static final String ENV_VAR_SERVICE_METRICS_DIAMETER_AVERAGE_RESPONSE_TIME = "ENV_VAR_SERVICE_METRICS_DIAMETER_AVERAGE_RESPONSE_TIME";
    public static final String ENV_VAR_SERVICE_METRICS_DIAMETER_PEAK_RESPONSE_TIME = "ENV_VAR_SERVICE_METRICS_DIAMETER_PEAK_RESPONSE_TIME";
    public static final String ENV_VAR_SERVICE_METRICS_DIAMETER_RESPONSE_COUNT_BY_TYPE = "ENV_VAR_SERVICE_METRICS_DIAMETER_RESPONSE_COUNT_BY_TYPE";
    public static final String ENV_VAR_SERVICE_METRICS_DIAMETER_REQUEST_COUNT = "ENV_VAR_SERVICE_METRICS_DIAMETER_REQUEST_COUNT";

    public static final String ENV_VAR_SERVICE_METRICS_TABLE_SOURCE = "ENV_VAR_SERVICE_METRICS_TABLE_SOURCE";
    public static final String ENV_VAR_SERVICE_METRICS_TABLE_DESTINATION = "ENV_VAR_SERVICE_METRICS_TABLE_DESTINATION";
    public static final String ENV_VAR_SERVICE_METRICS_TABLE_PAIR = "ENV_VAR_SERVICE_METRICS_TABLE_PAIR";

    public static final String ENV_VAR_SERVICE_METRICS_TABLE_WORST_SOURCE = "ENV_VAR_SERVICE_METRICS_TABLE_WORST_SOURCE";
    public static final String ENV_VAR_SERVICE_METRICS_TABLE_WORST_DESTINATION = "ENV_VAR_SERVICE_METRICS_TABLE_WORST_DESTINATION";
    public static final String ENV_VAR_SERVICE_METRICS_TABLE_WORST_PAIR = "ENV_VAR_SERVICE_METRICS_TABLE_WORST_PAIR";
}
