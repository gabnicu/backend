package com.radcom.qinsight.services.impl.env;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceConstants;
import com.radcom.qinsight.services.definitions.registration.ServiceRegistry;
import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import java.util.HashMap;
import java.util.Map;


@EnvVarServices
@ApplicationScoped
public class EnvVarRegistry implements ServiceRegistry {
	
	private static final Logger theLog = Logger.getLogger(EnvVarRegistry.class);
	
	private Map<String, String> envVarMap;

	@Override
	public void registerService(String name, String uri) {
		// Don't do anything, there is no service registry, we are based on environment variables defining the addresses on which we can find 
		// various micro services		
	}

	@Override
	public void unregisterService(String name, String uri) {
		// Don't do anything, there is no service registry, we are based on environment variables defining the addresses on which we can find 
		// various micro services
	}

	@Override
	public String discoverServiceURI(String name) {
		if ( envVarMap == null)	{
			initializeMap();
		}
		String envVar	=	envVarMap.get( name);
		if ( envVar == null)		{
			theLog.error("discoverServiceURI Null value in map: " + name);
			return null;			
		}
		String sz	=	System.getenv(envVar);
		if ( sz == null)	{
			theLog.info("GetEnv returned NULL for environment variable: " + envVar);
		}
		return sz;
	}
	
	private void initializeMap() {
		envVarMap	=	new HashMap<>();
		envVarMap.put(ServiceConstants.SERVICE_USERS, 					ServiceConstants.ENV_VAR_SERVICE_USERS);
		envVarMap.put(ServiceConstants.SERVICE_DASHBOARD_TEMPLATES, 	ServiceConstants.ENV_VAR_SERVICE_DASHBOARD_TEMPLATES);
		envVarMap.put(ServiceConstants.SERVICE_DNS_ERROR_CODES, 		ServiceConstants.ENV_VAR_SERVICE_DNS_ERROR_CODES);
		envVarMap.put(ServiceConstants.SERVICE_DNS_RESPONSE_TIME, 		ServiceConstants.ENV_VAR_SERVICE_DNS_RESPONSE_TIME);
		envVarMap.put(ServiceConstants.SERVICE_ADVANCED_FILTERS, 		ServiceConstants.ENV_VAR_SERVICE_ADVANCED_FILTERS);
		envVarMap.put(ServiceConstants.SERVICE_DASHLETS, 		ServiceConstants.ENV_VAR_SERVICE_DASHLETS);

		envVarMap.put(ServiceConstants.SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION, 	ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION);

		envVarMap.put(ServiceConstants.SERVICE_METRICS_TABLE_SOURCE, 		ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_SOURCE);
		envVarMap.put(ServiceConstants.SERVICE_METRICS_TABLE_DESTINATION, 		ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_DESTINATION);
		envVarMap.put(ServiceConstants.SERVICE_METRICS_TABLE_PAIR, 		ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_PAIR);

		envVarMap.put(ServiceConstants.SERVICE_METRICS_TABLE_WORST_SOURCE, 		ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_WORST_SOURCE);
		envVarMap.put(ServiceConstants.SERVICE_METRICS_TABLE_WORST_DESTINATION, 		ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_WORST_DESTINATION);
		envVarMap.put(ServiceConstants.SERVICE_METRICS_TABLE_WORST_PAIR, 		ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_WORST_PAIR);

	}

}
