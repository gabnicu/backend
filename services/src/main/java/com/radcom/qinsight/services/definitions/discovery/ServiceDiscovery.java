package com.radcom.qinsight.services.definitions.discovery;

import javax.ws.rs.client.WebTarget;

public interface ServiceDiscovery {

    WebTarget getUserService(String pathParam);

    WebTarget getNetworkElementsService();

    WebTarget getNetworkElementsDbEntriesService();

    WebTarget getDnsErrorCodesService();

    WebTarget getDnsResponseTimeService();

    WebTarget getDashboardTemplatesService();

    WebTarget getAdvancedFiltersService();

    WebTarget getAllAdvancedFiltersService(String filter);

    WebTarget getMetricsDiameterErrorCodeDistributionService();

    WebTarget getMetricsDiameterRetransmissionRatioService();

    WebTarget getMetricsDiameterAverageResponseTimeService();

    WebTarget getMetricsDiameterPeakResponseTimeService();

    WebTarget getMetricsDiameterResponseCountByTypeService();

    WebTarget getMetricsDiameterRequestCountService();


    WebTarget getMetricsDiameterTableSourceService();

    WebTarget getMetricsDiameterTableDestinationService();

    WebTarget getMetricsDiameterTablePairService();

    WebTarget getMetricsDiameterTableWorstSourceService();

    WebTarget getMetricsDiameterTableWorstDestinationService();

    WebTarget getMetricsDiameterTableWorstPairService();


    WebTarget getDashletsService();

}
