package com.radcom.qinsight.services.impl;

import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public abstract class AbsServiceDiscovery implements ServiceDiscovery {

    private WebTarget userService;
    private WebTarget dashletsService;
    private WebTarget networkElementsService;
    private WebTarget networkElementsDbEntriesService;
    private WebTarget dashboardTemplatesService;
    private WebTarget dnsErrorCodesService;
    private WebTarget dnsResponseTimeService;
    private WebTarget advancedFiltersService;
    private WebTarget allAdvancedFiltersService;

    private WebTarget metricsDiameterErrorCodeDistributionService;
    private WebTarget metricsDiameterRetransmissionRatioService;
    private WebTarget metricsDiameterAverageResponseTimeService;
    private WebTarget metricsDiameterPeakResponseTimeService;
    private WebTarget metricsDiameterResponseCountByTypeService;
    private WebTarget metricsDiameterRequesteCountService;


    public abstract String getUserServiceURI();

    public abstract String getDashletsServiceURI();

    public abstract String getNetworkElementsServiceURI();

    public abstract String getNetworkElementsDbEntriesServiceURI();

    public abstract String getDashboardTemplatesServiceURI();

    public abstract String getDnsErrorCodesServiceURI();

    public abstract String getDnsResponseTimeURI();

    public abstract String getAdvancedFiltersURI();

    public abstract String getAllAdvancedFiltersURI();

    public abstract String getMetricsDiameterErrorCodeDistributionURI();

    public abstract String getMetricsDiameterRetransmissionRatioURI();

    public abstract String getMetricsDiameterAverageResponseTimeURI();

    public abstract String getMetricsDiameterPeakResponseTimeURI();

    public abstract String getMetricsDiameterResponseCountByTypeURI();

    public abstract String getMetricsDiameterRequestCountURI();


    @Override
    public WebTarget getUserService(String pathParam) {

        if (pathParam == null) {
            userService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getUserServiceURI())).path("/user/resources/user").build());
        } else {
            userService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getUserServiceURI())).path("/user/resources/user/" + pathParam).build());
        }

        return userService;
    }

    @Override
    public WebTarget getDashletsService() {
        if (null == dashletsService) {
            dashletsService = ClientBuilder.newClient().target(UriBuilder
                    .fromUri(URI.create(getDnsErrorCodesServiceURI())).path("/dashlets/resources/dashlets").build());
        }

        return dashletsService;
    }

    @Override
    public WebTarget getDnsErrorCodesService() {
        if (null == dnsErrorCodesService) {
            dnsErrorCodesService = ClientBuilder.newClient().target(UriBuilder
                    .fromUri(URI.create(getDnsErrorCodesServiceURI())).path("/dns/resources/dns/errorCodes").build());
        }

        return dnsErrorCodesService;
    }

    @Override
    public WebTarget getDnsResponseTimeService() {
        if (null == dnsResponseTimeService) {
            dnsResponseTimeService = ClientBuilder.newClient().target(UriBuilder
                    .fromUri(URI.create(getDnsErrorCodesServiceURI())).path("/dns/resources/dns/responseTime").build());
        }

        return dnsResponseTimeService;
    }

    @Override
    public WebTarget getDashboardTemplatesService() {
        if (null == dashboardTemplatesService) {
            dashboardTemplatesService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/dashboardTemplate/resources/dashboardTemplates").build());
        }
        return dashboardTemplatesService;
    }


    @Override
    public WebTarget getNetworkElementsService() {
        if (null == networkElementsService) {
            networkElementsService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/nes/resources/nes").build());
        }
        return networkElementsService;
    }

    @Override
    public WebTarget getNetworkElementsDbEntriesService() {
        if (null == networkElementsDbEntriesService) {
            networkElementsDbEntriesService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/nes/resources/nes/dbEntries").build());
        }
        return networkElementsDbEntriesService;
    }

    @Override
    public WebTarget getAdvancedFiltersService() {
        if (null == advancedFiltersService) {
            advancedFiltersService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/advfilt/resources/advfilt").build());
        }
        return advancedFiltersService;
    }

    @Override
    public WebTarget getAllAdvancedFiltersService(String filter) {
        if (null == allAdvancedFiltersService) {
            allAdvancedFiltersService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/advfilt/resources/advfilt").build());
        }
        return allAdvancedFiltersService;
    }

    @Override
    public WebTarget getMetricsDiameterErrorCodeDistributionService() {
        if (null == metricsDiameterErrorCodeDistributionService) {
            metricsDiameterErrorCodeDistributionService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/metrics/resources/metrics/diameter/errorCodeDistribution").build());
        }
        return metricsDiameterErrorCodeDistributionService;
    }

    @Override
    public WebTarget getMetricsDiameterRetransmissionRatioService() {
        if (null == metricsDiameterRetransmissionRatioService) {
            metricsDiameterRetransmissionRatioService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/metrics/resources/metrics/diameter/retransmissionRatio").build());
        }
        return metricsDiameterRetransmissionRatioService;
    }

    @Override
    public WebTarget getMetricsDiameterAverageResponseTimeService() {
        if (null == metricsDiameterAverageResponseTimeService) {
            metricsDiameterAverageResponseTimeService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/metrics/resources/metrics/diameter/averageResponseTime").build());
        }
        return metricsDiameterAverageResponseTimeService;
    }

    @Override
    public WebTarget getMetricsDiameterPeakResponseTimeService() {
        if (null == metricsDiameterPeakResponseTimeService) {
            metricsDiameterPeakResponseTimeService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/metrics/resources/metrics/diameter/peakResponseTime").build());
        }
        return metricsDiameterPeakResponseTimeService;
    }

    @Override
    public WebTarget getMetricsDiameterResponseCountByTypeService() {
        if (null == metricsDiameterResponseCountByTypeService) {
            metricsDiameterResponseCountByTypeService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/metrics/resources/metrics/diameter/responseCountByType").build());
        }
        return metricsDiameterResponseCountByTypeService;
    }

    @Override
    public WebTarget getMetricsDiameterRequestCountService() {
        if (null == metricsDiameterRequesteCountService) {
            metricsDiameterRequesteCountService = ClientBuilder.newClient()
                    .target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/metrics/resources/metrics/diameter/requestCount").build());
        }
        return metricsDiameterRequesteCountService;
    }


}
