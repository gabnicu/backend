package com.radcom.qinsight.services.impl.env;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceConstants;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import com.radcom.qinsight.utils.WildFlyUtil;
import org.apache.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

@EnvVarServices
@ApplicationScoped
public class EnvVarDiscovery implements ServiceDiscovery {
    private static final Logger theLog = Logger.getLogger(EnvVarDiscovery.class);

    private WebTarget usersService;
    private WebTarget networkElementsService;
    private WebTarget networkElementsDbEntriesService;
    private WebTarget dashboardTemplatesService;
    private WebTarget dnsErrorCodesService;
    private WebTarget dnsResponseTimeService;
    private WebTarget advancedFiltersService;
    private WebTarget advancedFiltersServiceVlanId;
    private WebTarget advancedFiltersServiceProcedureSubtype;
    private WebTarget advancedFiltersServiceApplicationId;
    private WebTarget advancedFiltersServiceTransportLayerProtocol;
    private WebTarget advancedFiltersServiceReleaseType;
    private WebTarget advancedFiltersServiceCause;
    private WebTarget dashletsService;

    private WebTarget metricsDiameterErrorCodeDistributionService;
    private WebTarget metricsDiameterRetransmissionRatioService;
    private WebTarget metricsDiameterAverageResponseTimeService;
    private WebTarget metricsDiameterPeakResponseTimeService;
    private WebTarget metricsDiameterResponseCountByTypeService;
    private WebTarget metricsDiameterRequesteCountService;

    private WebTarget metricsDiameterTableSourceService;
    private WebTarget metricsDiameterTableDestinationService;
    private WebTarget metricsDiameterTablePairService;

    private WebTarget metricsDiameterTableWorstSourceService;
    private WebTarget metricsDiameterTableWorstDestinationService;
    private WebTarget metricsDiameterTableWorstPairService;

    private int roundRobinUsers = 0;
    private int roundRobinDashboardTemplates = 0;

    @Inject
    @EnvVarServices
    private EnvVarRegistry serviceRegistry;

    @EJB
    private WildFlyUtil util;

    @Override
    public WebTarget getUserService(String pathParam) {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.SERVICE_USERS);
        if (sz == null) {
            theLog.error("getUserService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
            theLog.error("getUserService() sz=" + sz);
        }

        String[] arrIps = parseEnvVar(sz);
        if (arrIps.length > 1) {
            if (roundRobinUsers >= arrIps.length)
                roundRobinUsers = 0;
            sz = "http://" + arrIps[roundRobinUsers];
            roundRobinUsers++;
        }

        // if ( usersService == null) {
        // usersService = getService( sz, "/users/resources/users");
        // }
        if (pathParam == null) {
            usersService = getService(sz, "/users/resources/users");
        } else {
            usersService = getService(sz, "/users/resources/users/" + pathParam);
        }
        return usersService;
    }

    @Override
    public WebTarget getDnsErrorCodesService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public WebTarget getDnsResponseTimeService() {
        // TODO Auto-generated method stub
        return null;
    }

    private WebTarget getService(String location, String path) {
//		theLog.error("EnvVarDiscovery::getService() location=" + location + ", path=" + path);
//		WebTarget webTarget = ClientBuilder.newClient()
//				.target(UriBuilder.fromUri(URI.create(location)).path(path).build());
//
//		return webTarget;
        theLog.error("EnvVarDiscovery::getService() location=" + location + ", path=" + path);
        ResteasyClientBuilder restEasyClientBuilder = new ResteasyClientBuilder();
        restEasyClientBuilder = restEasyClientBuilder.connectionPoolSize(20);
        ResteasyClient clnt = restEasyClientBuilder.build();
        WebTarget webTarget = clnt.target(UriBuilder.fromUri(URI.create(location)).path(path).build());
        return webTarget;
    }

    @Override
    public WebTarget getDashboardTemplatesService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.SERVICE_DASHBOARD_TEMPLATES);
        if (sz == null) {
            theLog.error(
                    "getDashboardTemplatesService() NULL from environment variable, getting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        String[] arrIps = parseEnvVar(sz);
        if (arrIps.length > 1) {
            if (roundRobinDashboardTemplates >= arrIps.length)
                roundRobinDashboardTemplates = 0;
            sz = "http://" + arrIps[roundRobinDashboardTemplates];
            roundRobinDashboardTemplates++;
        }

        if (dashboardTemplatesService == null) {
            dashboardTemplatesService = getService(sz, "/dashboardTemplate/resources/dashboardTemplates");
        }
        return dashboardTemplatesService;
    }

    @Override
    public WebTarget getNetworkElementsService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.SERVICE_NETWORK_ELEMENTS);
        if (sz == null) {
            theLog.error("getNetworkElementsService() NULL from environment variable, getting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (networkElementsService == null) {
            networkElementsService = getService(sz, "/nes/resources/nes");
        }
        return networkElementsService;
    }

    @Override
    public WebTarget getNetworkElementsDbEntriesService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.SERVICE_NETWORK_ELEMENTS_DB_ENTRIES);
        if (sz == null) {
            theLog.error(
                    "getNetworkElementsDbEntriesService() NULL from environment variable, getting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (networkElementsDbEntriesService == null) {
            networkElementsDbEntriesService = getService(sz, "/nes/resources/nes/dbEntries");
        }
        return networkElementsDbEntriesService;
    }

    @Override
    public WebTarget getAdvancedFiltersService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_ADVANCED_FILTERS);
        if (sz == null) {
            theLog.error("getAdvancedFiltersService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }
        if (advancedFiltersService == null) {
            advancedFiltersService = getService(sz, "/advfilt/resources/advfilt/procType");
        }
        return advancedFiltersService;
    }

    @Override
    public WebTarget getAllAdvancedFiltersService(String filter) {
        theLog.error("filter: " + filter);
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_ADVANCED_FILTERS);
        if (sz == null) {
            theLog.error(
                    "getAllAdvancedFiltersService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (filter.equals("procSubtype")) {
            if (advancedFiltersServiceProcedureSubtype == null) {
                advancedFiltersServiceProcedureSubtype = getService(sz, "/advfilt/resources/advfilt/procSubtype");
            }
            return advancedFiltersServiceProcedureSubtype;
        }
        if (filter.equals("vlanId")) {
            if (advancedFiltersServiceVlanId == null) {
                advancedFiltersServiceVlanId = getService(sz, "/advfilt/resources/advfilt/vlanId");
            }
            return advancedFiltersServiceVlanId;
        }
        if (filter.equals("applicationId")) {
            if (advancedFiltersServiceApplicationId == null) {
                advancedFiltersServiceApplicationId = getService(sz, "/advfilt/resources/advfilt/applicationId");
            }
            return advancedFiltersServiceApplicationId;
        }
        if (filter.equals("transportLayerProtocol")) {
            if (advancedFiltersServiceTransportLayerProtocol == null) {
                advancedFiltersServiceTransportLayerProtocol = getService(sz,
                        "/advfilt/resources/advfilt/transportLayerProtocol");
            }
            return advancedFiltersServiceTransportLayerProtocol;
        }
        if (filter.equals("releaseType")) {
            if (advancedFiltersServiceReleaseType == null) {
                advancedFiltersServiceReleaseType = getService(sz, "/advfilt/resources/advfilt/releaseType");
            }
            return advancedFiltersServiceReleaseType;
        }
        if (filter.equals("cause")) {
            if (advancedFiltersServiceCause == null) {
                advancedFiltersServiceCause = getService(sz, "/advfilt/resources/advfilt/cause");
            }
            return advancedFiltersServiceCause;
        }
        return null;
    }

    @Override
    public WebTarget getDashletsService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.SERVICE_DASHLETS);
        if (sz == null) {
            theLog.error("getDashletsService() NULL from environment variable, getting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (dashletsService == null) {
            dashletsService = getService(sz, "/dashlets/resources/dashlets");
        }
        return dashletsService;
    }

    @Override
    public WebTarget getMetricsDiameterErrorCodeDistributionService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterErrorCodeDistributionService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterErrorCodeDistributionService == null) {
            metricsDiameterErrorCodeDistributionService = getService(sz,"/metrics/resources/metrics/diameter/errorCodeDistribution");
        }
        return metricsDiameterErrorCodeDistributionService;
    }

    @Override
    public WebTarget getMetricsDiameterRetransmissionRatioService() {
        String sz = serviceRegistry
                .discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_RETRANSMISSION_RATIO);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterRetransmissionRatioService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterRetransmissionRatioService == null) {
            metricsDiameterRetransmissionRatioService = getService(sz,
                    "/metrics/resources/metrics/diameter/retransmissionRatio");
        }
        return metricsDiameterRetransmissionRatioService;
    }

    @Override
    public WebTarget getMetricsDiameterAverageResponseTimeService() {
        String sz = serviceRegistry
                .discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_AVERAGE_RESPONSE_TIME);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterAverageResponseTimeService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterAverageResponseTimeService == null) {
            metricsDiameterAverageResponseTimeService = getService(sz,
                    "/metrics/resources/metrics/diameter/averageResponseTime");
        }
        return metricsDiameterAverageResponseTimeService;
    }

    @Override
    public WebTarget getMetricsDiameterPeakResponseTimeService() {
        String sz = serviceRegistry
                .discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_PEAK_RESPONSE_TIME);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterPeakResponseTimeService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterPeakResponseTimeService == null) {
            metricsDiameterPeakResponseTimeService = getService(sz,
                    "/metrics/resources/metrics/diameter/peakResponseTime");
        }
        return metricsDiameterPeakResponseTimeService;
    }

    @Override
    public WebTarget getMetricsDiameterResponseCountByTypeService() {
        String sz = serviceRegistry
                .discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_RESPONSE_COUNT_BY_TYPE);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterResponseCountByTypeService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterResponseCountByTypeService == null) {
            metricsDiameterResponseCountByTypeService = getService(sz,
                    "/metrics/resources/metrics/diameter/responseCountByType");
        }
        return metricsDiameterResponseCountByTypeService;
    }

    @Override
    public WebTarget getMetricsDiameterRequestCountService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_REQUEST_COUNT);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterRequestCountService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterRequesteCountService == null) {
            metricsDiameterRequesteCountService = getService(sz, "/metrics/resources/metrics/diameter/requestCount");
        }
        return metricsDiameterRequesteCountService;
    }

    @Override
    public WebTarget getMetricsDiameterTableSourceService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_SOURCE);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterTableSourceService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterTableSourceService == null) {
            metricsDiameterTableSourceService = getService(sz, "/metrics/resources/metrics/diameter/table/source");
        }
        return metricsDiameterTableSourceService;
    }

    @Override
    public WebTarget getMetricsDiameterTableDestinationService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_DESTINATION);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterTableDestinationService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterTableDestinationService == null) {
            metricsDiameterTableDestinationService = getService(sz, "/metrics/resources/metrics/diameter/table/destination");
        }
        return metricsDiameterTableDestinationService;
    }

    @Override
    public WebTarget getMetricsDiameterTablePairService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_PAIR);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterTablePairService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterTablePairService == null) {
            metricsDiameterTablePairService = getService(sz, "/metrics/resources/metrics/diameter/table/pair");
        }
        return metricsDiameterTablePairService;
    }

    @Override
    public WebTarget getMetricsDiameterTableWorstSourceService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_WORST_SOURCE);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterTableWorstSourceService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterTableWorstSourceService == null) {
            metricsDiameterTableWorstSourceService = getService(sz, "/metrics/resources/metrics/diameter/table/worst/source");
        }
        return metricsDiameterTableWorstSourceService;
    }

    @Override
    public WebTarget getMetricsDiameterTableWorstDestinationService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_WORST_DESTINATION);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterTableWorstDestinationService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterTableWorstDestinationService == null) {
            metricsDiameterTableWorstDestinationService = getService(sz, "/metrics/resources/metrics/diameter/table/worst/destination");
        }
        return metricsDiameterTableWorstDestinationService;
    }

    @Override
    public WebTarget getMetricsDiameterTableWorstPairService() {
        String sz = serviceRegistry.discoverServiceURI(ServiceConstants.ENV_VAR_SERVICE_METRICS_TABLE_WORST_PAIR);
        if (sz == null) {
            theLog.error(
                    "getMetricsDiameterTableWorstPairService() NULL from environment variable, resorting to local IP .........");
            sz = "http://" + util.getHostName() + ":" + util.getHostPort();
        }

        if (metricsDiameterTableWorstPairService == null) {
            metricsDiameterTableWorstPairService = getService(sz, "/metrics/resources/metrics/diameter/table/worst/pair");
        }
        return metricsDiameterTableWorstPairService;
    }

    private String[] parseEnvVar(String sz) {
        return sz.split(";");
    }

}
