package com.radcom.qinsight.services.impl.consul;

import com.radcom.qinsight.services.ConsulServices;
import com.radcom.qinsight.services.definitions.registration.ServiceRegistry;
import com.radcom.qinsight.services.impl.AbsServiceDiscovery;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.WebTarget;


@ConsulServices
@ApplicationScoped
public class ConsulDiscovery extends AbsServiceDiscovery {


    private ServiceRegistry services = new ConsulRegistry();

    @Override
    public String getUserServiceURI() {
        return services.discoverServiceURI("user");
    }

    @Override
    public String getDashletsServiceURI() {
        return services.discoverServiceURI("dashlets");
    }

    @Override
    public String getDnsErrorCodesServiceURI() {
        return services.discoverServiceURI("dns/errorCodes");
    }

    @Override
    public String getDnsResponseTimeURI() {
        return services.discoverServiceURI("dns/responseTime");
    }

    @Override
    public String getDashboardTemplatesServiceURI() {
        return services.discoverServiceURI("dashboardTemplate");
    }

    @Override
    public String getNetworkElementsServiceURI() {
        return services.discoverServiceURI("nes");
    }

    @Override
    public String getNetworkElementsDbEntriesServiceURI() {
        return services.discoverServiceURI("nes/dbEntries");
    }

    @Override
    public String getAdvancedFiltersURI() {
        return services.discoverServiceURI("advfilt");
    }

    @Override
    public String getMetricsDiameterErrorCodeDistributionURI() {
        return services.discoverServiceURI("metricsDiameterErrorCodeDistribution");
    }

    @Override
    public String getMetricsDiameterRetransmissionRatioURI() {
        return services.discoverServiceURI("metricsDiameterRetransmissionRatio");
    }

    @Override
    public String getMetricsDiameterAverageResponseTimeURI() {
        return services.discoverServiceURI("metricsDiameterAverageResponseTime");
    }

    @Override
    public String getMetricsDiameterPeakResponseTimeURI() {
        return services.discoverServiceURI("metricsDiameterPeakResponseTime");
    }

    @Override
    public String getMetricsDiameterResponseCountByTypeURI() {
        return services.discoverServiceURI("metricsDiameterResponseCountByType");
    }

    @Override
    public String getMetricsDiameterRequestCountURI() {
        return services.discoverServiceURI("metricsDiameterRequestCount");
    }

    @Override
    public WebTarget getMetricsDiameterTableSourceService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public WebTarget getMetricsDiameterTableDestinationService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public WebTarget getMetricsDiameterTablePairService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public WebTarget getMetricsDiameterTableWorstSourceService() {
        return null;
    }

    @Override
    public WebTarget getMetricsDiameterTableWorstDestinationService() {
        return null;
    }

    @Override
    public WebTarget getMetricsDiameterTableWorstPairService() {
        return null;
    }

    @Override
    public WebTarget getAllAdvancedFiltersService(String filter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getAllAdvancedFiltersURI() {
        return services.discoverServiceURI("advfilt");
    }

}
