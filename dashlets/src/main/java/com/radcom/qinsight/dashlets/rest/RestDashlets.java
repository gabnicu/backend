package com.radcom.qinsight.dashlets.rest;

import com.radcom.qinsight.dashlets.business.DashletPrototypesProvider;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Transactional
@Path("dashlets")
public class RestDashlets {

    @Inject
    private DashletPrototypesProvider prov;

    @GET
    @Produces({"application/json"})
    public Response findAll() {
        BasicDTO dto = prov.getAll();
        return Response.status(Response.Status.OK).entity(dto).build();
    }
}
