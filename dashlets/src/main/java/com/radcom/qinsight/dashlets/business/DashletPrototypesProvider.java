package com.radcom.qinsight.dashlets.business;

import com.radcom.qinsight.dashlets.model.*;
import com.radcom.qinsight.dashlets.model.accessors.DaoDashletPrototypes;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class DashletPrototypesProvider {

    private static final Logger theLog = Logger.getLogger(DashletPrototypesProvider.class);

    @Inject
    private DaoDashletPrototypes dao;

    public BasicDTO getAll() {
        List<DashletPrototype> lst = dao.getAll();

        List<CategoryDashletPrototype> lstCateg = new ArrayList<>();
        for (DashletPrototype proto : lst) {
            CategoryDashletPrototype categ = getCategoryByName(lstCateg, proto.getCategory());
            if (categ == null) {
                categ = new CategoryDashletPrototype();
                categ.setCategory(proto.getCategory());
                lstCateg.add(categ);
            }
            categ.addDashletPrototype(proto);
        }
        BasicDTO dto = new BasicDTO();
        dto.setData(lstCateg);
        return dto;
    }


    private CategoryDashletPrototype getCategoryByName(List<CategoryDashletPrototype> lstCateg, String name) {
        if (lstCateg == null)
            return null;
        for (CategoryDashletPrototype categ : lstCateg) {
            if (name.equals(categ.getCategory()))
                return categ;
        }
        return null;
    }

    @Transactional
    public void verifyAtStartUp() {
        List<DashletPrototype> lst = dao.getAll();
        theLog.info("------------------------------------------------------------------ DashletPrototypesProvider verifyAtStartUp loaded: " + lst.size());
        createNeededDashletPrototypes(lst);
//		dao.delete( DashletType.DIAMETER_AVERAGE_RESPONSE_TIME.getKey());
    }

    private void createNeededDashletPrototypes(List<DashletPrototype> lst) {
        boolean bret = listContainsDashletOfType(lst, DashletType.DIAMETER_AVERAGE_RESPONSE_TIME.getKey());
        if (!bret) {
            theLog.info("Create Diameter Average Response Time");
            DashletPrototype dashProto = createDiameterAverageResponseTime();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DNS_AVERAGE_RESPONSE_TIME.getKey()))) {
            theLog.info("Create DNS Average Response Time");
            DashletPrototype dashProto = createDnsAverageResponseTime();
            dao.save(dashProto);
        }

        if (!(listContainsDashletOfType(lst, DashletType.DIAMETER_ERROR_CODE_DISTRIBUTION.getKey()))) {
            theLog.info("Create Diameter Error Code Distrib");
            DashletPrototype dashProto = createDiameterErrorCodeDistribution();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DNS_ERROR_CODE_DISTRIBUTION.getKey()))) {
            theLog.info("Create DNS Error Code Distrib");
            DashletPrototype dashProto = createDnsErrorCodeDistribution();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DIAMETER_RESPONSE_COUNT_BY_TYPE.getKey()))) {
            theLog.info("Create DIAMETER_RESPONSE_COUNT_BY_TYPE");
            DashletPrototype dashProto = createDiameterResponseCountByType();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DNS_RESPONSE_COUNT_BY_TYPE.getKey()))) {
            theLog.info("Create DNS_RESPONSE_COUNT_BY_TYPE");
            DashletPrototype dashProto = createDnsResponseCountByType();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DIAMETER_PEAK_RESPONSE_TIME.getKey()))) {
            theLog.info("Create DIAMETER_PEAK_RESPONSE_TIME");
            DashletPrototype dashProto = createDiameterPeakResponseTime();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DNS_PEAK_RESPONSE_TIME.getKey()))) {
            theLog.info("Create DNS_PEAK_RESPONSE_TIME");
            DashletPrototype dashProto = createDnsPeakResponseTime();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DIAMETER_RETRANSMISSION_RATIO.getKey()))) {
            theLog.info("Create DIAMETER_RETRANSMISSION_RATIO");
            DashletPrototype dashProto = createDiameterRetransmissionRatio();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DNS_RETRANSMISSION_RATIO.getKey()))) {
            theLog.info("Create DNS_RETRANSMISSION_RATIO");
            DashletPrototype dashProto = createDnsRetransmissionRatio();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DIAMETER_REQUEST_COUNT.getKey()))) {
            theLog.info("Create DIAMETER_REQUEST_COUNT");
            DashletPrototype dashProto = createDiameterRequestCount();
            dao.save(dashProto);
        }
        if (!(listContainsDashletOfType(lst, DashletType.DNS_REQUEST_COUNT.getKey()))) {
            theLog.info("Create DNS_REQUEST_COUNT");
            DashletPrototype dashProto = createDnsRequestCount();
            dao.save(dashProto);
        }
    }

    private boolean listContainsDashletOfType(List<DashletPrototype> lst, String dashletId) {
        if (lst == null)
            return false;
        if (dashletId == null)
            return false;
        if (lst.size() == 0)
            return false;
        for (DashletPrototype proto : lst) {
            if (dashletId.equals(proto.getId()))
                return true;
        }
        return false;
    }

    private DashletPrototype createDiameterAverageResponseTime() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DIAMETER_AVERAGE_RESPONSE_TIME.getKey());
        dt.setName(DashletType.DIAMETER_AVERAGE_RESPONSE_TIME.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DIAMETER.getText());
        dt.setTooltipValueLabel("Average Response Time (mS)");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/diameter/avgresptime");

        addDefaultFilters(dt);

        return dt;

    }

    private DashletPrototype createDnsAverageResponseTime() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DNS_AVERAGE_RESPONSE_TIME.getKey());
        dt.setName(DashletType.DNS_AVERAGE_RESPONSE_TIME.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DNS.getText());
        dt.setTooltipValueLabel("Average Response Time (mS)");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/dns/avgresptime");

        addDefaultFilters(dt);

        return dt;
    }


    private DashletPrototype createDiameterErrorCodeDistribution() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DIAMETER_ERROR_CODE_DISTRIBUTION.getKey());
        dt.setName(DashletType.DIAMETER_ERROR_CODE_DISTRIBUTION.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DIAMETER.getText());
        dt.setTooltipValueLabel("Number of Errors");
        dt.setStackFragmentLabel("Cause");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/diameter/avgresptime");

        addDefaultFilters(dt);
        addExtraFilters(dt);
        return dt;
    }

    private DashletPrototype createDnsErrorCodeDistribution() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DNS_ERROR_CODE_DISTRIBUTION.getKey());
        dt.setName(DashletType.DNS_ERROR_CODE_DISTRIBUTION.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DNS.getText());
        dt.setTooltipValueLabel("Number of Errors");
        dt.setStackFragmentLabel("Cause");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/diameter/avgresptime");

        addDefaultFilters(dt);
        addExtraFilters(dt);
        return dt;
    }

    private DashletPrototype createDiameterPeakResponseTime() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DIAMETER_PEAK_RESPONSE_TIME.getKey());
        dt.setName(DashletType.DIAMETER_PEAK_RESPONSE_TIME.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DIAMETER.getText());
        dt.setTooltipValueLabel("Peak Response Time (mS)");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/diameter/avgresptime");

        addDefaultFilters(dt);

        return dt;
    }

    private DashletPrototype createDnsPeakResponseTime() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DNS_PEAK_RESPONSE_TIME.getKey());
        dt.setName(DashletType.DNS_PEAK_RESPONSE_TIME.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DNS.getText());
        dt.setTooltipValueLabel("Peak Response Time (mS)");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/diameter/avgresptime");

        addDefaultFilters(dt);
        return dt;
    }

    private DashletPrototype createDiameterRetransmissionRatio() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DIAMETER_RETRANSMISSION_RATIO.getKey());
        dt.setName(DashletType.DIAMETER_RETRANSMISSION_RATIO.getValue());
        dt.setDisplayValueTypeSuffix(true);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DIAMETER.getText());
        dt.setTooltipValueLabel("Retransmission Ratio");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.DOUBLE_TWO_DECIMALS);
        dt.setUrlForData("api/v0/metrics/diameter/avgresptime");

        addDefaultFilters(dt);
        return dt;
    }

    private DashletPrototype createDnsRetransmissionRatio() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DNS_RETRANSMISSION_RATIO.getKey());
        dt.setName(DashletType.DNS_RETRANSMISSION_RATIO.getValue());
        dt.setDisplayValueTypeSuffix(true);
//        dt.setValueTypeSuffix("%");
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DNS.getText());
        dt.setTooltipValueLabel("Retransmission Ratio");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.DOUBLE_TWO_DECIMALS);
        dt.setUrlForData("api/v0/metrics/diameter/avgresptime");

        addDefaultFilters(dt);
        return dt;
    }


    private DashletPrototype createDiameterResponseCountByType() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DIAMETER_RESPONSE_COUNT_BY_TYPE.getKey());
        dt.setName(DashletType.DIAMETER_RESPONSE_COUNT_BY_TYPE.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DIAMETER.getText());
        dt.setTooltipValueLabel("Response Count");
        dt.setStackFragmentLabel("Release Type");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/diameter/respcountbytype");

        addDefaultFilters(dt);
        addExtraFilters(dt);
        return dt;
    }

    private DashletPrototype createDnsResponseCountByType() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DNS_RESPONSE_COUNT_BY_TYPE.getKey());
        dt.setName(DashletType.DNS_RESPONSE_COUNT_BY_TYPE.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DNS.getText());
        dt.setTooltipValueLabel("Response Count");
        dt.setStackFragmentLabel("Release Type");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/dns/responseCountByType");

        addDefaultFilters(dt);
        addExtraFilters(dt);
        return dt;
    }


    private DashletPrototype createDiameterRequestCount() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DIAMETER_REQUEST_COUNT.getKey());
        dt.setName(DashletType.DIAMETER_REQUEST_COUNT.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DIAMETER.getText());
        dt.setTooltipValueLabel("Request Count");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/diameter/requestCount");

        addDefaultFilters(dt);
        return dt;
    }

    private DashletPrototype createDnsRequestCount() {
        DashletPrototype dt = new DashletPrototype();
        dt.setId(DashletType.DNS_REQUEST_COUNT.getKey());
        dt.setName(DashletType.DNS_REQUEST_COUNT.getValue());
        dt.setDisplayValueTypeSuffix(false);
        dt.setValueTypeSuffix("");
        dt.setCategory(DashletCategory.DNS.getText());
        dt.setTooltipValueLabel("Request Count");
        dt.setStackFragmentLabel("");
        dt.setDisplayTypeAxisY(DataDisplayType.INTEGER);
        dt.setUrlForData("api/v0/metrics/dns/requestCount");

        addDefaultFilters(dt);
        return dt;
    }

    private void addDefaultFilters(DashletPrototype dt) {
        List<String> lstFilters = new ArrayList<>();
        lstFilters.add(FilterTypes.NE_HIERARCHY.getText());
        lstFilters.add(FilterTypes.LINK.getText());
        lstFilters.add(FilterTypes.START_DATE.getText());
        lstFilters.add(FilterTypes.END_DATE.getText());
        dt.setFilters(lstFilters);

        List<String> lstAdvFilters = new ArrayList<>();
        lstAdvFilters.add(AdvancedFilterTypes.APPLICATION_ID.getText());
        lstAdvFilters.add(AdvancedFilterTypes.VLAN_ID.getText());
        lstAdvFilters.add(AdvancedFilterTypes.PROCEDURE_TYPE.getText());
        lstAdvFilters.add(AdvancedFilterTypes.SUB_PROCEDURE_TYPE.getText());
        lstAdvFilters.add(AdvancedFilterTypes.TRANSPORT_LAYER_PROTOCOL.getText());
        dt.setAdvancedFiltersMandatory(lstAdvFilters);
    }

    private void addExtraFilters(DashletPrototype dt) {
        List<String> lstAdvFiltersExtra = new ArrayList<>();
        lstAdvFiltersExtra.add(AdvancedFilterTypes.RELEASE_TYPE.getText());
        lstAdvFiltersExtra.add(AdvancedFilterTypes.CAUSE.getText());
        dt.setAdvancedFiltersExtra(lstAdvFiltersExtra);
    }
}
