package com.radcom.qinsight.dashlets.model.accessors;

import com.radcom.qinsight.dashlets.model.DashletPrototype;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@ApplicationScoped
public class DaoDashletPrototypes {

    @PersistenceContext(unitName = "mongo-ogm")
    private EntityManager em;

    public void save(DashletPrototype dt) {
        em.persist(dt);
    }

    public List<DashletPrototype> getAll() {
        Query query = em.createQuery("FROM DashletPrototype dashlet");
        List<DashletPrototype> list = query.getResultList();

        return list;
    }

    public void delete(String id) {
        DashletPrototype templ = em.find(DashletPrototype.class, id);
        em.remove(templ);
    }

    public void deleteAll() {
        Query query = em.createQuery("DELETE FROM DashletPrototype dashlet");
        query.executeUpdate();
    }

}
