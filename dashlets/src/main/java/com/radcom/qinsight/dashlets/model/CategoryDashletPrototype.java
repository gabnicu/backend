package com.radcom.qinsight.dashlets.model;

import java.util.ArrayList;
import java.util.List;

public class CategoryDashletPrototype {


    private String category;
    private List<DashletPrototype> dashlets;

    public String getCategory() {
        return category;
    }

    public void setCategory(String cathegory) {
        this.category = cathegory;
    }

    public List<DashletPrototype> getDashlets() {
        return dashlets;
    }

    public void setDashlets(List<DashletPrototype> dashlets) {
        this.dashlets = dashlets;
    }

    public void addDashletPrototype(DashletPrototype proto) {
        if (dashlets == null)
            dashlets = new ArrayList<DashletPrototype>();
        dashlets.add(proto);
    }

}
