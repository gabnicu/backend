package com.radcom.qinsight.dashlets.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "dashlets")
public class DashletPrototype {

    @Id
    private String id;

    private String category;                            // Could be Diameter, DNS or whatever
    private String name;                                // Could be "Average Response Time", etc
    private String valueTypeSuffix;
    private Boolean displayValueTypeSuffix;             // could be "%" or no value;
    private String tooltipValueLabel;
    private String stackFragmentLabel;
    private DataDisplayType displayTypeAxisY = DataDisplayType.INTEGER;

    private String urlForData;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> filters;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> advancedFiltersMandatory;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> advancedFiltersExtra;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValueTypeSuffix() {
        return valueTypeSuffix;
    }

    public void setValueTypeSuffix(String valueTypeSuffix) {
        this.valueTypeSuffix = valueTypeSuffix;
    }

    public Boolean getDisplayValueTypeSuffix() {
        return displayValueTypeSuffix;
    }

    public void setDisplayValueTypeSuffix(Boolean displayValueTypeSuffix) {
        this.displayValueTypeSuffix = displayValueTypeSuffix;
    }

    public String getTooltipValueLabel() {
        return tooltipValueLabel;
    }

    public void setTooltipValueLabel(String tooltipValueLabel) {
        this.tooltipValueLabel = tooltipValueLabel;
    }

    public String getStackFragmentLabel() {
        return stackFragmentLabel;
    }

    public void setStackFragmentLabel(String stackFragmentLabel) {
        this.stackFragmentLabel = stackFragmentLabel;
    }

    public DataDisplayType getDisplayTypeAxisY() {
        return displayTypeAxisY;
    }

    public void setDisplayTypeAxisY(DataDisplayType displayTypeAxisY) {
        this.displayTypeAxisY = displayTypeAxisY;
    }

    public String getUrlForData() {
        return urlForData;
    }

    public void setUrlForData(String urlForData) {
        this.urlForData = urlForData;
    }

    public List<String> getFilters() {
        return filters;
    }

    public void setFilters(List<String> filters) {
        this.filters = filters;
    }

    public List<String> getAdvancedFiltersMandatory() {
        return advancedFiltersMandatory;
    }

    public void setAdvancedFiltersMandatory(List<String> advancedFiltersMandatory) {
        this.advancedFiltersMandatory = advancedFiltersMandatory;
    }

    public List<String> getAdvancedFiltersExtra() {
        return advancedFiltersExtra;
    }

    public void setAdvancedFiltersExtra(List<String> advancedFiltersExtra) {
        this.advancedFiltersExtra = advancedFiltersExtra;
    }
}
