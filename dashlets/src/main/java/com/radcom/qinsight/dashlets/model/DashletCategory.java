package com.radcom.qinsight.dashlets.model;

public enum DashletCategory {
    DIAMETER("DIAMETER"),
    DNS("DNS");

    private final String text;

    DashletCategory(final String txt) {
        this.text = txt;
    }

    @Override
    public String toString() {
        return text;
    }

    public String getText() {
        return this.text;
    }
}
