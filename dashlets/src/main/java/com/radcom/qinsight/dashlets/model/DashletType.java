package com.radcom.qinsight.dashlets.model;

public enum DashletType {
    DIAMETER_ERROR_CODE_DISTRIBUTION("DIAMETER_ERROR_CODE_DISTRIBUTION", "Diameter Error Code Distribution"),
    DIAMETER_AVERAGE_RESPONSE_TIME("DIAMETER_AVERAGE_RESPONSE_TIME", "Diameter Average Response Time"),
    DIAMETER_PEAK_RESPONSE_TIME("DIAMETER_PEAK_RESPONSE_TIME", "Diameter Peak Response Time"),
    DIAMETER_REQUEST_COUNT("DIAMETER_REQUEST_COUNT", "Diameter Request Count"),
    DIAMETER_RESPONSE_COUNT_BY_TYPE("DIAMETER_RESPONSE_COUNT_BY_TYPE", "Diameter Response Count By Type"),
    DIAMETER_RETRANSMISSION_RATIO("DIAMETER_RETRANSMISSION_RATIO", "Diameter Retransmission Ratio"),

    DNS_ERROR_CODE_DISTRIBUTION("DNS_ERROR_CODE_DISTRIBUTION", "DNS Error Code Distribution"),
    DNS_AVERAGE_RESPONSE_TIME("DNS_AVERAGE_RESPONSE_TIME", "DNS Average Response Time"),
    DNS_PEAK_RESPONSE_TIME("DNS_PEAK_RESPONSE_TIME", "DNS Peak Response Time"),
    DNS_REQUEST_COUNT("DNS_REQUEST_COUNT", "DNS Request Count"),
    DNS_RESPONSE_COUNT_BY_TYPE("DNS_RESPONSE_COUNT_BY_TYPE", "DNS Response Count By Type"),
    DNS_RETRANSMISSION_RATIO("DNS_RETRANSMISSION_RATIO", "DNS Retransmission Ratio");

    private final String key;
    private final String value;

    DashletType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

//	public static DashletType fromString(String text) {
//		for (DashletType dashTyp : DashletType.values()) {
//			if (dashTyp.text.equalsIgnoreCase(text)) {
//				return dashTyp;
//			}
//		}
//		return null;
//	}
}
