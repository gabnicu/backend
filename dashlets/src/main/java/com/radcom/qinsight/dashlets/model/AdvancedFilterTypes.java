package com.radcom.qinsight.dashlets.model;

public enum AdvancedFilterTypes {
    PROCEDURE_TYPE("Procedure Type"),
    SUB_PROCEDURE_TYPE("Procedure Subtype"),
    VLAN_ID("VLAN ID"),
    TRANSPORT_LAYER_PROTOCOL("Transport Layer Protocol"),
    RELEASE_TYPE("Release Type"),
    CAUSE("Cause"),
    APPLICATION_ID("Application ID");

    private final String text;

    AdvancedFilterTypes(final String txt) {
        this.text = txt;
    }

    @Override
    public String toString() {
        return text;
    }

    public String getText() {
        return this.text;
    }

}
