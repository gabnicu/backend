package com.radcom.qinsight.dashlets.model;

public enum DataDisplayType {
    INTEGER("INTEGER"),
    DOUBLE_TWO_DECIMALS("DOUBLE_TWO_DECIMALS"),
    LONG("LONG");

    private final String text;

    DataDisplayType(final String txt) {
        this.text = txt;
    }

    @Override
    public String toString() {
        return text;
    }
}
