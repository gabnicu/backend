package com.radcom.qinsight.dashlets.model;

public enum FilterTypes {

    NE_HIERARCHY("NE HIERARCHY"),
    LINK("LINK"),
    PROCEDURE_TYPE("PROCEDURE TYPE"),
    START_DATE("FROM"),
    END_DATE("TO");

    private final String text;

    FilterTypes(final String txt) {
        this.text = txt;
    }

    @Override
    public String toString() {
        return text;
    }

    public String getText() {
        return this.text;
    }
}
