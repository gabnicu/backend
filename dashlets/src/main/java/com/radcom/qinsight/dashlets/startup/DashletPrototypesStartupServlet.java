package com.radcom.qinsight.dashlets.startup;

import com.radcom.qinsight.dashlets.business.DashletPrototypesProvider;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DashletPrototypesStartupServlet implements ServletContextListener {

    private static final Logger theLog = Logger.getLogger(DashletPrototypesStartupServlet.class);

    @Inject
    private DashletPrototypesProvider prov;

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        theLog.info("------------------------------------------------------------------ DashletPrototypesStartupServlet");

        prov.verifyAtStartUp();
    }
}
