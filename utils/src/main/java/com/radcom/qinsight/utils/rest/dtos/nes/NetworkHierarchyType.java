package com.radcom.qinsight.utils.rest.dtos.nes;

public enum NetworkHierarchyType {
    ALL("ALL"),
    LOCATION("LOCATION"),
    GROUP("GROUP"),
    NETWORK_ELEMENT("NETWORK_ELEMENT");

    private final String text;


    NetworkHierarchyType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}
