package com.radcom.qinsight.utils.file;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FileUtil {

    private static final Logger theLog = Logger.getLogger(FileUtil.class);

    public static void readFile() {
        theLog.info("Config dir: " + System.getProperty("jboss.server.config.dir"));
        theLog.info("Data dir: " + System.getProperty("jboss.server.data.dir"));
    }

    public static Properties readPropertiesFile(String propFileName) {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(System.getProperty("jboss.server.data.dir") + "/" + propFileName);
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prop;
    }
}
