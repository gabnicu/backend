package com.radcom.qinsight.utils.rest.dtos.nes;

import java.util.List;

public class DTONetworkElements {
    protected List<NetworkElementEntry> data = null;

    public List<NetworkElementEntry> getData() {
        return data;
    }

    public void setData(List<NetworkElementEntry> data) {
        this.data = data;
    }
}
