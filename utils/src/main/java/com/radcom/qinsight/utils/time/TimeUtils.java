package com.radcom.qinsight.utils.time;

import com.radcom.qinsight.utils.constants.AppConstants;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class TimeUtils {

    private static final Logger logger = Logger.getLogger(TimeUtils.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static long getCurrentUtcAsLong() {
        long t3 = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
        logger.info("UTC: " + t3);
        return t3;
    }

    private static String obtainStringFromUtcLong(long millisecs) {
        String sz = Instant.ofEpochMilli(millisecs).toString();
//        logger.info("UTC: " + sz);
        return sz;
    }

    public static String obtainSqlStringFromUtcLong(long millisecs) {
        String sz = obtainStringFromUtcLong(millisecs);
        sz = sz.replaceAll("T", " ");
        sz = sz.replaceAll("Z", "");
        sz = sz + ".0000000";
        logger.info("SQL UTC: " + sz);
        return sz;
    }

    public static String convertDateFromYYYYMMDDtoMMMDD(String initialDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(initialDate);
            DateFormat out = new SimpleDateFormat("MMM dd");
            return (out.format(date));

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertDateFromYYYYMMDDtoMMMYYYY(String initialDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(initialDate);
            DateFormat out = new SimpleDateFormat("MMM YYYY");
            return (out.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int getYearFromDateYYYYMMDD(String date) {
        if (date == null)
            return AppConstants.NON_VALID_POSITIVE_VALUE;
        if (date.length() < 4)
            return AppConstants.NON_VALID_POSITIVE_VALUE;
        String year = date.substring(0, 4);
        return Integer.parseInt(year);
    }

    private static int getMonthFromDateYYYYMMDD(String date) {
        if (date == null)
            return AppConstants.NON_VALID_POSITIVE_VALUE;
        if (date.length() < 7)
            return AppConstants.NON_VALID_POSITIVE_VALUE;
        String month = date.substring(5, 7);
        if (month.startsWith("0"))
            month = month.substring(1);
        return Integer.parseInt(month);
    }

    private static int getDayFromDateYYYYMMDD(String date) {
        if (date == null)
            return AppConstants.NON_VALID_POSITIVE_VALUE;
        if (date.length() < 10)
            return AppConstants.NON_VALID_POSITIVE_VALUE;
        String month = date.substring(8, 10);
        if (month.startsWith("0"))
            month = month.substring(1);
        return Integer.parseInt(month);
    }

    public static String generateIsoDateStartOfDay(String date) {
        String part1 = date.substring(0, 10);
//        logger.info("generateIsoDateStartOfDay date: " + date + " return: " + part1 + " 00:00:00");
        return part1 + " 00:00:00";
    }

    public static String generateIsoDateEndOfDay(String date) {
        String part1 = date.substring(0, 10);
//        logger.info("generateIsoDateEndOfDay date: " + date + " return: " + part1 + " 23:59:59");
        return part1 + " 23:59:59";
    }

    public static String generateIsoDateEndOfYear(String szYear) {
        logger.info("generateIsoDateEndOfYear return: " + szYear + "-12-31 23:59:59");
        return szYear + "-12-31 23:59:59";
    }

    public static String generateIsoDateStartOfYear(String szYear) {
        logger.info("generateIsoDateStartOfYear return: " + szYear + "-01-01 00:00:00");
        return szYear + "-01-01 00:00:00";
    }

    public static String generateIsoDateEndOfMonth(String date) {
        String dt = computeLastDayOfMonth(date);

//        logger.info("generateIsoDateEndOfMonth date: " + date + " return: " + dt + " 23:59:59");

        return dt + " 23:59:59";
    }

    public static String generateIsoDateStartOfMonth(String date) {

//        logger.info("generateIsoDateStartOfMonth date: " + date + " return: " + date.substring(0, 7) + "-01 00:00:00");

        return date.substring(0, 7) + "-01 00:00:00";
    }


    public static String generateIsoDateEndOfQuarter(String date) {
//        logger.info("generateIsoDateEndOfQuarter date: " + date);

        GregorianCalendar calendar = new GregorianCalendar();

        int year = getYearFromDateYYYYMMDD(date);
        int month = getMonthFromDateYYYYMMDD(date);

        String monthExt = null;
        if (month <= 3) {
            monthExt = "03";
        } else if (month <= 6) {
            monthExt = "06";
        } else if (month <= 9) {
            monthExt = "9";
        } else if (month <= 12) {
            monthExt = "12";
        }

        calendar.set(year, (Integer.parseInt(monthExt) - 1), 1);
        int lastDay = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

//        logger.info("generateIsoDateEndOfQuarter date: " + date + " return: " + year + "-" + monthExt + "-" + lastDay + " 23:59:59");

        return year + "-" + monthExt + "-" + lastDay + " 23:59:59";

//        return "2018-06-30 23:59:59";
    }

    public static String generateIsoDateStartOfQuarter(String date) {

        int year = getYearFromDateYYYYMMDD(date);
        int month = getMonthFromDateYYYYMMDD(date);

        String monthExt = null;
        if (month <= 3) {
            monthExt = "01";
        } else if (month <= 6) {
            monthExt = "04";
        } else if (month <= 9) {
            monthExt = "07";
        } else if (month <= 12) {
            monthExt = "10";
        }

//        logger.info("generateIsoDateStartOfQuarter date: " + date + " return: " + year + "-" + monthExt + "-01 00:00:00");

        return year + "-" + monthExt + "-01 00:00:00";

//        return "2018-04-01 00:00:00";
    }

    public static String generateAngloSaxonDate(String date) {
//        logger.info("generateAngloSaxonDate date: " + date);
        if (date == null) {
            return null;
        }
        if (date.length() < 10) {
            return null;
        }
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        return month + "/" + day + "/" + year;
    }


    public static String generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(String date) {
//        logger.info("generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD date: " + date);
        if (date == null) {
            return null;
        }
        if (date.length() < 10) {
            return null;
        }
        int year = getYearFromDateYYYYMMDD(date);
        int month = getMonthFromDateYYYYMMDD(date);
        int day = getDayFromDateYYYYMMDD(date);

//        logger.info("year: " + year + " month: " + month + " day: " + day);

        String monthExt = null;
        if (month <= 3) {
            monthExt = "01";
        } else if (month <= 6) {
            monthExt = "04";
        } else if (month <= 9) {
            monthExt = "07";
        } else if (month <= 12) {
            monthExt = "10";
        }

//        logger.info("generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD return: " + (monthExt + "/01/" + year));

        return monthExt + "/01/" + year;
    }


    // parameter date is of format yyyy-MM-dd
    private static String computeLastDayOfMonth(String date) {
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate lastDayOfMonth = localDate.with(TemporalAdjusters.lastDayOfMonth());
        String szRet = lastDayOfMonth.toString();
//        logger.info("Last Day of Month: " + szRet);
        return szRet;
    }

    public static String getIsoStringFromLong(long theTime, String timeZone) {
        if (timeZone == null) {
            timeZone = "GMT+0:00";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        String date = sdf.format(theTime);
        return date;
    }

    public static long getLongFromIsoString(String isoDate, String timeZone) {
//        logger.info("getLongFromIsoString() isoDate: " + isoDate + " timeZone: " + timeZone);
        if (timeZone == null) {
            timeZone = "GMT+0:00";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d;
        try {
            d = sdf.parse(isoDate);
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
            cal.setTime(d);
            return cal.getTimeInMillis();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return -1L;
        }
    }

    public static String getGmtStringFromDeltaInMinutes(int delta) {
        switch (delta) {
            case 0:
                return "GMT+0.00";
            case -60:
                return "GMT+1.00";
            case -120:
                return "GMT+2.00";
            case -180:
                return "GMT+3.00";
            case -240:
                return "GMT+4.00";
            case -300:
                return "GMT+5.00";
            case -360:
                return "GMT+6.00";
            case -420:
                return "GMT+7.00";
            case -480:
                return "GMT+8.00";
            case -540:
                return "GMT+9.00";
            case -600:
                return "GMT+10.00";
            case -660:
                return "GMT+11.00";
            case -720:
                return "GMT+12.00";
            case 60:
                return "GMT-01.00";
            case 120:
                return "GMT-02.00";
            case 180:
                return "GMT-03.00";
            case 240:
                return "GMT-04.00";
            case 300:
                return "GMT-05.00";
            case 360:
                return "GMT-06.00";
            case 420:
                return "GMT-07.00";
            case 480:
                return "GMT-08.00";
            case 540:
                return "GMT-09.00";
            case 600:
                return "GMT-10.00";
            case 660:
                return "GMT-11.00";

            default:
                return "GMT+3.00";
        }
    }

    public static String getQuarterFromDate(String date) {
        int month = getMonthFromDateYYYYMMDD(date);
        int year = getYearFromDateYYYYMMDD(date);
        String ret = null;
        if (month <= 3) {
            ret = "Quarter 1, ";
        } else if (month <= 6) {
            ret = "Quarter 2, ";
        } else if (month <= 9) {
            ret = "Quarter 3, ";
        } else if (month <= 12) {
            ret = "Quarter 4, ";
        }
        ret += year;
        return ret;
    }

    public static String toISO8601FromUTC(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        df.setTimeZone(tz);
        return df.format(date);
    }

    public static String toISO8601FromUTC(long millisec) {
        Date date
                = new Date(millisec);
        return toISO8601FromUTC(date);
    }

    public static Date fromISO8601ToUTC(String dateStr) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        df.setTimeZone(tz);

        try {
            return df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static long fromISO8601ToUTCAsMillisec(String dateStr) {
        Date dt = fromISO8601ToUTC(dateStr);
        if (dt == null)
            return -1;
        return dt.getTime();
    }
}
