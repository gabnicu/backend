package com.radcom.qinsight.utils.rest.dtos;

public class DTOWithInclusions extends BasicDTO {

    protected Object included;

    public Object getIncluded() {
        return included;
    }

    public void setIncluded(Object included) {
        this.included = included;
    }

}
