package com.radcom.qinsight.utils.rest.dtos;

import java.io.Serializable;

public class BasicDTO implements Serializable {

    protected Object data = null;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
