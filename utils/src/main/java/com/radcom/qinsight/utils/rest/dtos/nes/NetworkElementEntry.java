package com.radcom.qinsight.utils.rest.dtos.nes;


public class NetworkElementEntry {

    private String neName;
    private String neType;
    private String technology;
    private String location;
    private String neGroup;
    private String interfaceAddr;


    public String getNeName() {
        return neName;
    }


    public void setNeName(String neName) {
        this.neName = neName;
    }


    public String getNeType() {
        return neType;
    }


    public void setNeType(String neType) {
        this.neType = neType;
    }


    public String getTechnology() {
        return technology;
    }


    public void setTechnology(String technology) {
        this.technology = technology;
    }


    public String getLocation() {
        return location;
    }


    public void setLocation(String location) {
        this.location = location;
    }


    public String getNeGroup() {
        return neGroup;
    }


    public void setNeGroup(String neGroup) {
        this.neGroup = neGroup;
    }

    public String getInterfaceAddr() {
        return interfaceAddr;
    }

    public void setInterfaceAddr(String interfaceAddr) {
        this.interfaceAddr = interfaceAddr;
    }
}
