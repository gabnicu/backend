package com.radcom.qinsight.utils.rest.dtos.nes;

import java.util.ArrayList;
import java.util.List;

public class NetworkHierarchyElement {

    private String neName = null;
    private NetworkHierarchyType neType = null;


    private List<NetworkHierarchyElement> listOfChildren = new ArrayList<NetworkHierarchyElement>();


    public void addChild(NetworkHierarchyElement nhe) {
        if (NetworkHierarchyType.NETWORK_ELEMENT.equals(neType)) {
            return;
        }
        listOfChildren.add(nhe);
    }

    public String getNeName() {
        return neName;
    }


    public void setNeName(String neName) {
        this.neName = neName;
    }


    public NetworkHierarchyType getNeType() {
        return neType;
    }


    public void setNeType(NetworkHierarchyType neType) {
        this.neType = neType;
    }


    public List<NetworkHierarchyElement> getListOfChildren() {
        return listOfChildren;
    }

}
