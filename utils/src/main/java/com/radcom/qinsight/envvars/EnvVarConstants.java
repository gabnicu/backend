package com.radcom.qinsight.envvars;

public class EnvVarConstants {

    public static final String NES_REPETITON_TIME_SECONDS = "NES_REPETITON_TIME";

    public static final long DEFAULT_NES_REPETITON_TIME_SECONDS = 120;

}
