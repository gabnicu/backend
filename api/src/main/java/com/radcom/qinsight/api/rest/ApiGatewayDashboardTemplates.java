package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Stateless
@Path("dashboardTemplates")
public class ApiGatewayDashboardTemplates {

    @Inject
    @EnvVarServices
    private ServiceDiscovery serviceDiscovery;


//    @GET
//    @Produces({"application/json"})
//    public List<TransferObjDnsEntry> findAll( @QueryParam("year") final int year, @QueryParam("month") final String month, @QueryParam("week") final String week) {
//    	
//    	System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
//    	
//    	TransferObjDnsEntry[] usersArr	=	services.getDnsErrorCodesService().queryParam("year", year).queryParam("month", month).queryParam("week", week).register(TransferObjDnsEntry.class).request().get(TransferObjDnsEntry[].class);
//    	List<TransferObjDnsEntry> lst	=	new ArrayList<TransferObjDnsEntry>();
//    	for ( int i=0; i<usersArr.length; i++)
//    		lst.add( usersArr[i]);
//    	return lst;
//    }   


    @GET
    @Produces({"application/json"})
    public Object findAll() {


        Object usersArr = serviceDiscovery.getDashboardTemplatesService().register(Object.class).request().get(Object.class);

        return usersArr;
    }


}
