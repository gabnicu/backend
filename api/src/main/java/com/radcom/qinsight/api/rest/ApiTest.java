package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.api.rest.tos.Product;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("product")
public class ApiTest {

    private static final Logger theLog = Logger.getLogger(ApiTest.class);

    @POST
    @Path("/post")
    @Consumes("application/json")
    public Response createProductInJSON(Product product) {

        String result = "Product received : " + product.getName() + "::" + product.getValue();
        return Response.status(201).entity(result).build();
    }

    @POST
    @Path("{userId}/post/")
    @Consumes("application/json")
    public Response createProductInJSONWithId(@PathParam("userId") String userId, Product product) {

        String result = "Product received : " + userId + "::" + product.getName() + "::" + product.getValue();
        return Response.status(201).entity(result).build();
    }


    @POST
    @Path("{userId}/dashboards/")
    @Consumes({"application/json"})
    public Response addDashboardToUser(@PathParam("userId") String userId, BasicDTO dto) {

        theLog.info("------- addDashboardToUser dto=" + dto);
        Object data = dto.getData();
        theLog.info("BasicDTO POST received : " + data.getClass().toString());
        Product prod = (Product) data;
        String result = "Product POST received : " + prod.getName() + "::" + prod.getValue();
        return Response.status(201).entity(result).build();
    }
}
