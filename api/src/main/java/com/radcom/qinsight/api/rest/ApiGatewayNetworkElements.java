package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Stateless
@Path("networkElements")
public class ApiGatewayNetworkElements {

    @Inject
    @EnvVarServices
    private ServiceDiscovery serviceDiscovery;

    @GET
    @Produces({"application/json"})
    public Object findAll(@QueryParam("path") String path, @QueryParam("include") String include) {
        try {
            Object usersArr = serviceDiscovery.getNetworkElementsService().queryParam("path", path).queryParam("include", include).register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
