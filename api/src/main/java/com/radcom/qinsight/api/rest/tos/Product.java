package com.radcom.qinsight.api.rest.tos;

public class Product {
    private String name;
    private int value = 77;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


}
