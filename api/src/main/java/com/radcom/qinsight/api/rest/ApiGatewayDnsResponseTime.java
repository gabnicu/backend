package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.api.rest.tos.TransferObjDnsEntry;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import com.radcom.qinsight.services.impl.consul.ConsulDiscovery;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Path("dns/errorCodes")
public class ApiGatewayDnsResponseTime {

    private ServiceDiscovery services = new ConsulDiscovery();


//    @GET
//    @Produces({"application/json"})
//    public List<TransferObjDnsEntry> findAll( @QueryParam("type") final String type, @QueryParam("year") final int year, @QueryParam("month") final String month, @QueryParam("week") final String week) {
//    	
//    	System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ type=" + type);
//    	
//    	TransferObjDnsEntry[] usersArr	=	services.getDnsErrorCodesService().queryParam("type", type).queryParam("year", year).queryParam("month", month).queryParam("week", week).register(TransferObjDnsEntry.class).request().get(TransferObjDnsEntry[].class);
//    	List<TransferObjDnsEntry> lst	=	new ArrayList<TransferObjDnsEntry>();
//    	for ( int i=0; i<usersArr.length; i++)
//    		lst.add( usersArr[i]);
//    	return lst;
//    }   
//    


    @GET
    @Produces({"application/json"})
    public List<TransferObjDnsEntry> findAll(@QueryParam("type") final String type, @QueryParam("year") final int year, @QueryParam("month") final String month, @QueryParam("week") final String week) {

        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ type=" + type);

        TransferObjDnsEntry[] usersArr = services.getDnsErrorCodesService().queryParam("type", type).queryParam("year", year).queryParam("month", month).queryParam("week", week).register(TransferObjDnsEntry.class).request().get(TransferObjDnsEntry[].class);
        List<TransferObjDnsEntry> lst = new ArrayList<TransferObjDnsEntry>();
        for (int i = 0; i < usersArr.length; i++)
            lst.add(usersArr[i]);
        return lst;
    }

}
