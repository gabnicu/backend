package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

@Stateless
@Path("metrics")
public class ApiGatewayMetrics {

    private static final Logger theLog = Logger.getLogger(ApiGatewayMetrics.class);


    @Inject
    @EnvVarServices
    private ServiceDiscovery serviceDiscovery;


    @GET
    @Path("diameter/errorCodeDistribution")
    @Produces({"application/json"})
    public Object getMetricsDiameterErrorCodeDistribution(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterErrorCodeDistributionService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta);
            if (tgt == null) {
                theLog.info("Returned Response.Status.NOT_FOUNDd");
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/retransmissionRatio")
    @Produces({"application/json"})
    public Object getMetricsDiameterRetransmissionRatio(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta) {
        theLog.info("/api/v0/metrics/diameter/retransmissionRatio");
        Response resp = null;
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterRetransmissionRatioService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta);
            if (tgt == null) {
                theLog.info("Returned Response.Status.NOT_FOUNDd");
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
//    		resp	=	tgt.register(Object.class).request().get();
//    		String szResp	=	(String)resp.getEntity();
//    		theLog.info( "Returning: " + szResp);
//    		return resp.getEntity();
        } catch (Exception ex) {
            ex.printStackTrace();
            theLog.info("Returned Response.Status.NOT_FOUND");
            return Response.status(Response.Status.NOT_FOUND).build();
        } finally {
            if (resp != null)
                resp.close();
        }
    }

    @GET
    @Path("diameter/averageResponseTime")
    @Produces({"application/json"})
    public Object getMetricsDiameterAverageResponseTime(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterAverageResponseTimeService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }

    }


    @GET
    @Path("diameter/requestCount")
    @Produces({"application/json"})
    public Object getMetricsDiameterRequestCount(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterRequestCountService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/peakResponseTime")
    @Produces({"application/json"})
    public Object getMetricsDiameterPeakResponseTime(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("procedureType") final String procedureType,
            @QueryParam("procedureSubtype") final String procedureSubtype,
            @QueryParam("vlanId") final String vlanId,
            @QueryParam("applicationId") final String applicationId,
            @QueryParam("transportLayerProtocol") final String transportLayerProtocol) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterPeakResponseTimeService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta)
                    .queryParam("procedureType", procedureType)
                    .queryParam("procedureSubtype", procedureSubtype)
                    .queryParam("vlanId", vlanId)
                    .queryParam("applicationId", applicationId)
                    .queryParam("transportLayerProtocol", transportLayerProtocol);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/responseCountByType")
    @Produces({"application/json"})
    public Object getMetricsDiameterResponseCountByType(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterResponseCountByTypeService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/table/source")
    @Produces({"application/json"})
    public Object getMetricsDiameterTableSource(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") final String fragmentValue) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterTableSourceService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta)
                    .queryParam("dashletType", dashletType)
                    .queryParam("fragmentValue", fragmentValue);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/table/destination")
    @Produces({"application/json"})
    public Object getMetricsDiameterTableDestination(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") final String fragmentValue) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterTableDestinationService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta)
                    .queryParam("dashletType", dashletType)
                    .queryParam("fragmentValue", fragmentValue);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/table/pair")
    @Produces({"application/json"})
    public Object getMetricsDiameterTablePair(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") final String fragmentValue) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterTablePairService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta)
                    .queryParam("dashletType", dashletType)
                    .queryParam("fragmentValue", fragmentValue);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/table/worst/source")
    @Produces({"application/json"})
    public Object getMetricsDiameterWorstTableSource(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") final String fragmentValue) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterTableWorstSourceService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta)
                    .queryParam("dashletType", dashletType)
                    .queryParam("fragmentValue", fragmentValue);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/table/worst/destination")
    @Produces({"application/json"})
    public Object getMetricsDiameterWorstTableDestination(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") final String fragmentValue) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterTableWorstDestinationService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta)
                    .queryParam("dashletType", dashletType)
                    .queryParam("fragmentValue", fragmentValue);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("diameter/table/worst/pair")
    @Produces({"application/json"})
    public Object getMetricsDiameterWorstTablePair(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") final String fragmentValue) {
        try {
            WebTarget tgt = serviceDiscovery.getMetricsDiameterTableWorstPairService()
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("networkElements", networkElements)
                    .queryParam("delta", delta)
                    .queryParam("dashletType", dashletType)
                    .queryParam("fragmentValue", fragmentValue);
            if (tgt == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}