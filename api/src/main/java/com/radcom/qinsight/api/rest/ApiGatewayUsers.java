package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import com.radcom.qinsight.utils.constants.AppConstants;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("users")
public class ApiGatewayUsers {

    private static final Logger theLog = Logger.getLogger(ApiGatewayUsers.class);

    @Inject
    @EnvVarServices
    ServiceDiscovery serviceDiscovery;

//    @GET
//    @Produces({"application/json"})
//    public Object findAll( @QueryParam("name") String name) {   	
//    	Object usersArr	=	serviceDiscovery.getUserService().queryParam("name", name).register(Object.class).request().get(Object.class);
//    	return usersArr;
//    }   

    @GET
    @Produces({"application/json"})
    public Object findAll(@QueryParam("name") String name) {

        for (int i = 0; i < 3; i++) {

            try {
                Object usersArr = serviceDiscovery.getUserService(null).queryParam("name", name).register(Object.class).request().get(Object.class);
                return usersArr;
            } catch (Exception ex) {
//	    		ex.printStackTrace();
//	    		return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    @GET
    @Path("{userId}/dashboards/{dashboardId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Object getDashboardByIdForUser(@PathParam("userId") String userId, @PathParam("dashboardId") String dashboardId) {
        if (userId == null)
            userId = AppConstants.DEMO_USER_ID;

        for (int i = 0; i < 3; i++) {

            try {
                Object usersArr = serviceDiscovery.getUserService(userId + "/dashboards/" + dashboardId).register(Object.class).request().get(Object.class);
                return usersArr;
            } catch (Exception ex) {
//	    		ex.printStackTrace();
//	    		return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("{userId}/dashboards/")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Object getAllDashboardsForUser(@PathParam("userId") String userId) {
        if (userId == null)
            userId = AppConstants.DEMO_USER_ID;

        for (int i = 0; i < 3; i++) {

            try {
                Object usersArr = serviceDiscovery.getUserService(userId + "/dashboards/").register(Object.class).request().get(Object.class);
                return usersArr;
            } catch (Exception ex) {
//	    		ex.printStackTrace();
//	    		return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    @GET
    @Path("{userId}/dashboards/recent")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Object getLastViewedDashboardsForUser(@PathParam("userId") String userId) {
        if (userId == null)
            userId = AppConstants.DEMO_USER_ID;

        for (int i = 0; i < 3; i++) {

            try {
                Object usersArr = serviceDiscovery.getUserService(userId + "/dashboards/recent").register(Object.class).request().get(Object.class);
                return usersArr;
            } catch (Exception ex) {
//	    		ex.printStackTrace();
//	    		return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    @DELETE
    @Path("{id}")
    @Produces({"application/json"})
    public Object removeUser(@PathParam("id") String id) {
//    	WebTarget usersTarget	=	serviceDiscovery.getUserService( id);
//    	Object usersArr	=	usersTarget.register(Object.class).request().delete( Object.class);
//    	return usersArr;
        WebTarget usersTarget = serviceDiscovery.getUserService(id);
        Response resp = usersTarget.request().delete();
        return resp;
    }

    @POST
    @Path("{userId}/dashboards/{dashboardId}/lastDisplayed")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response changeLastUpdatedForDashboard(@PathParam("userId") String userId, @PathParam("dashboardId") String dashboardId) {
        WebTarget usersTarget = serviceDiscovery.getUserService(userId + "/dashboards/" + dashboardId + "/lastDisplayed");
        Response resp = usersTarget.register(Object.class).request().post(Entity.json(""));
        boolean bRet = resp.hasEntity();
        if (bRet) {
            String payload = resp.readEntity(String.class);
            return Response.status(resp.getStatus()).entity(payload).build();
        }
        return Response.status(resp.getStatus()).build();
    }

    @POST
    @Path("{userId}/dashboards/")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addDashboardToUser(@PathParam("userId") String userId, Object dto) {

        theLog.info("------- addDashboardToUser dto=" + dto.toString());
        WebTarget usersTarget = serviceDiscovery.getUserService(userId + "/dashboards/");
        Response resp = usersTarget.register(Object.class).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(dto));
        boolean bRet = resp.hasEntity();
        if (bRet) {
            String payload = resp.readEntity(String.class);
            return Response.status(resp.getStatus()).entity(payload).build();
        }
        return Response.status(resp.getStatus()).build();
    }

    @PUT
    @Path("{userId}/dashboards/")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response editDashboardForUser(@PathParam("userId") String userId, Object dto) {

        theLog.info("------- editDashboardForUser");
        WebTarget usersTarget = serviceDiscovery.getUserService(userId + "/dashboards/");
        Response resp = usersTarget.register(Object.class).request(MediaType.APPLICATION_JSON_TYPE).put(Entity.json(dto));
        boolean bRet = resp.hasEntity();
        if (bRet) {
            String payload = resp.readEntity(String.class);
            return Response.status(resp.getStatus()).entity(payload).build();
        }
        return Response.status(resp.getStatus()).build();
    }

}
