package com.radcom.qinsight.api.rest.tos;

public class TransferObjDnsEntry {

    private String theDate = null;
    private double theValue = 0.0;

    public String getTheDate() {
        return theDate;
    }

    public void setTheDate(String theDate) {
        this.theDate = theDate;
    }

    public double getTheValue() {
        return theValue;
    }

    public void setTheValue(double theValue) {
        this.theValue = theValue;
    }

}
