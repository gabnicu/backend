package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Stateless
@Path("dashlets")
public class ApiGatewayDashlets {

    @Inject
    @EnvVarServices
    private ServiceDiscovery serviceDiscovery;


    @GET
    @Produces({"application/json"})
    public Object findAll() {
        try {
            Object resp = serviceDiscovery.getDashletsService().register(Object.class).request().get(Object.class);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
