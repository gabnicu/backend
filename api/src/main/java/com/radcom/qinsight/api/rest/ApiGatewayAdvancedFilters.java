package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

@Stateless
@Path("advancedFilters")
public class ApiGatewayAdvancedFilters {

    private static final Logger theLog = Logger.getLogger(ApiGatewayAdvancedFilters.class);

    @Inject
    @EnvVarServices
    private ServiceDiscovery serviceDiscovery;


    @GET
    @Path("procType")
    @Produces({"application/json"})
    public Object getProcType() {
        theLog.info("getProcType");
        try {
            WebTarget tgt = serviceDiscovery.getAdvancedFiltersService();
            if (tgt == null) {
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ NULL ");
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("procSubtype")
    @Produces({"application/json"})
    public Object getProcSubtype() {
        theLog.info("getProcSubtype");
        try {
            WebTarget tgt = serviceDiscovery.getAllAdvancedFiltersService("procSubtype");
            if (tgt == null) {
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ NULL ");
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("vlanId")
    @Produces({"application/json"})
    public Object getVlanId() {
        theLog.info("getVlanId()");
        try {
            WebTarget tgt = serviceDiscovery.getAllAdvancedFiltersService("vlanId");
            if (tgt == null) {
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ NULL ");
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("applicationId")
    @Produces({"application/json"})
    public Object getApplicationId() {
        theLog.info("getApplicationId()");
        try {
            WebTarget tgt = serviceDiscovery.getAllAdvancedFiltersService("applicationId");
            if (tgt == null) {
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ NULL ");
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("transportLayerProtocol")
    @Produces({"application/json"})
    public Object getTransportLayerProtocol() {
        theLog.info("getTransportLayerProtocol()");
        try {
            WebTarget tgt = serviceDiscovery.getAllAdvancedFiltersService("transportLayerProtocol");
            if (tgt == null) {
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ NULL ");
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("releaseType")
    @Produces({"application/json"})
    public Object getReleaseType() {
        theLog.info("getReleaseType()");
        try {
            WebTarget tgt = serviceDiscovery.getAllAdvancedFiltersService("releaseType");
            if (tgt == null) {
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ NULL ");
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("cause")
    @Produces({"application/json"})
    public Object getCause() {
        theLog.info("getCause()");
        try {
            WebTarget tgt = serviceDiscovery.getAllAdvancedFiltersService("cause");
            if (tgt == null) {
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ NULL ");
            }
            Object usersArr = tgt.register(Object.class).request().get(Object.class);
            return usersArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
