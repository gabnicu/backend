package com.radcom.qinsight.api.rest;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import com.radcom.qinsight.utils.constants.AppConstants;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("dashboards")
public class ApiGatewayDashboards {


    private static final Logger theLog = Logger.getLogger(ApiGatewayDashboards.class);

    @Inject
    @EnvVarServices
    private ServiceDiscovery serviceDiscovery;


    @GET
    @Produces({"application/json"})
    public Object getAllDashboardsForDemoUser() {
        for (int i = 0; i < 3; i++) {

            try {
                Object usersArr = serviceDiscovery.getUserService(AppConstants.DEMO_USER_ID + "/dashboards/").register(Object.class).request().get(Object.class);
                return usersArr;
            } catch (Exception ex) {
//	    		ex.printStackTrace();
//	    		return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("{dashboardId}")
    @Produces({"application/json"})
    public Object getDashboardById(@PathParam("dashboardId") String dashboardId) {
        for (int i = 0; i < 3; i++) {

            try {
                Object usersArr = serviceDiscovery.getUserService(AppConstants.DEMO_USER_ID + "/dashboards/" + dashboardId).register(Object.class).request().get(Object.class);
                return usersArr;
            } catch (Exception ex) {
//	    		ex.printStackTrace();
//	    		return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    @GET
    @Path("recent")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Object getLastViewedDashboardsForUser() {
        for (int i = 0; i < 3; i++) {

            try {
                Object usersArr = serviceDiscovery.getUserService(AppConstants.DEMO_USER_ID + "/dashboards/recent").register(Object.class).request().get(Object.class);
                return usersArr;
            } catch (Exception ex) {
//	    		ex.printStackTrace();
//	    		return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


//    @POST
//    @Consumes({"application/json"})
//    @Produces({"application/json"})
//    public Response addDashboardToDemoUser( BasicDTO dto)
//    {
//    	
//    	theLog.info( "------- addDashboardToUser dto=" + dto.toString());
//    	WebTarget usersTarget	=	serviceDiscovery.getUserService( AppConstants.DEMO_USER_ID + "/dashboards/");
//    	Response resp	=	usersTarget.register(Object.class).request(MediaType.APPLICATION_JSON_TYPE).post( Entity.json( dto.getData()));
//    	boolean bRet	=	resp.hasEntity();
//    	if ( bRet)	{
//    		String payload	=	resp.readEntity( String.class);
//    		return Response.status(resp.getStatus()).entity( payload).build();
//    	}
//    	return Response.status(resp.getStatus()).build();
//    }
//
//    @PUT
//    @Consumes({"application/json"})
//    @Produces({"application/json"})
//    public Response editDashboardForDemoUser( BasicDTO dto)
//    {
//    	
//    	theLog.info( "------- editDashboardForDemoUser dto=" + dto.toString());
//    	WebTarget usersTarget	=	serviceDiscovery.getUserService( AppConstants.DEMO_USER_ID + "/dashboards/");
//    	Response resp	=	usersTarget.register(Object.class).request(MediaType.APPLICATION_JSON_TYPE).put( Entity.json( dto.getData()));
//    	boolean bRet	=	resp.hasEntity();
//    	if ( bRet)	{
//    		String payload	=	resp.readEntity( String.class);
//    		return Response.status(resp.getStatus()).entity( payload).build();
//    	}
//    	return Response.status(resp.getStatus()).build();
//    } 
//    

    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addDashboardToDemoUser(Object dto) {

        theLog.info("------- addDashboardToUser dto=" + dto.toString());
        WebTarget usersTarget = serviceDiscovery.getUserService(AppConstants.DEMO_USER_ID + "/dashboards/");
        Response resp = usersTarget.register(Object.class).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(dto));
        boolean bRet = resp.hasEntity();
        if (bRet) {
            String payload = resp.readEntity(String.class);
            return Response.status(resp.getStatus()).entity(payload).build();
        }
        return Response.status(resp.getStatus()).build();
    }

    @PUT
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response editDashboardForDemoUser(Object dto) {

        theLog.info("------- editDashboardForDemoUser dto=" + dto.toString());
        WebTarget usersTarget = serviceDiscovery.getUserService(AppConstants.DEMO_USER_ID + "/dashboards/");
        Response resp = usersTarget.register(Object.class).request(MediaType.APPLICATION_JSON_TYPE).put(Entity.json(dto));
        boolean bRet = resp.hasEntity();
        if (bRet) {
            String payload = resp.readEntity(String.class);
            return Response.status(resp.getStatus()).entity(payload).build();
        }
        return Response.status(resp.getStatus()).build();
    }

    @POST
    @Path("{dashboardId}/lastDisplayed")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response changeLastUpdatedForDashboardForDemoUser(@PathParam("dashboardId") String dashboardId) {
        WebTarget usersTarget = serviceDiscovery.getUserService(AppConstants.DEMO_USER_ID + "/dashboards/" + dashboardId + "/lastDisplayed");
        Response resp = usersTarget.register(Object.class).request().post(Entity.json(""));
        boolean bRet = resp.hasEntity();
        if (bRet) {
            String payload = resp.readEntity(String.class);
            return Response.status(resp.getStatus()).entity(payload).build();
        }
        return Response.status(resp.getStatus()).build();
    }
}
