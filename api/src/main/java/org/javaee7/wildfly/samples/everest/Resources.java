package org.javaee7.wildfly.samples.everest;

import com.radcom.qinsight.services.ConsulServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;


@ApplicationScoped
public class Resources {

    @Inject
    @ConsulServices
    ServiceDiscovery services;

    @Produces
    public ServiceDiscovery getService() {
        return services;
    }
}
