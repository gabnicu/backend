package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoErrorCodeDistribution;

import javax.enterprise.context.RequestScoped;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RequestScoped
@ProviderFile
public class DaoErrorCodeDistributionFile implements DaoErrorCodeDistribution {

    @Override
    public List<DbEntry> getData(String startDate, String endDate, int deltaUTC, String nes, boolean isMoreThan2days) {
        String dir = System.getProperty("jboss.server.data.dir");
        String fullPath = dir + "/" + "7.json";
        ObjectMapper mapper = new ObjectMapper();
        // Staff obj = mapper.readValue(new File("c:\\file.json"), Staff.class);
        try {
            List<DbEntry> list = mapper.readValue(new File(fullPath), new TypeReference<List<DbEntry>>() {
            });
            System.out.println("Loaded " + list.size() + " entries");
            return list;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
}
