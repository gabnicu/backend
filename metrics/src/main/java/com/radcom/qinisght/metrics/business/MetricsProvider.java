package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.*;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleDoubleEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryResponseCountByType;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryRetransmissionRatio;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryTable;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoErrorCodeDistribution;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl.*;
import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RequestScoped
public class MetricsProvider {

    private static final Logger theLog = Logger.getLogger(MetricsProvider.class);

    @Inject
    @ProviderVertica
    private DaoErrorCodeDistribution daoVertica;

    @Inject
    @ProviderVertica
    private DaoDataFromDb dbEntries;

    @Inject
    @ProviderVertica
    private DaoResponseCountByTypeVertica daoResponseCountByType;

    @Inject
    @ProviderVertica
    private DaoSourceTable daoSourceTable;

    @Inject
    @ProviderVertica
    private DaoWorstSourceTable daoWorstSourceTable;

    @Inject
    @ProviderVertica
    private DaoWorstDestinationTable daoWorstDestinationTable;

    @Inject
    @ProviderVertica
    private DaoWorstPairTable daoWorstPairTable;

    @Inject
    @ProviderVertica
    private DaoDestinationTable daoDestinationTable;

    @Inject
    @ProviderVertica
    private DaoPairTable daoPairTable;

    @Inject
    private DataAggregator aggregExt;

    @Inject
    private RetransmissionRatioDataAgreggator aggregRatio;

    @Inject
    private AverageResponseTimeAggregator aggregAverage;

    @Inject
    private PeakResponseTimeAggregator aggregPeak;

    @Inject
    private RequestCountAggregator aggregRequestCount;

    @Inject
    private ResponseCountByTypeAggregator aggregResponseCountByType;

    @Inject
    private TableAggregator tableAggregator;

    @Inject
    private NetworkElementsResolver resolver;

    public MetricsProvider() {
    }

    private AggregationType getAggregationType(String startDate, String endDate) {
        startDate = processDate(startDate);
        endDate = processDate(endDate);

        theLog.info("getAggregationType() startDate: " + startDate + " endDate " + endDate);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        AggregationType aggType;
        try {
            Date dtStart = sdf.parse(startDate);
            Date dtEnd = sdf.parse(endDate);

            theLog.info("after parse dtStart: " + dtStart + " dtEnd " + dtEnd);

            long delta = dtEnd.getTime() - dtStart.getTime();
            theLog.info("delta time: " + delta);
            if (delta < (3600L + 10) * 1000L) {
                aggType = AggregationType.AGG_TYPE_PER_5MINS;
            } else if (delta <= 2 * 24 * 3600L * 1000L) {
                aggType = AggregationType.AGG_TYPE_PER_HOUR;
            } else {
                if (delta < 60 * 24 * 3600L * 1000L)
                    aggType = AggregationType.AGG_TYPE_PER_DAY;
                else if (delta < 6 * 31 * 24 * 3600L * 1000L)
                    aggType = AggregationType.AGG_TYPE_PER_MONTH;
                else if (delta < 367 * 24 * 3600L * 1000L)
                    aggType = AggregationType.AGG_TYPE_PER_QUARTER;
                else
                    aggType = AggregationType.AGG_TYPE_PER_YEAR;
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return aggType;
    }

    private boolean isMoreThanTwoDays(String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date dtStart = sdf.parse(startDate);
            Date dtEnd = sdf.parse(endDate);
            long delta = dtEnd.getTime() - dtStart.getTime();


            if (delta <= 2 * 24 * 3600L * 1000L) {
                theLog.info("isMoreThanTwoDays: " + false);
                return false;
            } else {
                theLog.info("isMoreThanTwoDays: " + true);
                return true;
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            theLog.error("isMoreThanTwoDays: " + false + " error: " + e.getMessage());
            return false;
        }
    }

    public List<SampleEntry> getDiameterErrorCodeDistribution(String startDate, String endDate, int delta, String nes) {
        AggregationType aggType = getAggregationType(startDate, endDate);
        List<DbEntry> lstDbEntries = daoVertica.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate));
        return aggregExt.aggregateData(lstDbEntries, aggType, delta);
    }

    public List<SampleDoubleEntry> getRetransmissionRatioExt(String startDate, String endDate, int delta, String nes) {
        AggregationType aggType = getAggregationType(startDate, endDate);
        List<DbEntryRetransmissionRatio> lstDbEntries = dbEntries.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate));

        return aggregRatio.aggregateData(lstDbEntries, aggType, delta);
    }

    public List<SampleDoubleEntry> getAverageResponseTime(String startDate, String endDate, int delta, String nes) {
        AggregationType aggType = getAggregationType(startDate, endDate);
        List<DbEntryRetransmissionRatio> lstDbEntries = dbEntries.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate));

        return aggregAverage.aggregateData(lstDbEntries, aggType, delta);
    }

    public List<SampleEntry> getPeakResponseTime(String startDate, String endDate, int delta, String nes) {
        AggregationType aggType = getAggregationType(startDate, endDate);
        List<DbEntryRetransmissionRatio> lstDbEntries = dbEntries.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate));
        return aggregPeak.aggregateData(lstDbEntries, aggType, delta);
    }

    public List<SampleEntry> getRequestCount(String startDate, String endDate, int delta, String nes) {
        AggregationType aggType = getAggregationType(startDate, endDate);
        List<DbEntryRetransmissionRatio> lstDbEntries = dbEntries.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate));
        return aggregRequestCount.aggregateData(lstDbEntries, aggType, delta);
    }

    public List<SampleEntry> getResponseCountByType(String startDate, String endDate, int delta, String nes) {
        AggregationType aggType = getAggregationType(startDate, endDate);
        List<DbEntryResponseCountByType> lstDbEntries = daoResponseCountByType.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate));
        return aggregResponseCountByType.aggregateData(lstDbEntries, aggType, delta);
    }

    public TableResponse getTableDataForSourceNes(String startDate, String endDate, int delta, String nes, String fragmentValue, String dashletType) {
        List<DbEntryTable> lstDbEntries = daoSourceTable.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate), fragmentValue, dashletType);
        return tableAggregator.aggregateData(lstDbEntries, nes);
    }

    public TableResponse getTableDataForDestinationNes(String startDate, String endDate, int delta, String nes, String fragmentValue, String dashletType) {
        List<DbEntryTable> lstDbEntries = daoDestinationTable.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate), fragmentValue, dashletType);
        return tableAggregator.aggregateData(lstDbEntries, nes);
    }

    public TableResponse getTableDataForPairNes(String startDate, String endDate, int delta, String nes, String fragmentValue, String dashletType) {
        List<DbEntryTable> lstDbEntries = daoPairTable.getData(startDate, endDate, delta, nes, isMoreThanTwoDays(startDate, endDate), fragmentValue, dashletType);
        return tableAggregator.aggregateData(lstDbEntries, nes);
    }

    public TableResponse getTableDataForWorstSourceNes(String startDate, String endDate, int deltaUTC, String nes, String fragmentValue, String dashletType) {
        List<DbEntryTable> lstDbEntries = daoWorstSourceTable.getData(startDate, endDate, deltaUTC, nes, isMoreThanTwoDays(startDate, endDate), fragmentValue, dashletType);
        return tableAggregator.aggregateData(lstDbEntries, nes);
    }

    public TableResponse getTableDataForWorstDestinationNes(String startDate, String endDate, int deltaUTC, String nes, String fragmentValue, String dashletType) {
        List<DbEntryTable> lstDbEntries = daoWorstSourceTable.getData(startDate, endDate, deltaUTC, nes, isMoreThanTwoDays(startDate, endDate), fragmentValue, dashletType);
        return tableAggregator.aggregateData(lstDbEntries, nes);
    }

    public TableResponse getTableDataForWorstPairNes(String startDate, String endDate, int deltaUTC, String nes, String fragmentValue, String dashletType) {
        List<DbEntryTable> lstDbEntries = daoWorstSourceTable.getData(startDate, endDate, deltaUTC, nes, isMoreThanTwoDays(startDate, endDate), fragmentValue, dashletType);
        return tableAggregator.aggregateData(lstDbEntries, nes);
    }

    // 2018-05-27 00:00:02
    private String processDate(String initialString) {

        if (initialString.length() < 19)
            return null;
        if (initialString.length() > 19)
            initialString = initialString.substring(0, 19);
        String sz = initialString.replaceAll("T", " ");
        return sz;
    }


    private List<String> processNetworkElements(String initialString) {

        initialString = initialString.replaceAll("\\[", "");
        initialString = initialString.replaceAll("]", "");
        initialString = initialString.replaceAll("\\{", "");
        initialString = initialString.replaceAll("}", "");
        initialString = initialString.replaceAll("\"", "");

        String[] items = initialString.split(",");
        Set<String> hashItems = new HashSet<>();
        for (String item : items) {
            item = item.trim();
            List<String> lstResolved = resolver.resolveNetworkElement(item);
            hashItems.addAll(lstResolved);
        }
        List<String> lstRet = new ArrayList<>();
        for (String item : hashItems) {
            item = item.trim();
            lstRet.add(item);
        }
        return lstRet;
    }
}
