package com.radcom.qinisght.metrics.dtos;

public class TableSubLine extends TableLineKPIs {
    private String destinationName; //TE1

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

}
