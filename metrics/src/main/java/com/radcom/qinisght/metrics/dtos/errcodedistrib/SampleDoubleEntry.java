package com.radcom.qinisght.metrics.dtos.errcodedistrib;

import java.util.ArrayList;
import java.util.List;

public class SampleDoubleEntry {

//	private static final Logger theLog = Logger.getLogger(SampleDoubleEntry.class);

    private String label;

    List<DoubleValueEntry> values;

    private String startDateAsString;
    private String endDateAsString;

    private String date;

    private Long startDate;
    private Long endDate;

    private Boolean hasDrilldown = true;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStartDateAsString() {
        return startDateAsString;
    }

    public void setStartDateAsString(String startDate) {
        this.startDateAsString = startDate;
    }

    public String getEndDateAsString() {
        return endDateAsString;
    }

    public void setEndDateAsString(String endDate) {
        this.endDateAsString = endDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public List<DoubleValueEntry> getValues() {
        return values;
    }

    public void setValues(List<DoubleValueEntry> value) {
        this.values = value;
    }

    public void addSeriesDoubleEntry(DoubleValueEntry ent) {
        if (values == null) {
            values = new ArrayList<DoubleValueEntry>();
        }
        values.add(ent);
    }

    public Boolean getHasDrilldown() {
        return hasDrilldown;
    }

    public void setHasDrilldown(Boolean hasDrilldown) {
        this.hasDrilldown = hasDrilldown;
    }
}
