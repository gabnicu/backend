package com.radcom.qinisght.metrics.model.diameter.errcodedistrib;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DbEntryTable {
    private String TE0;
    private String TE1;
    private Long TE2;
    private Long TE3;
    private Long TE4;
    private Long TE5;
    private Long TE6;
    private Long TE7;
    private Long TE8;
    private Long TE9;
    private Long TE10;

    @JsonProperty("TE0")
    public String getTE0() {
        return TE0;
    }

    @JsonProperty("TE0")
    public void setTE0(String tE0) {
        TE0 = tE0;
    }

    @JsonProperty("TE1")
    public String getTE1() {
        return TE1;
    }

    @JsonProperty("TE1")
    public void setTE1(String tE1) {
        TE1 = tE1;
    }

    @JsonProperty("TE2")
    public Long getTE2() {
        return TE2;
    }

    @JsonProperty("TE2")
    public void setTE2(Long tE2) {
        TE2 = tE2;
    }

    @JsonProperty("TE3")
    public Long getTE3() {
        return TE3;
    }

    @JsonProperty("TE3")
    public void setTE3(Long tE3) {
        TE3 = tE3;
    }

    @JsonProperty("TE4")
    public Long getTE4() {
        return TE4;
    }

    @JsonProperty("TE4")
    public void setTE4(Long tE4) {
        TE4 = tE4;
    }

    @JsonProperty("TE5")
    public Long getTE5() {
        return TE5;
    }

    @JsonProperty("TE5")
    public void setTE5(Long tE5) {
        TE5 = tE5;
    }

    @JsonProperty("TE6")
    public Long getTE6() {
        return TE6;
    }

    @JsonProperty("TE6")
    public void setTE6(Long tE6) {
        TE6 = tE6;
    }

    @JsonProperty("TE7")
    public Long getTE7() {
        return TE7;
    }

    @JsonProperty("TE7")
    public void setTE7(Long tE7) {
        TE7 = tE7;
    }

    @JsonProperty("TE8")
    public Long getTE8() {
        return TE8;
    }

    @JsonProperty("TE8")
    public void setTE8(Long tE8) {
        TE8 = tE8;
    }

    @JsonProperty("TE9")
    public Long getTE9() {
        return TE9;
    }

    @JsonProperty("TE9")
    public void setTE9(Long tE9) {
        TE9 = tE9;
    }

    @JsonProperty("TE10")
    public Long getTE10() {
        return TE10;
    }

    @JsonProperty("TE10")
    public void setTE10(Long tE10) {
        TE10 = tE10;
    }


}
