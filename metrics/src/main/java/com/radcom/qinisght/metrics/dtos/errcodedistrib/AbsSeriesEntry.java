package com.radcom.qinisght.metrics.dtos.errcodedistrib;

public abstract class AbsSeriesEntry {

    private String fragmentValue;

    public String getFragmentValue() {
        return fragmentValue;
    }

    public void setFragmentValue(String cause) {
        this.fragmentValue = cause;
    }
}
