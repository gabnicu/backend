package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao;

import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryTable;

import java.util.List;

public interface DaoTable {
    List<DbEntryTable> getData(String startDate, String endDate, int delta, String nes, boolean isMoreThan2days, String fragmentValue, String dashletType);
}
