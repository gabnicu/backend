package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.DoubleValueEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleDoubleEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryRetransmissionRatio;
import com.radcom.qinsight.utils.time.TimeUtils;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import java.util.*;

@RequestScoped
public class RetransmissionRatioDataAgreggator {

    private static final Logger theLog = Logger.getLogger(RetransmissionRatioDataAgreggator.class);

    public List<SampleDoubleEntry> aggregateData(List<DbEntryRetransmissionRatio> lstDbEntries, AggregationType aggType, int delta) {

        if (aggType.equals(AggregationType.AGG_TYPE_PER_5MINS))
            return aggregateDataPer5Mins(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_HOUR))
            return aggregateDataPerHour(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_DAY))
            return aggregateDataPerDay(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_MONTH))
            return aggregateDataPerMonth(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_QUARTER))
            return aggregateDataPerQuarter(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_YEAR))
            return aggregateDataPerYear(lstDbEntries, delta);

        return aggregateDataPerYear(lstDbEntries, delta);
    }

    private List<SampleDoubleEntry> sortMapByDate(Map<String, SampleDoubleEntry> map) {
        theLog.error("sortMapByDate");
        List<SampleDoubleEntry> lstRet = new ArrayList<>();
        SampleEntryAggregator theSamplesAggregator = new SampleEntryAggregator();
        // SORT entries in list by DATE
        SortedSet<String> keys = new TreeSet<>(map.keySet());
        for (String key : keys) {
            SampleDoubleEntry smpl = map.get(key);
            // Do aggregation by fragmentValue
            theSamplesAggregator.aggregateDataByAverage(smpl);
            lstRet.add(smpl);
        }
        return lstRet;
    }

    private List<SampleDoubleEntry> aggregateDataPer5Mins(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPer5Mins");
        Map<String, SampleDoubleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio dbEntry : lstDbEntries) {

            // Key is of type "2018-05-29 00:00"
            String date = dbEntry.getTE0().substring(0, 16);
            date = date.replaceAll("T", " ");
            SampleDoubleEntry val = map.get(date);
            if (val == null) {
                val = new SampleDoubleEntry();
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);
                // Label on chart is of type 19:15, 19:20, 19:25
                val.setLabel(date.substring(11, 16));

                // Date is of type "05/29/2018 19:15"
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());

                val.setStartDateAsString(date + ":00");
                String szStart = val.getStartDateAsString();
                long lgStart = TimeUtils.fromISO8601ToUTCAsMillisec(szStart);
                lgStart += delta * 60 * 1000L;
                long lgEnd = lgStart + 5 * 60 * 1000L;
                val.setEndDate(lgEnd);
                val.setStartDate(lgStart);

                val.setHasDrilldown(false);

                map.put(date, val);
            } else {
                Integer te1Value = dbEntry.getTE1();
                Integer te8Value = dbEntry.getTE8();
                if (te1Value == null || te8Value == null) {
                    theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
                    continue;
                }
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);
            }

        }
        return sortMapByDate(map);
    }

    private List<SampleDoubleEntry> aggregateDataPerHour(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerHour");
        Map<String, SampleDoubleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio dbEntry : lstDbEntries) {
            // Key is of type 2018-05-29 00, 2018-05-29 01, 2018-05-29 11
            String date = dbEntry.getTE0().substring(0, 13);
            date = date.replaceAll("T", " ");
            SampleDoubleEntry val = map.get(date);
            if (val == null) {
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val = new SampleDoubleEntry();
                val.addSeriesDoubleEntry(ent);
                // Label on chart is of type 19:00, 20:00, 21:00
                val.setLabel(date.substring(11, 13) + ":00");

                // Date is of type 05/29/2018 19:00, 05/29/2018 20:00
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
                val.setEndDateAsString(date + ":59:59");
                val.setStartDateAsString(date + ":00:00");

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(date + ":00:00", TimeUtils.getGmtStringFromDeltaInMinutes(delta)) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(date + ":59:59", TimeUtils.getGmtStringFromDeltaInMinutes(delta)) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(date, val);
            } else {
                Integer te1Value = dbEntry.getTE1();
                Integer te8Value = dbEntry.getTE8();
                if (te1Value == null || te8Value == null) {
                    theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
                    continue;
                }
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);
            }

        }
        return sortMapByDate(map);
    }

    private List<SampleDoubleEntry> aggregateDataPerDay(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerDay");
        Map<String, SampleDoubleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio dbEntry : lstDbEntries) {
            String date = dbEntry.getTE0().substring(0, 10);
            SampleDoubleEntry val = map.get(date);
            if (val == null) {
                val = new SampleDoubleEntry();
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);

                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMDD(date));
                val.setDate(TimeUtils.generateAngloSaxonDate(date));
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfDay(date));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfDay(date));

                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), TimeUtils.getGmtStringFromDeltaInMinutes(delta));
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), TimeUtils.getGmtStringFromDeltaInMinutes(delta)) + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(date, val);
            } else {
                Integer te1Value = dbEntry.getTE1();
                Integer te8Value = dbEntry.getTE8();
                if (te1Value == null || te8Value == null) {
                    theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
                    continue;
                }
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);
                theLog.error("date: " + date + " retransmissionRatio " + retransmissionRatio);
            }

        }
        return sortMapByDate(map);
    }

    private List<SampleDoubleEntry> aggregateDataPerMonth(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerMonth");
        Map<String, SampleDoubleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio dbEntry : lstDbEntries) {
            String szYearAndMonth = dbEntry.getTE0().substring(0, 7);
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            SampleDoubleEntry val = map.get(szYearAndMonth);
            if (val == null) {
                val = new SampleDoubleEntry();
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);

                // Label must be of type "May 2018"
                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMYYYY(dbEntry.getTE0().substring(0, 11)));

                // Date must be of type "05/01/2018", 06/01/2018, 07/01/2018, etc. Always first
                // day of month
                val.setDate(TimeUtils.generateAngloSaxonDate(dbEntry.getTE0().substring(0, 7) + "-01"));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfMonth(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfMonth(szYearAndMonthAndDay));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(szYearAndMonth, val);
            } else {
                Integer te1Value = dbEntry.getTE1();
                Integer te8Value = dbEntry.getTE8();
                if (te1Value == null || te8Value == null) {
                    theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
                    continue;
                }
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);
            }

        }
        return sortMapByDate(map);
    }

    private List<SampleDoubleEntry> aggregateDataPerQuarter(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerQuarter");
        Map<String, SampleDoubleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio dbEntry : lstDbEntries) {
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            String quarter = TimeUtils.getQuarterFromDate(szYearAndMonthAndDay);
            SampleDoubleEntry val = map.get(quarter);
            if (val == null) {
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val = new SampleDoubleEntry();
                val.addSeriesDoubleEntry(ent);

                // Label must be of type "Quarter 1, 2018"
                val.setLabel(quarter);

                // Date must be of type "01/01/2018", "04/01/2018"
                val.setDate(TimeUtils.generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(szYearAndMonthAndDay));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfQuarter(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfQuarter(szYearAndMonthAndDay));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(quarter, val);
            } else {
                Integer te1Value = dbEntry.getTE1();
                Integer te8Value = dbEntry.getTE8();
                if (te1Value == null || te8Value == null) {
                    theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
                    continue;
                }
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);
            }

        }
        return sortMapByDate(map);
    }

    private List<SampleDoubleEntry> aggregateDataPerYear(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerYear");
        Map<String, SampleDoubleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio dbEntry : lstDbEntries) {
            String szYear = dbEntry.getTE0().substring(0, 4);
            SampleDoubleEntry val = map.get(szYear);
            if (val == null) {
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val = new SampleDoubleEntry();
                val.addSeriesDoubleEntry(ent);
                theLog.error("szYear " + szYear + " retransmissionRatio " + retransmissionRatio);

                val.setLabel(szYear);

                val.setDate("01/01/" + szYear);
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfYear(szYear));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfYear(szYear));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(szYear, val);
            } else {
                Integer te1Value = dbEntry.getTE1();
                Integer te8Value = dbEntry.getTE8();
                if (te1Value == null || te8Value == null) {
                    theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
                    continue;
                }
                Integer totalRetransmission = dbEntry.getTE7();
                Integer retransmissionCounter = dbEntry.getTE8();
                double retransmissionRatio = ((double) totalRetransmission) / retransmissionCounter * 100;
                DoubleValueEntry ent = new DoubleValueEntry(retransmissionRatio, "");
                val.addSeriesDoubleEntry(ent);

                theLog.error("szYear " + szYear + " retransmissionRatio " + retransmissionRatio);
            }
        }

        Set<String> keys = map.keySet();
        for (String szYear : keys) {
            SampleDoubleEntry val = map.get(szYear);
            theLog.error("szYear " + szYear + " entries " + val.getValues().size());
        }
        return sortMapByDate(map);
    }
}
