package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl;

import com.radcom.qinisght.metrics.business.DashletType;
import com.radcom.qinisght.metrics.business.SqlProvider;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryRetransmissionRatio;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoDbEntries;
import com.radcom.qinsight.nes.model.DbEntryNetworkElements;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@ProviderVertica
public class DaoDataFromDb implements DaoDbEntries {

    private static final Logger theLog = Logger.getLogger(DaoDataFromDb.class);

    @Resource(mappedName = "java:jboss/VerticaDS")
    private DataSource ds;

    @Inject
    private SqlProvider sqlProvider;

    private String processDate(String initialString) {

        if (initialString.length() < 19) {
            return null;
        }
        if (initialString.length() > 19) {
            initialString = initialString.substring(0, 19);
        }
        String sz = initialString.replaceAll("T", " ");
        sz = sz + ".0000000";
        return sz;
    }

    @Override
    public List<DbEntryRetransmissionRatio> getData(String startDate, String endDate, int deltaUTC, String nes,
                                                    boolean isMoreThan2days) {
        theLog.info("getData()");
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String st = processDate(startDate);
        String end = processDate(endDate);

        String query = getSql(isMoreThan2days);

        query = query.replaceAll("START_DATE", st);
        query = query.replaceAll("END_DATE", end);
        query = query.replaceAll("SOURCE_NES", nes);
        query = query.replaceAll("DEST_NES", nes);
        query = query.replaceAll("DELTA", "" + deltaUTC);

        theLog.info("Executing SQL in getData: " + query);

        List<DbEntryRetransmissionRatio> retransmissionRatios = new ArrayList<>();

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                DbEntryRetransmissionRatio retransmissionRatioEntry = new DbEntryRetransmissionRatio();

                Timestamp date = rs.getTimestamp("TE0");
                int requestCount = rs.getInt("TE1");
                long totalDuration = rs.getLong("TE2");
                int averageCounter = rs.getInt("TE3");
                int peakResponseTime = rs.getInt("TE4");
                int worst = rs.getInt("TE5");
                int sumRetransmissionRatio = rs.getInt("TE6");
                int totalRetransmission = rs.getInt("TE7");
                int retransmissionCounter = rs.getInt("TE8");
                int countRetransmissionRatio = rs.getInt("TE18");

                retransmissionRatioEntry.setTE0(date.toString());
                retransmissionRatioEntry.setTE1(requestCount);
                retransmissionRatioEntry.setTE2(totalDuration);
                retransmissionRatioEntry.setTE3(averageCounter);
                retransmissionRatioEntry.setTE4(peakResponseTime);
                retransmissionRatioEntry.setTE5(worst);
                retransmissionRatioEntry.setTE6(sumRetransmissionRatio);
                retransmissionRatioEntry.setTE7(totalRetransmission);
                retransmissionRatioEntry.setTE8(retransmissionCounter);
                retransmissionRatioEntry.setTE18(countRetransmissionRatio);
                retransmissionRatios.add(retransmissionRatioEntry);
            }

            theLog.error(
                    "=============================================================== Retransmission Ratio read entries #"
                            + retransmissionRatios.size());
        } catch (SQLException sqlEx) {
            theLog.error("SQL Exception", sqlEx);
            return null;
        } catch (Exception ex) {
            theLog.error("General Exception", ex);
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception ex2) {
                theLog.error("Exception closing connection", ex2);
            }
        }

        theLog.info("Read Retransmission Ratio From Verica. Number of lines read is: " + retransmissionRatios.size());
        return retransmissionRatios;
    }

    private String getSql(boolean isMoreThan2days) {

        String query;

        if (isMoreThan2days) {
            query = sqlProvider.getQuery("SQL_GET_MORE_THAN_2_DAYS");
            theLog.error("sql SQL_GET_MORE_THAN_2_DAYS");
        } else {
            query = sqlProvider.getQuery("SQL_GET_LESS_THAN_2_DAYS");
            theLog.error("sql SQL_GET_LESS_THAN_2_DAYS");
        }
        return query;
    }

    private String getSqlForNes(boolean isMoreThan2days, DashletType dashletType) {

        String query = null;

        switch (dashletType) {
            case DIAMETER_AVERAGE_RESPONSE_TIME:
                if (isMoreThan2days) {
                    query = sqlProvider.getQuery("SQL_SOURCE_NES_AVERAGE_RESPONSE_TIME_MORE_THAN_2_DAYS");
                    theLog.info("DIAMETER_AVERAGE_RESPONSE_TIME, isMoreThan2days " + isMoreThan2days);
                } else {
                    query = sqlProvider.getQuery("SQL_SOURCE_NES_AVERAGE_RESPONSE_TIME_LESS_THAN_2_DAYS");
                    theLog.info("DIAMETER_AVERAGE_RESPONSE_TIME, isMoreThan2days " + isMoreThan2days);
                }
                break;
            case DIAMETER_ERROR_CODE_DISTRIBUTION:
                if (isMoreThan2days) {
                    query = sqlProvider.getQuery("SQL_SOURCE_NES_DIAMETER_ERROR_CODE_DISTRIBUTION_MORE_THAN_2_DAYS");
                    theLog.info("DIAMETER_ERROR_CODE_DISTRIBUTION, isMoreThan2days " + isMoreThan2days);
                } else {
                    query = sqlProvider.getQuery("SQL_SOURCE_NES_DIAMETER_ERROR_CODE_DISTRIBUTION_LESS_THAN_2_DAYS");
                    theLog.info("DIAMETER_ERROR_CODE_DISTRIBUTION, isMoreThan2days " + isMoreThan2days);
                }
                break;
            default:
                break;
        }
        if (query == null) {
            theLog.error("QUERY NULL !!!");
        }
        return query;
    }

    public List<DbEntryNetworkElements> getDataForNes(String startDate, String endDate, int deltaUTC, String nes,
                                                      boolean isMoreThan2days, DashletType dashletType) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String st = processDate(startDate);
        String end = processDate(endDate);

        String query = getSqlForNes(isMoreThan2days, dashletType);

        query = query.replaceAll("START_DATE", st);
        query = query.replaceAll("END_DATE", end);
        query = query.replaceAll("SOURCE_NES", nes);
        query = query.replaceAll("DEST_NES", nes);
        query = query.replaceAll("DELTA", "" + deltaUTC);

        theLog.info("START_DATE: " + st);
        theLog.info("END_DATE: " + end);
        theLog.info("SOURCE_NES: " + nes);
        theLog.info("DEST_NES: " + nes);
        theLog.info("DELTA: " + deltaUTC);

        theLog.info("Executing SQL in getDataForNes: " + query);

        List<DbEntryNetworkElements> retransmissionRatios = new ArrayList<>();

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                DbEntryNetworkElements networkElements = new DbEntryNetworkElements();

                String destName = rs.getString("TE0");
                String sourceName = rs.getString("TE1");
                int requestCount = rs.getInt("TE2");
                int responseCount = rs.getInt("TE3");
                int successResponseCount = rs.getInt("TE4");
                int failedResponseCount = rs.getInt("TE5");
                int timeouts = rs.getInt("TE6");
                int retransmissionCount = rs.getInt("TE7");
                int totalDuration = rs.getInt("TE8");
                int averageCounter = rs.getInt("TE9");
                int peakResponseTime = rs.getInt("TE10");

                networkElements.setTE0(destName);
                networkElements.setTE1(sourceName);
                networkElements.setTE2(requestCount);
                networkElements.setTE3(responseCount);
                networkElements.setTE4(successResponseCount);
                networkElements.setTE5(failedResponseCount);
                networkElements.setTE6(timeouts);
                networkElements.setTE7(retransmissionCount);
                networkElements.setTE8(totalDuration);
                networkElements.setTE9(averageCounter);
                networkElements.setTE10(peakResponseTime);
                retransmissionRatios.add(networkElements);
            }

            theLog.error(
                    "=============================================================== Retransmission Ratio read entries #"
                            + retransmissionRatios.size());
        } catch (SQLException sqlEx) {
            theLog.error("SQL Exception", sqlEx);
            return null;
        } catch (Exception ex) {
            theLog.error("General Exception", ex);
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception ex2) {
                theLog.error("Exception closing connection", ex2);
            }
        }

        theLog.info("Read Retransmission Ratio From Verica. Number of lines read is: " + retransmissionRatios.size());
        return retransmissionRatios;
    }
}
