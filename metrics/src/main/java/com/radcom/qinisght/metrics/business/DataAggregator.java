package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.IntValueEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinsight.utils.time.TimeUtils;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import java.util.*;

@RequestScoped
public class DataAggregator {

    private static final Logger theLog = Logger.getLogger(DataAggregator.class);

    public List<SampleEntry> aggregateData(List<DbEntry> lstDbEntries, AggregationType aggType, int delta) {

        if (aggType.equals(AggregationType.AGG_TYPE_PER_5MINS)) {
            return aggregateDataPer5Mins(lstDbEntries, delta);
        }
        if (aggType.equals(AggregationType.AGG_TYPE_PER_HOUR)) {
            return aggregateDataPerHour(lstDbEntries, delta);
        }
        if (aggType.equals(AggregationType.AGG_TYPE_PER_DAY)) {
            return aggregateDataPerDay(lstDbEntries, delta);
        }
        if (aggType.equals(AggregationType.AGG_TYPE_PER_MONTH)) {
            return aggregateDataPerMonth(lstDbEntries, delta);
        }
        if (aggType.equals(AggregationType.AGG_TYPE_PER_QUARTER)) {
            return aggregateDataPerQuarter(lstDbEntries, delta);
        }
        if (aggType.equals(AggregationType.AGG_TYPE_PER_YEAR)) {
            return aggregateDataPerYear(lstDbEntries, delta);
        }
        return aggregateDataPerYear(lstDbEntries, delta);
    }

    private List<SampleEntry> sortMapByDate(Map<String, SampleEntry> map) {
        List<SampleEntry> lstRet = new ArrayList<>();
        SampleEntryAggregator theSamplesAggregator = new SampleEntryAggregator();
        // SORT entries in list by DATE
        SortedSet<String> keys = new TreeSet<>(map.keySet());
        for (String key : keys) {
            SampleEntry smpl = map.get(key);
            // Do aggregation by fragmentValue
            theSamplesAggregator.aggregateDataBySum(smpl);
            lstRet.add(smpl);
        }
        return lstRet;
    }

    private List<SampleEntry> aggregateDataPer5Mins(List<DbEntry> lstDbEntries, int delta) {
        Set<String> errors = new HashSet<>();
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {

            // Key is of type "2018-05-29 00:00"
            String date = dbEntry.getTE0().substring(0, 16);
            date = date.replaceAll("T", " ");
            SampleEntry val = map.get(date);
            errors.add(dbEntry.getHC0E1());
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
                // Label on chart is of type 19:15, 19:20, 19:25
                val.setLabel(date.substring(11, 16));

                // Date is of type "05/29/2018 19:15"
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());

                val.setStartDateAsString(date + ":00");
                String szStart = val.getStartDateAsString();
                long lgStart = TimeUtils.fromISO8601ToUTCAsMillisec(szStart);
                lgStart += delta * 60 * 1000L;
                long lgEnd = lgStart + 5 * 60 * 1000L;
                val.setEndDate(lgEnd);
                val.setStartDate(lgStart);

                val.setHasDrilldown(false);

                map.put(date, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }

        Set<String> keys = new HashSet<>(map.keySet());

        for (String key : keys) {
            SampleEntry entry = map.get(key);

            for (String error : errors) {
                IntValueEntry newEntry = new IntValueEntry(0, error);
                entry.addSeriesEntry(newEntry);
                map.put(key, entry);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerHour(List<DbEntry> lstDbEntries, int delta) {
        Set<String> errors = new HashSet<>();
        theLog.error("aggregateDataPerHour");
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            // Key is of type 2018-05-29 00, 2018-05-29 01, 2018-05-29 11
            String date = dbEntry.getTE0().substring(0, 13);
            date = date.replaceAll("T", " ");
            SampleEntry val = map.get(date);
            errors.add(dbEntry.getHC0E1());
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
                // Label on chart is of type 19:00, 20:00, 21:00
                val.setLabel(date.substring(11, 13) + ":00");

                // Date is of type 05/29/2018 19:00, 05/29/2018 20:00
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());

                val.setStartDateAsString(date + ":00:00");
                String szStart = val.getStartDateAsString();
                long lgStart = TimeUtils.fromISO8601ToUTCAsMillisec(szStart);
                long lgEnd = lgStart + 3600 * 1000L;
                String szEnd = TimeUtils.toISO8601FromUTC(lgEnd);
                val.setEndDateAsString(szEnd);

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(date + ":00:00", TimeUtils.getGmtStringFromDeltaInMinutes(delta)) + longDelta;
                long endDate = startDate + 3600 * 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(date, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }

        Set<String> keys = new HashSet<>(map.keySet());

        for (String key : keys) {
            SampleEntry entry = map.get(key);
            for (String error : errors) {
                IntValueEntry newEntry = new IntValueEntry(0, error);
                entry.addSeriesEntry(newEntry);
                map.put(key, entry);
            }
        }

        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerDay(List<DbEntry> lstDbEntries, int delta) {
        theLog.info("aggregateDataPerDay");
        Set<String> errors = new HashSet<>();
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            String date = dbEntry.getTE0().substring(0, 10);
            SampleEntry val = map.get(date);
            errors.add(dbEntry.getHC0E1());
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMDD(date));
                val.setDate(TimeUtils.generateAngloSaxonDate(date));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfDay(date));
                String szStart = val.getStartDateAsString();
                long lgStart = TimeUtils.fromISO8601ToUTCAsMillisec(szStart);
                long lgEnd = lgStart + 24 * 3600 * 1000L;
                String szEnd = TimeUtils.toISO8601FromUTC(lgEnd);
                val.setEndDateAsString(szEnd);

                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), TimeUtils.getGmtStringFromDeltaInMinutes(delta));
                long endDate = startDate + 24 * 3600 * 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(date, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }

        Set<String> keys = new HashSet<>(map.keySet());

        for (String key : keys) {
            SampleEntry entry = map.get(key);
            for (String error : errors) {
                IntValueEntry newEntry = new IntValueEntry(0, error);
                entry.addSeriesEntry(newEntry);
                map.put(key, entry);
            }
        }

        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerMonth(List<DbEntry> lstDbEntries, int delta) {
        theLog.info("aggregateDataPerMonth");
        Set<String> errors = new HashSet<>();
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            String szYearAndMonth = dbEntry.getTE0().substring(0, 7);
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            SampleEntry val = map.get(szYearAndMonth);
            errors.add(dbEntry.getHC0E1());
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                // Label must be of type "May 2018"
                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMYYYY(dbEntry.getTE0().substring(0, 11)));

                // Date must be of type "05/01/2018", 06/01/2018, 07/01/2018, etc. Always first
                // day of month
                val.setDate(TimeUtils.generateAngloSaxonDate(dbEntry.getTE0().substring(0, 7) + "-01"));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfMonth(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfMonth(szYearAndMonthAndDay));

                long longDelta = delta * 60 * 1000L;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta;
                endDate += 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                theLog.info("label: " + val.getLabel());
                theLog.info("start date string: " + val.getStartDateAsString() + " end date str: " + val.getEndDateAsString());
                theLog.info("start date long: " + val.getStartDate() + " end date long: " + val.getEndDate());

                map.put(szYearAndMonth, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }

        Set<String> keys = new HashSet<>(map.keySet());

        for (String key : keys) {
            SampleEntry entry = map.get(key);
            for (String error : errors) {
                IntValueEntry newEntry = new IntValueEntry(0, error);
                entry.addSeriesEntry(newEntry);
                map.put(key, entry);
            }
        }

        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerQuarter(List<DbEntry> lstDbEntries, int delta) {
        Map<String, SampleEntry> map = new HashMap<>();
        Set<String> errors = new HashSet<>();
        for (DbEntry dbEntry : lstDbEntries) {
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            String quarter = TimeUtils.getQuarterFromDate(szYearAndMonthAndDay);
            SampleEntry val = map.get(quarter);
            errors.add(dbEntry.getHC0E1());
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                // Label must be of type "Quarter 1, 2018"
                val.setLabel(quarter);

                // Date must be of type "01/01/2018", "04/01/2018"
                val.setDate(TimeUtils.generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(szYearAndMonthAndDay));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfQuarter(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfQuarter(szYearAndMonthAndDay));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                theLog.info("aggregateDataPerQuarter NEW PUT quarter=" + quarter + " val=" + val);

                map.put(quarter, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(newVal, dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }

        Set<String> keys = new HashSet<>(map.keySet());

        for (String key : keys) {
            SampleEntry entry = map.get(key);
            for (String error : errors) {
                IntValueEntry newEntry = new IntValueEntry(0, error);
                entry.addSeriesEntry(newEntry);
                map.put(key, entry);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerYear(List<DbEntry> lstDbEntries, int delta) {
        Set<String> errors = new HashSet<>();
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            String szYear = dbEntry.getTE0().substring(0, 4);
            SampleEntry val = map.get(szYear);
            errors.add(dbEntry.getHC0E1());
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                val.setLabel(szYear);

                val.setDate("01/01/" + szYear);
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfYear(szYear));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfYear(szYear));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(szYear, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }

        Set<String> keys = new HashSet<>(map.keySet());

        for (String key : keys) {
            SampleEntry entry = map.get(key);
            for (String error : errors) {
                IntValueEntry newEntry = new IntValueEntry(0, error);
                entry.addSeriesEntry(newEntry);
                map.put(key, entry);
            }
        }
        return sortMapByDate(map);
    }
}