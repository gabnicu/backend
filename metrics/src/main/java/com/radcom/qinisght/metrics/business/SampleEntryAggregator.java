package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.DoubleValueEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.IntValueEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleDoubleEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import org.apache.log4j.Logger;

import java.util.*;

class SampleEntryAggregator {

    private static final Logger theLog = Logger.getLogger(SampleEntryAggregator.class);

    void aggregateDataBySum(SampleEntry dateSampleEntry) {
        List<IntValueEntry> lst = dateSampleEntry.getValues();

        List<IntValueEntry> newList = new ArrayList<>();
        Map<String, Integer> map = new HashMap<>();
        for (IntValueEntry intValueEntry : lst) {
            String fragmentValue = intValueEntry.getFragmentValue();
            if (fragmentValue == null) {
                continue;
            }

            Integer oldVal = map.get(fragmentValue);
            if (oldVal == null) {
                Integer newVal = intValueEntry.getValue();
                map.put(fragmentValue, newVal);
            } else {
                oldVal += intValueEntry.getValue();
                map.put(fragmentValue, oldVal);
            }
        }

        Set<String> theKeys = map.keySet();
        for (String fragmentValue : theKeys) {
            newList.add(new IntValueEntry(map.get(fragmentValue), fragmentValue));
        }

        // Fixed order of values
        Collections.sort(newList, new ComparatorSeriesEntryFixedOrder());
        dateSampleEntry.setValues(newList);
    }

    void aggregateDataByAverage(SampleDoubleEntry dateSampleEntry) {
        theLog.info("aggregateDataByAverage");
        int counter = 0;
        List<DoubleValueEntry> lst = dateSampleEntry.getValues();

        List<DoubleValueEntry> newList = new ArrayList<>();
        Map<String, Double> map = new HashMap<>();
        for (DoubleValueEntry seriesEntry : lst) {
            String fragmentValue = "";
            Double oldVal = map.get(fragmentValue);
            if (oldVal == null) {
                if (seriesEntry.getValue() == null) {
                    map.put(fragmentValue, null);
                } else {
                    Double newVal = seriesEntry.getValue();
                    map.put(fragmentValue, newVal);
                }
                counter++;
            } else {
                if (seriesEntry.getValue() == null) {
                    counter++;
                    continue;
                }
                oldVal += seriesEntry.getValue();
                map.put(fragmentValue, oldVal);
                counter++;
            }
        }

        Set<String> theKeys = map.keySet();
        for (String fragmentValue : theKeys) {
            if (map.get(fragmentValue) == null) {
                newList.add(new DoubleValueEntry(null, fragmentValue));
                theLog.info("added: " + fragmentValue);
            } else {
                newList.add(new DoubleValueEntry(map.get(fragmentValue) / counter, fragmentValue));
            }
        }

        // FIxed order of values
//		Collections.sort(newList, new ComparatorSeriesEntryFixedOrder());
        dateSampleEntry.setValues(newList);
    }

    void aggregateDataByIndividual(SampleEntry dateSampleEntry) {
        theLog.info("aggregateDataByIndividual");
        List<IntValueEntry> lst = dateSampleEntry.getValues();
        dateSampleEntry.setValues(lst);
    }

    void aggregateDataByIndividual(SampleDoubleEntry dateSampleEntry) {
        theLog.info("aggregateDataByIndividual");
        List<DoubleValueEntry> lst = dateSampleEntry.getValues();
        dateSampleEntry.setValues(lst);
    }

    void aggregateDataByMax(SampleEntry dateSampleEntry) {
        theLog.info("aggregateDataByMax");
        List<IntValueEntry> lst = dateSampleEntry.getValues();
        int max = 0;
        for (IntValueEntry i : lst) {
            max = Math.max(max, i.getValue());
        }
        lst.clear();
        lst.add(new IntValueEntry(max, ""));
        dateSampleEntry.setValues(lst);
    }
}
