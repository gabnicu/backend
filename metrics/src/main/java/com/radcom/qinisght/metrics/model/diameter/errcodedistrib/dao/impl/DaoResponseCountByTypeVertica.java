package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl;

import com.radcom.qinisght.metrics.business.SqlProvider;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryResponseCountByType;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoResponseCountByType;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@ProviderVertica
public class DaoResponseCountByTypeVertica implements DaoResponseCountByType {
	
	private static final Logger theLog = Logger.getLogger(DaoResponseCountByTypeVertica.class);

	@Resource(mappedName = "java:jboss/VerticaDS")
	private DataSource ds;
	
	@Inject
	private SqlProvider sqlProvider;

	private String processDate(String initialString) {
		if (initialString.length() < 19)
			return null;
		if (initialString.length() > 19)
			initialString = initialString.substring(0, 19);
		String sz = initialString.replaceAll("T", " ");
		sz = sz + ".0000000";
		return sz;
	}

	@Override
	public List<DbEntryResponseCountByType> getData(String startDate, String endDate, int deltaUTC, String nes, boolean isMoreThan2days) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String st = processDate(startDate);
		String end = processDate(endDate);

		String query = getSql(isMoreThan2days);

		query = query.replaceAll("START_DATE", st);
		query = query.replaceAll("END_DATE", end);
		query = query.replaceAll("SOURCE_NES", nes);
		query = query.replaceAll("DEST_NES", nes);
		query = query.replaceAll("DELTA", "" + deltaUTC);

		theLog.info("Executing SQL: " + query);

		List<DbEntryResponseCountByType> responseCount = new ArrayList<>();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				DbEntryResponseCountByType responseCountEntry = new DbEntryResponseCountByType();

				Timestamp date = rs.getTimestamp("TE0");
				String releaseType = rs.getString("TE1");
				int responseCountByType = rs.getInt("TE2");
				int averageCounter = rs.getInt("TE3");
				int worst = rs.getInt("TE4");
				String description = rs.getString("HC0E0");

				responseCountEntry.setTE0(date.toString());
				responseCountEntry.setTE1(releaseType);
				responseCountEntry.setTE2(responseCountByType);
				responseCountEntry.setTE3(averageCounter);
				responseCountEntry.setTE4(worst);
				responseCountEntry.setHC0E0(description);
				responseCount.add(responseCountEntry);
			}

			theLog.error(
					"=============================================================== Retransmission Ratio read entries #"
							+ responseCount.size());
		} catch (SQLException sqlEx) {
			theLog.error("SQL Exception", sqlEx);
			return null;
		} catch (Exception ex) {
			theLog.error("General Exception", ex);
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (Exception ex2) {
				theLog.error("Exception closing connection", ex2);
			}
		}

		theLog.info("Read Retransmission Ratio From Verical. Number of lines read is: " + responseCount.size());
		return responseCount;
	}
	
	private String getSql(boolean isMoreThan2days) {

		String query;

		if (isMoreThan2days) {
			query = sqlProvider.getQuery("SQL_GET_RESPONSE_COUNT_BY_TYPE_MORE_THAN_2_DAYS");
			theLog.info("SQL_GET_RESPONSE_COUNT_BY_TYPE_MORE_THAN_2_DAYS");
		} else {
			query = sqlProvider.getQuery("SQL_GET_RESPONSE_COUNT_BY_TYPE_LESS_THAN_2_DAYS");
			theLog.info("SQL_GET_RESPONSE_COUNT_BY_TYPE_LESS_THAN_2_DAYS");
		}
        if (query == null) {
		    theLog.error("NULL QUERY RETRIEVED FROM SQL FILE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
		return query;
	}

}
