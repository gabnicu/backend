package com.radcom.qinisght.metrics.model.diameter.errcodedistrib;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DbEntryResponseCountByType {

    private String TE0;
    private String TE1;
    private Integer TE2;
    private Integer TE3;
    private Integer TE4;
    private String HC0E0;

    @JsonProperty("TE0")
    public String getTE0() {
        return TE0;
    }

    @JsonProperty("TE0")
    public void setTE0(String tE0) {
        TE0 = tE0;
    }

    @JsonProperty("TE1")
    public String getTE1() {
        return TE1;
    }

    @JsonProperty("TE1")
    public void setTE1(String tE1) {
        TE1 = tE1;
    }

    @JsonProperty("TE2")
    public Integer getTE2() {
        return TE2;
    }

    @JsonProperty("TE2")
    public void setTE2(Integer tE2) {
        TE2 = tE2;
    }

    @JsonProperty("TE3")
    public Integer getTE3() {
        return TE3;
    }

    @JsonProperty("TE3")
    public void setTE3(Integer tE3) {
        TE3 = tE3;
    }

    @JsonProperty("TE4")
    public Integer getTE4() {
        return TE4;
    }

    @JsonProperty("TE4")
    public void setTE4(Integer tE4) {
        TE4 = tE4;
    }

    @JsonProperty("HC0E0")
    public String getHC0E0() {
        return HC0E0;
    }

    @JsonProperty("HC0E0")
    public void setHC0E0(String hC0E0) {
        HC0E0 = hC0E0;
    }


}
