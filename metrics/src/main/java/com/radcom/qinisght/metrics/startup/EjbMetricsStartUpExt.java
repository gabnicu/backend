package com.radcom.qinisght.metrics.startup;


import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;

@Startup
@Singleton
public class EjbMetricsStartUpExt {

    private static final Logger theLog = Logger.getLogger(EjbMetricsStartUpExt.class);

    @Resource
    private TimerService timerService;

    @Inject
    private NetworkElementsResolver resolver;

    @PostConstruct
    void init() {
        theLog.info("EjbMetricsStartUpExt init()");
        setTheTimer(15 * 60 * 1000);
    }

    @PreDestroy
    void drawDown() {
        theLog.info("EjbMetricsStartUp drawDown()");
    }


    private void execFunction() {
        resolver.initiateNetworkElementsLoad();
    }

    private void setTheTimer(long intervalDuration) {
        TimerConfig conf = new TimerConfig();
        conf.setPersistent(false);
        Timer timer = timerService.createIntervalTimer(0, intervalDuration, conf);
    }

    @Timeout
    public void programmaticTimeout(Timer timer) {
        execFunction();
    }
}
