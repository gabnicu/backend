package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.IntValueEntry;

import java.util.Comparator;

public class ComparatorSeriesEntryFixedOrder implements Comparator<IntValueEntry> {

    @Override
    public int compare(IntValueEntry o1, IntValueEntry o2) {
        if (o1.getValue() > o2.getValue()) {
            return 1;
        }

        if (o1.getValue() == o2.getValue()) {
            return 0;
        }

        if (o1.getValue() < o2.getValue()) {
            return -1;
        }
        return 0;
    }
}
