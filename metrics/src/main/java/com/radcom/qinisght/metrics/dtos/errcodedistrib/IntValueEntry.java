package com.radcom.qinisght.metrics.dtos.errcodedistrib;

public class IntValueEntry extends AbsSeriesEntry {

    private int value;

    public IntValueEntry() {
        value = 0;
        setFragmentValue(null);
    }

    public IntValueEntry(int theVal, String theCause) {
        value = theVal;
        setFragmentValue(theCause);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
