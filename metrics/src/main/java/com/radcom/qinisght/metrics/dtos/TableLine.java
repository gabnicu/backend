package com.radcom.qinisght.metrics.dtos;

import java.util.List;

public class TableLine {
    private GroupNE groupNE;
    private List<TableSubLine> data;

    public List<TableSubLine> getData() {
        return data;
    }

    public void setData(List<TableSubLine> data) {
        this.data = data;
    }

    public GroupNE getGroupNE() {
        return groupNE;
    }

    public void setGroupNE(GroupNE groupNE) {
        this.groupNE = groupNE;
    }
}
