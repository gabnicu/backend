package com.radcom.qinisght.metrics.dtos;

public class TableLineTotal {
    private String label;
    private TableLineKPIs values;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public TableLineKPIs getValues() {
        return values;
    }

    public void setValues(TableLineKPIs values) {
        this.values = values;
    }

}
