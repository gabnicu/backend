package com.radcom.qinisght.metrics.model.diameter.errcodedistrib;

public class MetricsEntry {

    private String moment;
    private int cause;
    private String releaseType;
    private int averageCounter;
    private int maxWorst;
    private int numErrorCodeDistrib;
    private String description;

    public String getMoment() {
        return moment;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }

    public int getCause() {
        return cause;
    }

    public void setCause(int cause) {
        this.cause = cause;
    }

    public String getReleaseType() {
        return releaseType;
    }

    public void setReleaseType(String releaseType) {
        this.releaseType = releaseType;
    }

    public int getAverageCounter() {
        return averageCounter;
    }

    public void setAverageCounter(int averageCounter) {
        this.averageCounter = averageCounter;
    }

    public int getMaxWorst() {
        return maxWorst;
    }

    public void setMaxWorst(int maxWorst) {
        this.maxWorst = maxWorst;
    }

    public int getNumErrorCodeDistrib() {
        return numErrorCodeDistrib;
    }

    public void setNumErrorCodeDistrib(int numErrorCodeDistrib) {
        this.numErrorCodeDistrib = numErrorCodeDistrib;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
