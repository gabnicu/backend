package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.IntValueEntry;

import java.util.Comparator;

public class ComparatorSeriesEntryByValue implements Comparator<IntValueEntry> {

    @Override
    public int compare(IntValueEntry o1, IntValueEntry o2) {
        return (new Integer(o1.getValue())).compareTo(new Integer(o2.getValue()));
    }

}
