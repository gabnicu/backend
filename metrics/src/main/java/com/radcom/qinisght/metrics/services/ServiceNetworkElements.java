package com.radcom.qinisght.metrics.services;

import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import com.radcom.qinisght.metrics.startup.MetricsStartUpEngine;
import org.apache.log4j.Logger;

import javax.inject.Inject;

public class ServiceNetworkElements implements Runnable {

    private static final Logger theLog = Logger.getLogger(MetricsStartUpEngine.class);

    @Inject
    NetworkElementsResolver resolver;

    @Override
    public void run() {
        theLog.info("777777777777777777777777777777777777777777777777777777777777 Scheduler called");
        resolver.incrementNumber();
    }
}
