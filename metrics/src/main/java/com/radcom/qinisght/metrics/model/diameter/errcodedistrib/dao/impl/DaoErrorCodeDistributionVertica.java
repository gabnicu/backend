package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl;

import com.radcom.qinisght.metrics.business.SqlProvider;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoErrorCodeDistribution;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@ProviderVertica
public class DaoErrorCodeDistributionVertica implements DaoErrorCodeDistribution {

    private static final Logger theLog = Logger.getLogger(DaoErrorCodeDistributionVertica.class);

    @Resource(mappedName = "java:jboss/VerticaDS")
    private DataSource ds;

    @Inject
    private SqlProvider sqlProvider;

    @Override
    public List<DbEntry> getData(String startDate, String endDate, int deltaUTC, String nes, boolean isMoreThan2days) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String st = processDate(startDate);
        String end = processDate(endDate);

        String query = getSql(isMoreThan2days);
        query = query.replaceAll("START_DATE", st);
        query = query.replaceAll("END_DATE", end);
        query = query.replaceAll("SOURCE_NES", nes);
        query = query.replaceAll("DEST_NES", nes);
        query = query.replaceAll("DELTA", "" + deltaUTC);

        theLog.info("Executing SQL: " + query);

        List<DbEntry> theList = new ArrayList<>();
        try {

            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                DbEntry metricsEntry = new DbEntry();

                // String moment = rs.getString("TE0");
                Timestamp date = rs.getTimestamp("TE0");
                int cause = rs.getInt("TE1");
                String releaseType = rs.getString("TE2");
                int averageCounter = rs.getInt("TE3");
                int maxWorst = rs.getInt("TE4");
                int numErrorCodeDistrib = rs.getInt("TE5");
                String description = rs.getString("HC0E1");

                metricsEntry.setTE0(date.toString());
                metricsEntry.setTE1(cause);
                metricsEntry.setTE2(releaseType);
                metricsEntry.setTE3(averageCounter);
                metricsEntry.setTE4(maxWorst);
                metricsEntry.setTE5(numErrorCodeDistrib);
                metricsEntry.setHC0E1(description);
                theList.add(metricsEntry);

            }

            theLog.error("=============================================================== Metrics read entries #"
                    + theList.size());
        } catch (SQLException sqlEx) {
            theLog.error("SQL Exception", sqlEx);
            return null;
        } catch (Exception ex) {
            theLog.error("General Exception", ex);
            return null;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex2) {
                theLog.error("Exception closing connection", ex2);
            }
        }

        theLog.info("Read Metrics From Verica. Number of lines read is: " + theList.size());
        return theList;

    }

    // 2018-05-27 00:00:02
    private String processDate(String initialString) {

        if (initialString.length() < 19)
            return null;
        if (initialString.length() > 19)
            initialString = initialString.substring(0, 19);
        String sz = initialString.replaceAll("T", " ");
        sz = sz + ".0000000";
        return sz;
    }

    private String getSql(boolean isMoreThan2days) {

        String query;

        if (isMoreThan2days) {
            query = sqlProvider.getQuery("SQL_GET_ERROR_CODE_DISTRIBUTION_MORE_THAN_2_DAYS");
        } else {
            query = sqlProvider.getQuery("SQL_GET_ERROR_CODE_DISTRIBUTION_LESS_THAN_2_DAYS");
        }
        return query;
    }
}
