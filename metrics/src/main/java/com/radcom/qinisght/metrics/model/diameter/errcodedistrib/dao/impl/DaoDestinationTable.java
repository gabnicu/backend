package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl;

import com.radcom.qinisght.metrics.business.SqlProvider;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryTable;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoTable;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@ProviderVertica
public class DaoDestinationTable implements DaoTable {

    private static final Logger theLog = Logger.getLogger(DaoDestinationTable.class);

    @Resource(mappedName = "java:jboss/VerticaDS")
    private DataSource ds;

    @Inject
    private SqlProvider sqlProvider;

    @Override
    public List<DbEntryTable> getData(String startDate, String endDate, int deltaUTC, String nes,
                                      boolean isMoreThan2days, String fragmentValue, String dashletType) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String st = processDate(startDate);
        String end = processDate(endDate);

        String query = getSql(isMoreThan2days, dashletType);

        query = query.replaceAll("START_DATE", st);
        query = query.replaceAll("END_DATE", end);
        query = query.replaceAll("SOURCE_NES", nes);
        query = query.replaceAll("DEST_NES", nes);
        query = query.replaceAll("DELTA", "" + deltaUTC);

        if (fragmentValue != null) {
            query = query.replaceAll("RELEASE_TYPE_PLACEHOLDER", fragmentValue);
        }

        if (dashletType.equals("DIAMETER_ERROR_CODE_DISTRIBUTION")) {
            fragmentValue = "72004";
            query = query.replaceAll("RELEASE_CAUSE_PLACEHOLDER", fragmentValue);
        }

        query = query.replaceAll("RELEASE_TYPE_PLACEHOLDER", fragmentValue);

        theLog.info("Executing SQL: " + query);

        List<DbEntryTable> theList = new ArrayList<>();
        try {

            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                DbEntryTable metricsEntry = new DbEntryTable();

                String destName = rs.getString("TE0");
                String sourceName = rs.getString("TE1");
                Long requestCount = rs.getLong("TE2");
                Long responseCount = rs.getLong("TE3");
                Long successResponseCount = rs.getLong("TE4");
                Long failedResponseCount = rs.getLong("TE5");
                Long timeouts = rs.getLong("TE6");
                Long retransmissionCount = rs.getLong("TE7");
                Long totalDuration = rs.getLong("TE8");
                Long averageCounter = rs.getLong("TE9");
                Long responseTime = rs.getLong("TE10");

                metricsEntry.setTE0(destName);
                metricsEntry.setTE1(sourceName);
                metricsEntry.setTE2(requestCount);
                metricsEntry.setTE3(responseCount);
                metricsEntry.setTE4(successResponseCount);
                metricsEntry.setTE5(failedResponseCount);
                metricsEntry.setTE6(timeouts);
                metricsEntry.setTE7(retransmissionCount);
                metricsEntry.setTE8(totalDuration);
                metricsEntry.setTE9(averageCounter);
                metricsEntry.setTE10(responseTime);

                theList.add(metricsEntry);

            }

            theLog.error("======================================= Metrics read entries for table #" + theList.size());
        } catch (SQLException sqlEx) {
            theLog.error("SQL Exception", sqlEx);
            return null;
        } catch (Exception ex) {
            theLog.error("General Exception", ex);
            return null;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex2) {
                theLog.error("Exception closing connection", ex2);
            }
        }

        theLog.info("Read Metrics From Verica. Number of lines read is: " + theList.size());
        return theList;
    }

    private String processDate(String initialString) {

        if (initialString.length() < 19)
            return null;
        if (initialString.length() > 19)
            initialString = initialString.substring(0, 19);
        String sz = initialString.replaceAll("T", " ");
        sz = sz + ".0000000";
        return sz;
    }

    private String getSql(boolean isMoreThan2days, String dashletType) {
        // Retransmission Ratio, Average Response Time, Peak Response Time, Request Count
        String query = null;
        if (isMoreThan2days) {
            theLog.error("--------------------- isMoreThan2days " + isMoreThan2days + " ---------------------");
            switch (dashletType) {
                case "DIAMETER_AVERAGE_RESPONSE_TIME":
                case "DIAMETER_PEAK_RESPONSE_TIME":
                case "DIAMETER_REQUEST_COUNT":
                case "DIAMETER_RETRANSMISSION_RATIO":
                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_MORE_THAN_2_DAYS");
                    theLog.error("dashletType: " + dashletType);
                    break;
                case "DIAMETER_ERROR_CODE_DISTRIBUTION":
                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_ERROR_CODE_DISTRIBUTION_MORE_THAN_2_DAYS");
                    theLog.error("dashletType: " + dashletType);
                    break;
//                case "DIAMETER_RESPONSE_COUNT_BY_TYPE_SUCCESS":
//                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_SUCCESS_MORE_THAN_2_DAYS");
//                    theLog.error("dashletType: " + dashletType);
//                    break;
//                case "DIAMETER_RESPONSE_COUNT_BY_TYPE_TIMEOUT":
//                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_TIMEOUT_MORE_THAN_2_DAYS");
//                    theLog.error("dashletType: " + dashletType);
//                    break;
//                case "DIAMETER_RESPONSE_COUNT_BY_TYPE_ERROR":
//                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_ERROR_MORE_THAN_2_DAYS");
//                    theLog.error("dashletType: " + dashletType);
//                    break;
                case "DIAMETER_RESPONSE_COUNT_BY_TYPE":
                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_MORE_THAN_2_DAYS");
                    theLog.error("dashletType: " + dashletType);
                    break;
                default:
                    break;
            }
        } else {
            theLog.error("--------------------- isMoreThan2days " + isMoreThan2days + " ---------------------");
            switch (dashletType) {
                case "DIAMETER_AVERAGE_RESPONSE_TIME":
                case "DIAMETER_PEAK_RESPONSE_TIME":
                case "DIAMETER_REQUEST_COUNT":
                case "DIAMETER_RETRANSMISSION_RATIO":
                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_LESS_THAN_2_DAYS");
                    theLog.error("dashletType: " + dashletType);
                    break;
                case "DIAMETER_ERROR_CODE_DISTRIBUTION":
                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_ERROR_CODE_DISTRIBUTION_LESS_THAN_2_DAYS");
                    theLog.error("dashletType: " + dashletType);
                    break;
//                case "DIAMETER_RESPONSE_COUNT_BY_TYPE_SUCCESS":
//                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_SUCCESS_LESS_THAN_2_DAYS");
//                    theLog.error("dashletType: " + dashletType);
//                    break;
//                case "DIAMETER_RESPONSE_COUNT_BY_TYPE_TIMEOUT":
//                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_TIMEOUT_LESS_THAN_2_DAYS");
//                    theLog.error("dashletType: " + dashletType);
//                    break;
//                case "DIAMETER_RESPONSE_COUNT_BY_TYPE_ERROR":
//                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_ERROR_LESS_THAN_2_DAYS");
//                    theLog.error("dashletType: " + dashletType);
//                    break;
                case "DIAMETER_RESPONSE_COUNT_BY_TYPE":
                    query = sqlProvider.getQueryForDestinationTable("SQL_GET_DESTINATION_NES_TABLE_RESPONSE_COUNT_BY_TYPE_LESS_THAN_2_DAYS");
                    theLog.error("dashletType: " + dashletType);
                    break;
                default:
                    break;
            }
        }
        if (query == null) {
            theLog.error("NULL QUERY RETRIEVED FROM SQL FILE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        return query;
    }
}
