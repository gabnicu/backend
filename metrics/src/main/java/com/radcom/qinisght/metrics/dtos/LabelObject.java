package com.radcom.qinisght.metrics.dtos;

public class LabelObject {

    private String sourceName;
    private String destinationName;
    private String requestCount;
    private String responseCount;
    private String successResponseCount;
    private String failedResponseCount;
    private String timeouts;
    private String retransmissionCount;
    private String averageResponseTime;
    private String peakResponseTime;

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(String requestCount) {
        this.requestCount = requestCount;
    }

    public String getResponseCount() {
        return responseCount;
    }

    public void setResponseCount(String responseCount) {
        this.responseCount = responseCount;
    }

    public String getSuccessResponseCount() {
        return successResponseCount;
    }

    public void setSuccessResponseCount(String successResponseCount) {
        this.successResponseCount = successResponseCount;
    }

    public String getFailedResponseCount() {
        return failedResponseCount;
    }

    public void setFailedResponseCount(String failedResponseCount) {
        this.failedResponseCount = failedResponseCount;
    }

    public String getTimeouts() {
        return timeouts;
    }

    public void setTimeouts(String timeouts) {
        this.timeouts = timeouts;
    }

    public String getRetransmissionCount() {
        return retransmissionCount;
    }

    public void setRetransmissionCount(String retransmissionCount) {
        this.retransmissionCount = retransmissionCount;
    }

    public String getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(String averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public String getPeakResponseTime() {
        return peakResponseTime;
    }

    public void setPeakResponseTime(String peakResponseTime) {
        this.peakResponseTime = peakResponseTime;
    }

}
