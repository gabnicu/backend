package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao;

import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryRetransmissionRatio;

import java.util.List;

public interface DaoDbEntries {
    public List<DbEntryRetransmissionRatio> getData(String startDate, String endDate, int deltaUTC, String nes, boolean isMoreThan2days);
}
