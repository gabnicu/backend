package com.radcom.qinisght.metrics.dtos;

public class TableLineKPIs {

    private Long requestCount; //TE2
    private Long responseCount; //TE3
    private Long successResponseCount; //TE4
    private Long failedResponseCount; //TE5
    private Long timeouts; //TE6
    private Long retransmissionCount; //TE7
    private Long totalDuration; //TE8
    private Long averageResponseTime; //TE9
    private Long peakResponseTime; //TE10

    public Long getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Long requestCount) {
        this.requestCount = requestCount;
    }

    public Long getResponseCount() {
        return responseCount;
    }

    public void setResponseCount(Long responseCount) {
        this.responseCount = responseCount;
    }

    public Long getSuccessResponseCount() {
        return successResponseCount;
    }

    public void setSuccessResponseCount(Long successResponseCount) {
        this.successResponseCount = successResponseCount;
    }

    public Long getFailedResponseCount() {
        return failedResponseCount;
    }

    public void setFailedResponseCount(Long failedResponseCount) {
        this.failedResponseCount = failedResponseCount;
    }

    public Long getTimeouts() {
        return timeouts;
    }

    public void setTimeouts(Long timeouts) {
        this.timeouts = timeouts;
    }

    public Long getRetransmissionCount() {
        return retransmissionCount;
    }

    public void setRetransmissionCount(Long retransmissionCount) {
        this.retransmissionCount = retransmissionCount;
    }

    public Long getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(Long totalDuration) {
        this.totalDuration = totalDuration;
    }

    public Long getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(Long averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public Long getPeakResponseTime() {
        return peakResponseTime;
    }

    public void setPeakResponseTime(Long peakResponseTime) {
        this.peakResponseTime = peakResponseTime;
    }

}
