package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.accessors;

import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.MetricsEntry;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class EjbGetMetrics {

    private static final Logger theLog = Logger.getLogger(EjbGetMetrics.class);

    @Resource(mappedName = "java:jboss/VerticaDS")
    private DataSource ds;

    private static final String SQL_GET_LESS_THAN_2_DAYS = "SELECT T.\"TE0\" AS \"TE0\", T.\"TE1\" AS \"TE1\", T.\"TE2\" AS \"TE2\", T.\"TE5\" AS \"TE5\", T.\"TE4\" AS \"TE4\", T.\"TE3\" AS \"TE3\", HC0.\"HC0E1\" AS \"HC0E1\" FROM (SELECT T.\"TIME\" AS \"TE0\", T.\"Cause\" AS \"TE1\", T.\"Release Type\" AS \"TE2\", SUM(T.\"Num of Error Code Distribution\") AS \"TE5\", MAX(T.\"Worst\") AS \"TE4\", SUM(T.\"Average Counter\") AS \"TE3\" FROM (SELECT * FROM (select (case when '5MIN' = '5MIN' then TIMESTAMPADD('mi',-1*'-180'::Integer,START_TIME) else START_TIME end) as TIME, agg.PROCEDURE_TYPE as \"Message\", agg.PROCEDURE_SUBTYPE as \"SubType\", agg.RELEASE_CAUSE as \"Cause\", agg.RELEASE_TYPE as \"Release Type\", agg.VLAN_ID as \"VLAN ID\", agg.TRANSPORT_LAYER_PROTOCOL_NAME as \"Transport Layer Protocol\", agg.APPLICATION_ID_NAME as \"Application ID Name\", SUM(NUMBER_OF_REQUESTS) AS 'Request Count', SUM(case when agg.RELEASE_TYPE = 'Timeout' then NUMBER_OF_REQUESTS else NUMBER_OF_RESPONSES end ) AS 'Response Count by Response Type', SUM(PROCEDURE_DURATION *PROCEDURE_DURATION_COUNTER )/NULLIF(SUM(PROCEDURE_DURATION_COUNTER),0) AS 'Average Response Time Ms', SUM(PROCEDURE_DURATION *PROCEDURE_DURATION_COUNTER ) AS 'Total Duration', NULLIF(SUM(PROCEDURE_DURATION_COUNTER),0) AS 'Average Counter', MAX(PROCEDURE_DURATION_MAX) AS 'Peak Response Time Ms', MAX(1) as 'Worst', SUM(NUM_OF_RETRANSMISSION_FRAMES )/NULLIF(SUM( NUMBER_OF_REQUESTS+NUMBER_OF_RESPONSES ),0) AS 'Retransmission Ratio', SUM(NUM_OF_RETRANSMISSION_FRAMES ) AS 'Total Retransmission', NULLIF(SUM( NUMBER_OF_REQUESTS+NUMBER_OF_RESPONSES ),0) AS 'Retransmission Counter', SUM(NUM_OF_RETRANSMISSION_FRAMES) as 'Retransmission count', SUM(case when agg.RELEASE_TYPE = 'Timeout' then NUMBER_OF_REQUESTS else NUMBER_OF_RESPONSES end ) AS 'Num of Error Code Distribution', SUM(case when agg.RELEASE_TYPE='Success' then NUMBER_OF_RESPONSES end) as 'Response Count by Success', SUM(case when agg.RELEASE_TYPE='Failure' then NUMBER_OF_RESPONSES when agg.RELEASE_TYPE='Error' then NUMBER_OF_RESPONSES end) as 'Response Count by Failure', SUM(case when agg.RELEASE_TYPE='Timeout' then NUMBER_OF_REQUESTS-NUM_OF_RETRANSMISSION_FRAMES end) as 'Response Count by Truncated' from omniq.AGG_HPA_5_MIN agg WHERE start_time >= TIMESTAMPADD('mi',('-180'::Integer + MOD('-180'::Integer,60)),SUBSTRING('2018-05-15 02:00:00.0000000',0,17)::TIMESTAMP) and start_time < TIMESTAMPADD('mi',('-180'::Integer + MOD('-180'::Integer,60)),SUBSTRING('2018-05-15 03:00:00.0000000',0,17)::TIMESTAMP) AND '5MIN' = '5MIN' AND ( ((SOURCE_NE_NAME IN ('AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9') AND 'NE' = 'NE') or (DEST_NE_NAME IN ('AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9') AND 'NE' = 'NE')) OR (((1 = 1) AND 'NE' = 'LINK') AND ((1 = 1) AND 'NE' = 'LINK')) ) GROUP BY 1,2,3,4,5,6,7,8 UNION select (case when '5MIN' = '5MIN' then TIMESTAMPADD('mi',-1*'-180'::Integer,START_TIME) else START_TIME end) as TIME, agg.PROCEDURE_TYPE as \"Message\", agg.PROCEDURE_SUBTYPE as \"SubType\", agg.RELEASE_CAUSE as \"Cause\", agg.RELEASE_TYPE as \"Release Type\", agg.VLAN_ID as \"VLAN ID\", agg.TRANSPORT_LAYER_PROTOCOL_NAME as \"Transport Layer Protocol\", agg.APPLICATION_ID_NAME as \"Application ID Name\", SUM(NUMBER_OF_REQUESTS) AS 'Request Count', SUM(case when agg.RELEASE_TYPE = 'Timeout' then NUMBER_OF_REQUESTS else NUMBER_OF_RESPONSES end ) AS 'Response Count by Response Type', SUM(PROCEDURE_DURATION *PROCEDURE_DURATION_COUNTER )/NULLIF(SUM(PROCEDURE_DURATION_COUNTER),0) AS 'Average Response Time Ms', SUM(PROCEDURE_DURATION *PROCEDURE_DURATION_COUNTER ) AS 'Total Duration', NULLIF(SUM(PROCEDURE_DURATION_COUNTER),0) AS 'Average Counter', MAX(PROCEDURE_DURATION_MAX) AS 'Peak Response Time Ms', MAX(1) as 'Worst', SUM(NUM_OF_RETRANSMISSION_FRAMES )/NULLIF(SUM( NUMBER_OF_REQUESTS+NUMBER_OF_RESPONSES ),0) AS 'Retransmission Ratio', SUM(NUM_OF_RETRANSMISSION_FRAMES ) AS 'Total Retransmission', NULLIF(SUM( NUMBER_OF_REQUESTS+NUMBER_OF_RESPONSES ),0) AS 'Retransmission Counter', SUM(NUM_OF_RETRANSMISSION_FRAMES) as 'Retransmission count', SUM(case when agg.RELEASE_TYPE = 'Timeout' then NUMBER_OF_REQUESTS else NUMBER_OF_RESPONSES end ) AS 'Num of Error Code Distribution', SUM(case when agg.RELEASE_TYPE='Success' then NUMBER_OF_RESPONSES end) as 'Response Count by Success', SUM(case when agg.RELEASE_TYPE='Failure' then NUMBER_OF_RESPONSES when agg.RELEASE_TYPE='Error' then NUMBER_OF_RESPONSES end) as 'Response Count by Failure', SUM(case when agg.RELEASE_TYPE='Timeout' then NUMBER_OF_REQUESTS-NUM_OF_RETRANSMISSION_FRAMES end) as 'Response Count by Truncated' from omniq.AGG_HPA_DAY agg WHERE start_time >= TIMESTAMPADD('mi',(MOD('-180'::Integer,60)),SUBSTRING('2018-05-15 02:00:00.0000000',0,17)::TIMESTAMP) and start_time < TIMESTAMPADD('mi',(MOD('-180'::Integer,60)),SUBSTRING('2018-05-15 03:00:00.0000000',0,17)::TIMESTAMP) AND '5MIN' = 'DAY' AND ( ((SOURCE_NE_NAME IN ('AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9') AND 'NE' = 'NE') or (DEST_NE_NAME IN ('AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9') AND 'NE' = 'NE')) OR (((1 = 1) AND 'NE' = 'LINK') AND ((1 = 1) AND 'NE' = 'LINK')) ) GROUP BY 1,2,3,4,5,6,7,8 ) T1 WHERE T1.\"Release Type\" IN ('Error')) T GROUP BY T.\"TIME\", T.\"Cause\", T.\"Release Type\") T INNER JOIN (SELECT HC0.\"HC01E0\" AS \"HC0E0\", HC0.\"HC01E1\" AS \"HC0E1\" FROM (SELECT HC01.\"RELEASE_CAUSE\" AS \"HC01E0\", HC01.\"DESCRIPTION\" AS \"HC01E1\" FROM (SELECT RELEASE_CAUSE,REPLACE(DESCRIPTION ,'DIAMETER: ' ,'') AS DESCRIPTION,RELEASE_TYPE FROM omniq.ENRICHMENT_RELEASE_CAUSE WHERE DESCRIPTION LIKE '%DIAMETER%' ) HC01 GROUP BY HC01.\"RELEASE_CAUSE\", HC01.\"DESCRIPTION\") HC0 WHERE (HC0.\"HC01E0\" IS NOT NULL) AND (HC0.\"HC01E1\" IS NOT NULL) GROUP BY HC0.\"HC01E0\", HC0.\"HC01E1\") HC0 ON T.\"TE1\" = HC0.\"HC0E0\"";
//    private static final String SQL_GET_MORE_THAN_2_DAYS	=	"SELECT * FROM omniq.ne_configuration";


    public List<MetricsEntry> getAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<MetricsEntry> theList = new ArrayList<>();
        try {

            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(SQL_GET_LESS_THAN_2_DAYS);

            while (rs.next()) {
                MetricsEntry metricsEntry = new MetricsEntry();

//				String moment = rs.getString("TE0");
                Timestamp date = rs.getTimestamp("TE0");
                int cause = rs.getInt("TE1");
                String releaseType = rs.getString("TE2");
                int averageCounter = rs.getInt("TE3");
                int maxWorst = rs.getInt("TE4");
                int numErrorCodeDistrib = rs.getInt("TE5");
                String description = rs.getString("HC0E1");

//				metricsEntry.setMoment(moment);
                metricsEntry.setCause(cause);
                metricsEntry.setReleaseType(releaseType);
                metricsEntry.setAverageCounter(averageCounter);
                metricsEntry.setMaxWorst(maxWorst);
                metricsEntry.setNumErrorCodeDistrib(numErrorCodeDistrib);
                metricsEntry.setDescription(description);
                theList.add(metricsEntry);


//				theLog.error( "UTC: " + date);
            }

            theLog.error("=============================================================== Metrics read entries #" + theList.size());
        } catch (SQLException sqlEx) {
            theLog.error("SQL Exception", sqlEx);
            return null;
        } catch (Exception ex) {
            theLog.error("General Exception", ex);
            return null;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex2) {
                theLog.error("Exception closing connection", ex2);
            }
        }

        theLog.info("Read Network Elements From Verica. Number of network elements is: " + theList.size());
        return theList;

    }
}
