package com.radcom.qinisght.metrics.startup;

import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import java.util.Collection;

@Startup
@Singleton
public class EjbMetricsStartUp {

    private static final String TIMER_NAME = "TIMER_NAME_NES_FROM_METRICS";

    private static final Logger theLog = Logger.getLogger(EjbMetricsStartUp.class);
//	private ScheduledExecutorService sched;

    @Resource
    private TimerService timerService;

    @Inject
    private NetworkElementsResolver resolver;

    @PostConstruct
    void init() {
        theLog.info("EjbMetricsStartUp init()");
//		sched = Executors.newSingleThreadScheduledExecutor();
//        sched.scheduleWithFixedDelay(new ServiceNetworkElements(), 0, 10, TimeUnit.SECONDS);
        listTimersAndCancelPersisted();
//		execAndRestartTimer();
    }

    @PreDestroy
    void drawDown() {
        theLog.info("EjbMetricsStartUp drawDown()");
//		sched.shutdownNow();
    }

    private void listTimersAndCancelPersisted() {

        Collection<Timer> lstTimers = timerService.getTimers();
        if (lstTimers == null)
            return;
        for (Timer tim : lstTimers) {
            if (TIMER_NAME.equals(tim.getInfo())) {
                tim.cancel();
            }
        }
    }

    private void execAndRestartTimer() {
        resolver.incrementNumber();
        setTheTimer(5 * 1000);
    }

    private void setTheTimer(long intervalDuration) {
        Timer timer = timerService.createTimer(intervalDuration, TIMER_NAME);
    }

    @Timeout
    public void programmaticTimeout(Timer timer) {
        theLog.info("----------------------------------- Programmatic timeout occurred.");
        timer.cancel();
        execAndRestartTimer();
    }
}
