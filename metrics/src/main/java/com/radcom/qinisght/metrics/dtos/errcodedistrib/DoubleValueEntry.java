package com.radcom.qinisght.metrics.dtos.errcodedistrib;

public class DoubleValueEntry extends AbsSeriesEntry {

    private Double value;

    public DoubleValueEntry() {
        this.value = 0.0;
        setFragmentValue("");
    }

    public DoubleValueEntry(Double value, String fragmentValue) {
        this.value = value;
        setFragmentValue(fragmentValue);
    }

    public void setValue(int value) {
        this.value = (double) value;
    }

    public Double getValue() {
        if (value == null) {
            return null;
        }
        return value;
    }
}
