package com.radcom.qinisght.metrics.model.diameter.errcodedistrib;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DbEntryRetransmissionRatio {

    private String TE0;
    private Integer TE1;
    private Long TE2;
    private Integer TE3;
    private Integer TE4;
    private Integer TE5;
    private Integer TE6;
    private Integer TE7;
    private Integer TE8;
    private Integer TE18;

    @JsonProperty("TE0")
    public String getTE0() {
        return TE0;
    }

    @JsonProperty("TE0")
    public void setTE0(String tE0) {
        TE0 = tE0;
    }

    @JsonProperty("TE1")
    public Integer getTE1() {
        return TE1;
    }

    @JsonProperty("TE1")
    public void setTE1(Integer tE1) {
        TE1 = tE1;
    }

    @JsonProperty("TE2")
    public Long getTE2() {
        return TE2;
    }

    @JsonProperty("TE2")
    public void setTE2(Long tE2) {
        TE2 = tE2;
    }

    @JsonProperty("TE3")
    public Integer getTE3() {
        return TE3;
    }

    @JsonProperty("TE3")
    public void setTE3(Integer tE3) {
        TE3 = tE3;
    }

    @JsonProperty("TE4")
    public Integer getTE4() {
        return TE4;
    }

    @JsonProperty("TE4")
    public void setTE4(Integer tE4) {
        TE4 = tE4;
    }

    @JsonProperty("TE5")
    public Integer getTE5() {
        return TE5;
    }

    @JsonProperty("TE5")
    public void setTE5(Integer tE5) {
        TE5 = tE5;
    }

    @JsonProperty("TE6")
    public Integer getTE6() {
        return TE6;
    }

    @JsonProperty("TE6")
    public void setTE6(Integer tE6) {
        TE6 = tE6;
    }

    @JsonProperty("TE7")
    public Integer getTE7() {
        return TE7;
    }

    @JsonProperty("TE7")
    public void setTE7(Integer tE7) {
        TE7 = tE7;
    }

    @JsonProperty("TE8")
    public Integer getTE8() {
        return TE8;
    }

    @JsonProperty("TE8")
    public void setTE8(Integer tE8) {
        TE8 = tE8;
    }

    @JsonProperty("TE18")
    public Integer getTE18() {
        return TE18;
    }

    @JsonProperty("TE18")
    public void setTE18(Integer tE18) {
        TE18 = tE18;
    }

}
