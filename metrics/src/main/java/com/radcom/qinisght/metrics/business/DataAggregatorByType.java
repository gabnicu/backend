package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.IntValueEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinsight.utils.time.TimeUtils;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import java.util.*;

@RequestScoped
public class DataAggregatorByType {


    private static final Logger theLog = Logger.getLogger(DataAggregatorByType.class);

    public List<SampleEntry> aggregateData(List<DbEntry> lstDbEntries, AggregationType aggType, DashletType dashletType) {

        if (aggType.equals(AggregationType.AGG_TYPE_PER_5MINS))
            return aggregateDataPer5Mins(lstDbEntries, dashletType);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_HOUR))
            return aggregateDataPerHour(lstDbEntries);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_DAY))
            return aggregateDataPerDay(lstDbEntries);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_MONTH))
            return aggregateDataPerMonth(lstDbEntries);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_QUARTER))
            return aggregateDataPerQuarter(lstDbEntries);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_YEAR))
            return aggregateDataPerYear(lstDbEntries);

        return aggregateDataPerYear(lstDbEntries);
    }

    private List<SampleEntry> sortMapByDate(Map<String, SampleEntry> map) {
        List<SampleEntry> lstRet = new ArrayList<>();
        SampleEntryAggregator theSamplesAggregator = new SampleEntryAggregator();
        // SORT entries in list by DATE
        SortedSet<String> keys = new TreeSet<>(map.keySet());
        for (String key : keys) {
            SampleEntry smpl = map.get(key);
            // Do aggregation by fragmentValue
            theSamplesAggregator.aggregateDataBySum(smpl);
            lstRet.add(smpl);
        }
        return lstRet;
    }

    private List<SampleEntry> aggregateDataPer5Mins(List<DbEntry> lstDbEntries, DashletType dashletType) {
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {

            // Key is of type "2018-05-29 00:00"
            String date = dbEntry.getTE0().substring(0, 16);
            date = date.replaceAll("T", " ");
            SampleEntry val = map.get(date);
            if (val == null) {
                val = new SampleEntry();

                switch (dashletType) {
                    case DIAMETER_AVERAGE_RESPONSE_TIME:

                    case DIAMETER_ERROR_CODE_DISTRIBUTION:
                        IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                        val.addSeriesEntry(ent);
                        break;
                    default:
                        break;
                }

                // Label on chart is of type 19:15, 19:20, 19:25
                val.setLabel(date.substring(11, 16));

                // Date is of type "05/29/2018 19:15"
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
                val.setEndDateAsString(null);
                val.setStartDateAsString(null);
                val.setEndDate(null);
                val.setStartDate(null);

                map.put(date, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerHour(List<DbEntry> lstDbEntries) {
        theLog.error("aggregateDataPerHour");
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            // Key is of type 2018-05-29 00, 2018-05-29 01, 2018-05-29 11
            String date = dbEntry.getTE0().substring(0, 13);
            date = date.replaceAll("T", " ");
            SampleEntry val = map.get(date);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
                // Label on chart is of type 19:00, 20:00, 21:00
                val.setLabel(date.substring(11, 13) + ":00");

                // Date is of type 05/29/2018 19:00, 05/29/2018 20:00
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
                val.setEndDateAsString(date + ":59:59");
                val.setStartDateAsString(date + ":00:00");

                val.setEndDate(TimeUtils.getLongFromIsoString(date + ":59:59", "GMT+0:00"));
                val.setStartDate(TimeUtils.getLongFromIsoString(date + ":00:00", "GMT+0:00"));
                map.put(date, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerDay(List<DbEntry> lstDbEntries) {
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            String date = dbEntry.getTE0().substring(0, 10);
            SampleEntry val = map.get(date);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMDD(date));
                val.setDate(TimeUtils.generateAngloSaxonDate(date));
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfDay(date));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfDay(date));
                val.setEndDate(TimeUtils.getLongFromIsoString(val.getEndDateAsString(), "GMT+0:00"));
                val.setStartDate(
                        TimeUtils.getLongFromIsoString(val.getStartDateAsString(), "GMT+0:00"));

                map.put(date, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerMonth(List<DbEntry> lstDbEntries) {
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            String szYearAndMonth = dbEntry.getTE0().substring(0, 7);
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            SampleEntry val = map.get(szYearAndMonth);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                // Label must be of type "May 2018"
                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMYYYY(dbEntry.getTE0().substring(0, 11)));

                // Date must be of type "05/01/2018", 06/01/2018, 07/01/2018, etc. Always first
                // day of month
                val.setDate(TimeUtils.generateAngloSaxonDate(dbEntry.getTE0().substring(0, 7) + "-01"));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfMonth(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfMonth(szYearAndMonthAndDay));

                val.setEndDate(TimeUtils.getLongFromIsoString(val.getEndDateAsString(), "GMT+0:00"));
                val.setStartDate(
                        TimeUtils.getLongFromIsoString(val.getStartDateAsString(), "GMT+0:00"));

                map.put(szYearAndMonth, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerQuarter(List<DbEntry> lstDbEntries) {

        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
//			String szYearAndMonth = dbEntry.getTE0().substring(0, 7);
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            String quarter = TimeUtils.getQuarterFromDate(szYearAndMonthAndDay);
            SampleEntry val = map.get(quarter);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                // Label must be of type "Quarter 1, 2018"
                val.setLabel(quarter);

                // Date must be of type "01/01/2018", "04/01/2018"
                val.setDate(TimeUtils.generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(szYearAndMonthAndDay));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfQuarter(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfQuarter(szYearAndMonthAndDay));

                val.setEndDate(TimeUtils.getLongFromIsoString(val.getEndDateAsString(), "GMT+0:00"));
                val.setStartDate(
                        TimeUtils.getLongFromIsoString(val.getStartDateAsString(), "GMT+0:00"));

                theLog.info("aggregateDataPerQuarter NEW PUT quarter=" + quarter + " val=" + val);

                map.put(quarter, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerYear(List<DbEntry> lstDbEntries) {
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntry dbEntry : lstDbEntries) {
            String szYear = dbEntry.getTE0().substring(0, 4);
            SampleEntry val = map.get(szYear);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);

                val.setLabel(szYear);

                val.setDate("01/01/" + szYear);
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfYear(szYear));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfYear(szYear));

                val.setEndDate(TimeUtils.getLongFromIsoString(val.getEndDateAsString(), "GMT+0:00"));
                val.setStartDate(
                        TimeUtils.getLongFromIsoString(val.getStartDateAsString(), "GMT+0:00"));

                map.put(szYear, val);
            } else {
                Integer newVal = dbEntry.getTE5();
                if (newVal == null) {
                    theLog.error("NULL TE5 " + dbEntry.getTE0());
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(dbEntry.getTE5(), dbEntry.getHC0E1());
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }
}
