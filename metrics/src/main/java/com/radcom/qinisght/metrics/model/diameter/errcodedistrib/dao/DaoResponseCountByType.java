package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao;

import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryResponseCountByType;

import java.util.List;

public interface DaoResponseCountByType {
    public List<DbEntryResponseCountByType> getData(String startDate, String endDate, int deltaUTC, String nes, boolean isMoreThan2days);
}
