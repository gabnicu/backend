package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.*;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryTable;
import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.*;

@RequestScoped
public class TableAggregator {

    private static final Logger theLog = Logger.getLogger(TableAggregator.class);

    @Inject
    private NetworkElementsResolver resolver;

    public TableResponse aggregateData(List<DbEntryTable> lstDbEntries, String nes) {
        return getTableResponse(lstDbEntries, nes);
    }

    private TableLineKPIs getTotal(List<DbEntryTable> lstDbEntries) {
        TableLineKPIs values = new TableLineKPIs();

        Map<String, Long> map = new HashMap<>();

        Long requestCount = 0L;
        Long responseCount;
        Long totalResponseCount = 0L;
        Long successResponseCount = 0L;
        Long failedResponseCount = 0L;
        Long timeouts = 0L;
        Long retransmissionCount = 0L;
        Long totalDuration;
        Long averageResponseTime = 0L;
        Long peakResponseTime = 0L;

        for (DbEntryTable dbEntry : lstDbEntries) {
            requestCount += dbEntry.getTE2();
            responseCount = dbEntry.getTE3();
            totalResponseCount += dbEntry.getTE3();
            successResponseCount += dbEntry.getTE4();
            failedResponseCount += dbEntry.getTE5();
            timeouts += dbEntry.getTE6();
            retransmissionCount += dbEntry.getTE7();
            totalDuration = dbEntry.getTE8();
//			averageResponseTime += dbEntry.getTE9();
//            averageResponseTime = Math.max(averageResponseTime, dbEntry.getTE9());
            peakResponseTime = Math.max(peakResponseTime, dbEntry.getTE10());

//            peakResponseTime += dbEntry.getTE10();

            averageResponseTime = Math.max(averageResponseTime, (Math.round((double) totalDuration / responseCount)));

            map.put("Request Count", requestCount);
            map.put("Response Count", totalResponseCount);
            map.put("Success Response Count", successResponseCount);
            map.put("Failed Response Count", failedResponseCount);
            map.put("Timeouts", timeouts);
            map.put("Retransmission Count", retransmissionCount);
            map.put("Average Response Time", averageResponseTime);
            map.put("Peak Response Time", peakResponseTime);
        }

        values.setRequestCount(map.get("Request Count"));
        values.setResponseCount(map.get("Response Count"));
        values.setSuccessResponseCount(map.get("Success Response Count"));
        values.setFailedResponseCount(map.get("Failed Response Count"));
        values.setTimeouts(map.get("Timeouts"));
        values.setRetransmissionCount(map.get("Retransmission Count"));
        values.setAverageResponseTime(map.get("Average Response Time"));
        values.setPeakResponseTime(map.get("Peak Response Time"));

        return values;
    }

    private TableResponse getTableResponse(List<DbEntryTable> lstDbEntries, String nes) {

        TableResponse tableResp = new TableResponse();

        tableResp.setNetworkElements(processNetworkElements(nes));

        LabelObject lo = new LabelObject();
        lo.setSourceName("Source Name");
        lo.setDestinationName("Destination Name");
        lo.setRequestCount("Request Count");
        lo.setResponseCount("Response Count");
        lo.setSuccessResponseCount("Success Response Count");
        lo.setFailedResponseCount("Failed Response Count");
        lo.setTimeouts("Timeouts");
        lo.setRetransmissionCount("Retransmission Count");
        lo.setAverageResponseTime("Average Response Time");
        lo.setPeakResponseTime("Peak Response Time");

        tableResp.setLabels(lo);
        tableResp.setGroups(getLinesForTable(lstDbEntries));

        TableLineTotal tot = new TableLineTotal();
        tot.setLabel("Grand Total");
        tot.setValues(getTotal(lstDbEntries));
        tableResp.setTotal(tot);

//		List<TableLine> tableLines = new ArrayList<>();
//		List<TableSubLine> data = new ArrayList<>();
//		TableSubLine tsl = new TableSubLine();
//
//		tsl.setDestinationName("yoni1");
//		GroupNE groupNE = new GroupNE();
//		groupNE.setSourceName("yoni1");
//
//		TableLine tl = new TableLine();
//		tl.setGroupNE(groupNE);
//		data.add(tsl);
//		tl.setData(data);
//		tableLines.add(tl);

        return tableResp;
    }

    private List<TableLine> getLinesForTable(List<DbEntryTable> lstDbEntries) {
        Map<String, List<TableSubLine>> map = new HashMap<>();
        for (DbEntryTable ent : lstDbEntries) {
            List<TableSubLine> lst = map.get(ent.getTE1());
            if (lst == null) {
                lst = new ArrayList<>();
                lst.add(fromDbEntryTable(ent));
                map.put(ent.getTE1(), lst);
            } else {
                lst.add(fromDbEntryTable(ent));
            }
        }
        List<TableLine> lstTableLines = new ArrayList<>();

        Set<String> setKeys = map.keySet();
        for (String source : setKeys) {

            TableLine tl = new TableLine();
            tl.setData(map.get(source));
            GroupNE gne = new GroupNE();
            gne.setSourceName(source);
            tl.setGroupNE(gne);
            lstTableLines.add(tl);
        }
        theLog.info("lstTableLines " + lstTableLines.size());

        return lstTableLines;
    }

    private TableSubLine fromDbEntryTable(DbEntryTable dbEntry) {
        TableSubLine tsl = new TableSubLine();

        String destinationName = dbEntry.getTE0();
//		String sourceName = dbEntry.getTE1();
        Long requestCount = dbEntry.getTE2();
        Long responseCount = dbEntry.getTE3();
        Long successResponseCount = dbEntry.getTE4();
        Long failedResponseCount = dbEntry.getTE5();
        Long timeouts = dbEntry.getTE6();
        Long retransmissionCount = dbEntry.getTE7();
        Long totalDuration = dbEntry.getTE8();
//		Long averageCounter = dbEntry.getTE9();
        Long peakResponseTime = dbEntry.getTE10();

        Long averageResponseTime = Math.round((double) totalDuration / responseCount);

        tsl.setDestinationName(destinationName);
        tsl.setRequestCount(requestCount);
        tsl.setResponseCount(responseCount);
        tsl.setSuccessResponseCount(successResponseCount);
        tsl.setFailedResponseCount(failedResponseCount);
        tsl.setTimeouts(timeouts);
        tsl.setRetransmissionCount(retransmissionCount);
        tsl.setTotalDuration(totalDuration);
        tsl.setAverageResponseTime(averageResponseTime);
        tsl.setPeakResponseTime(peakResponseTime);
        return tsl;
    }

    private List<String> processNetworkElements(String initialString) {
        initialString = initialString.replaceAll("\\[", "");
        initialString = initialString.replaceAll("]", "");
        initialString = initialString.replaceAll("\\{", "");
        initialString = initialString.replaceAll("}", "");
        initialString = initialString.replaceAll("\"", "");

        String items[] = initialString.split(",");
        Set<String> hashItems = new HashSet<>();
        for (String item : items) {
            item = item.trim();
            List<String> lstResolved = resolver.resolveNetworkElement(item);
            hashItems.addAll(lstResolved);
        }
        List<String> lstRet = new ArrayList<>();
        for (String item : hashItems) {
            item = item.trim();
            lstRet.add(item);
        }
        return lstRet;
    }
}
