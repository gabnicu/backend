package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao;

import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;

import java.util.List;

public interface DaoErrorCodeDistribution {
    List<DbEntry> getData(String startDate, String endDate, int deltaUTC, String nes, boolean isMoreThan2days);

}
