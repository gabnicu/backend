package com.radcom.qinisght.metrics.rest.diameter;

import com.radcom.qinisght.metrics.business.MetricsProvider;
import com.radcom.qinisght.metrics.dtos.TableResponse;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleDoubleEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import com.radcom.qinsight.utils.time.TimeUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Transactional
@Path("metrics")
public class RestDiameter {

    private static final Logger theLog = Logger.getLogger(RestDiameter.class);

    @Inject
    private MetricsProvider metricsProvider;

    @Inject
    private NetworkElementsResolver resolver;

    @GET
    @Path("diameter/errorCodeDistribution")
    @Produces({"application/json"})
    public Response getMetricsDiameterErrorCodeDistribution(@QueryParam("startDate") final Long startDate,
                                                            @QueryParam("endDate") final Long endDate,
                                                            @QueryParam("networkElements") final String networkElements,
                                                            @QueryParam("delta") final Integer delta) {

        theLog.info("------ getMetricsDiameterErrorCodeDistribution startDate= " + startDate + " endDate= " + endDate
                + " networkElements= " + networkElements + " delta= " + delta);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));

        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsDiameterErrorCodeDistribution stdt= " + stdt + " endt= " + endt);

        //        List<SampleEntry> lst = metricsProvider.getDiameterErrorCodeDistribution(stdt, endt, delta, nes);
        List<SampleEntry> lst = metricsProvider.getDiameterErrorCodeDistribution(processStartDate(startDateExt, delta), processEndDate(endDateExt, delta), delta, nes);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/retransmissionRatio")
    @Produces({"application/json"})
    public Response getMetricsRetransmissionRatio(@QueryParam("startDate") final Long startDate,
                                                  @QueryParam("endDate") final Long endDate, @QueryParam("networkElements") final String networkElements,
                                                  @QueryParam("delta") final Integer delta) {

        theLog.info("------ getMetricsRetransmissionRatio startDate= " + startDate + " endDate= " + endDate
                + " networkElements= " + networkElements + " delta= " + delta);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsRetransmissionRatio stdt= " + stdt + " endt= " + endt);

        List<SampleDoubleEntry> lst = metricsProvider.getRetransmissionRatioExt(stdt, endt, delta, nes);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/averageResponseTime")
    @Produces({"application/json"})
    public Response getAverageResponseTime(@QueryParam("startDate") final Long startDate,
                                           @QueryParam("endDate") final Long endDate, @QueryParam("networkElements") final String networkElements,
                                           @QueryParam("delta") final Integer delta) {

        theLog.info("------ getAverageResponseTime startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getAverageResponseTime stdt= " + stdt + " endt= " + endt);

        List<SampleDoubleEntry> lst = metricsProvider.getAverageResponseTime(stdt, endt, delta, nes);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/peakResponseTime")
    @Produces({"application/json"})
    public Response getPeakResponseTime(@QueryParam("startDate") final Long startDate,
                                        @QueryParam("endDate") final Long endDate,
                                        @QueryParam("networkElements") final String networkElements,
                                        @QueryParam("delta") final Integer delta,
                                        @QueryParam("procedureType") final String procedureType,
                                        @QueryParam("procedureSubtype") final String procedureSubtype,
                                        @QueryParam("vlanId") final String vlanId,
                                        @QueryParam("applicationId") final String applicationId,
                                        @QueryParam("transportLayerProtocol") final String transportLayerProtocol) {

        theLog.info("------ getPeakResponseTime startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta);

        theLog.info("procedureType: " + procedureType);
        theLog.info("procedureSubtype: " + procedureSubtype);
        theLog.info("vlanId: " + vlanId);
        theLog.info("applicationId: " + applicationId);
        theLog.info("transportLayerProtocol: " + transportLayerProtocol);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getPeakResponseTime stdt= " + stdt + " endt= " + endt);

        List<SampleEntry> lst = metricsProvider.getPeakResponseTime(stdt, endt, delta, nes);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/requestCount")
    @Produces({"application/json"})
    public Response getRequestCount(@QueryParam("startDate") final Long startDate,
                                    @QueryParam("endDate") final Long endDate,
                                    @QueryParam("networkElements") final String networkElements,
                                    @QueryParam("delta") final Integer delta) {

        theLog.info("------ getRequestCount startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getRequestCount stdt= " + stdt + " endt= " + endt);

        List<SampleEntry> lst = metricsProvider.getRequestCount(stdt, endt, delta, nes);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/responseCountByType")
    @Produces({"application/json"})
    public Response getResponseCountByType(@QueryParam("startDate") final Long startDate,
                                           @QueryParam("endDate") final Long endDate,
                                           @QueryParam("networkElements") final String networkElements,
                                           @QueryParam("delta") final Integer delta) {

        theLog.info("------ getResponseCountByType startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getResponseCountByType stdt= " + stdt + " endt= " + endt);

        List<SampleEntry> lst = metricsProvider.getResponseCountByType(stdt, endt, delta, nes);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/table/source")
    @Produces({"application/json"})
    public Response getMetricsTableSource(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") String fragmentValue) {
        fragmentValue   =   correctionForFragmentValue( fragmentValue);

        theLog.info("------ getMetricsTableSource startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta + " dashletType " + dashletType + " fragmentValue: " + fragmentValue);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsTableSource stdt= " + stdt + " endt= " + endt);

        TableResponse tableResp = metricsProvider.getTableDataForSourceNes(stdt, endt, delta, nes, fragmentValue, dashletType);
        BasicDTO dto = new BasicDTO();
        dto.setData(tableResp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/table/destination")
    @Produces({"application/json"})
    public Response getMetricsTableDestination(@QueryParam("startDate") final Long startDate,
                                               @QueryParam("endDate") final Long endDate, @QueryParam("networkElements") final String networkElements,
                                               @QueryParam("delta") final Integer delta,
                                               @QueryParam("releaseCause") final String releaseCause,
                                               @QueryParam("releaseType") final String releaseType,
                                               @QueryParam("dashletType") final String dashletType,
                                               @QueryParam("fragmentValue") String fragmentValue) {
        fragmentValue   =   correctionForFragmentValue( fragmentValue);

        theLog.info("------ getMetricsTableDestination startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta + " dashletType " + dashletType + " fragmentValue: " + fragmentValue);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsTableDestination stdt= " + stdt + " endt= " + endt);

        TableResponse tableResp = metricsProvider.getTableDataForDestinationNes(stdt, endt, delta, nes, fragmentValue, dashletType);

        BasicDTO dto = new BasicDTO();
        dto.setData(tableResp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/table/pair")
    @Produces({"application/json"})
    public Response getMetricsTablePair(@QueryParam("startDate") final Long startDate,
                                        @QueryParam("endDate") final Long endDate, @QueryParam("networkElements") final String networkElements,
                                        @QueryParam("delta") final Integer delta,
                                        @QueryParam("releaseCause") final String releaseCause,
                                        @QueryParam("releaseType") final String releaseType,
                                        @QueryParam("dashletType") final String dashletType,
                                        @QueryParam("fragmentValue") String fragmentValue) {
        fragmentValue   =   correctionForFragmentValue( fragmentValue);

        theLog.info("------ getMetricsTablePair startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta + " dashletType " + dashletType + " fragmentValue: " + fragmentValue);


        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsTablePair stdt= " + stdt + " endt= " + endt);

        TableResponse tableResp = metricsProvider.getTableDataForPairNes(stdt, endt, delta, nes, fragmentValue, dashletType);

        BasicDTO dto = new BasicDTO();
        dto.setData(tableResp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/table/worst/source")
    @Produces({"application/json"})
    public Response getMetricsTableWorstSource(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("releaseCause") final String releaseCause,
            @QueryParam("releaseType") final String releaseType,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") String fragmentValue) {

        fragmentValue   =   correctionForFragmentValue( fragmentValue);

        theLog.info("------ getMetricsTableWorstSource startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta + " dashletType " + dashletType + " fragmentValue: " + fragmentValue);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsTableWorstSource stdt= " + stdt + " endt= " + endt);

        TableResponse tableResp = metricsProvider.getTableDataForWorstSourceNes(stdt, endt, delta, nes, fragmentValue, dashletType);
        BasicDTO dto = new BasicDTO();
        dto.setData(tableResp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/table/worst/destination")
    @Produces({"application/json"})
    public Response getMetricsTableWorstDestination(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("releaseCause") final String releaseCause,
            @QueryParam("releaseType") final String releaseType,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") String fragmentValue) {

        fragmentValue   =   correctionForFragmentValue( fragmentValue);
        theLog.info("------ getMetricsTableWorstDestination startDate= " + startDate + " endDate= " + endDate
                + " networkElements= " + networkElements + " delta= " + delta + " dashletType " + dashletType + " fragmentValue: " + fragmentValue);

        theLog.info("difference: " + (endDate - startDate));

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsTableWorstDestination stdt= " + stdt + " endt= " + endt);

        TableResponse tableResp = metricsProvider.getTableDataForWorstDestinationNes(stdt, endt, delta, nes, fragmentValue, dashletType);
        BasicDTO dto = new BasicDTO();
        dto.setData(tableResp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("diameter/table/worst/pair")
    @Produces({"application/json"})
    public Response getMetricsTableWorstPair(
            @QueryParam("startDate") final Long startDate,
            @QueryParam("endDate") final Long endDate,
            @QueryParam("networkElements") final String networkElements,
            @QueryParam("delta") final Integer delta,
            @QueryParam("releaseCause") final String releaseCause,
            @QueryParam("releaseType") final String releaseType,
            @QueryParam("dashletType") final String dashletType,
            @QueryParam("fragmentValue") String fragmentValue) {

        fragmentValue   =   correctionForFragmentValue( fragmentValue);
        theLog.info("------ getMetricsTableWorstPair startDate= " + startDate + " endDate= " + endDate + " networkElements= "
                + networkElements + " delta= " + delta + " dashletType " + dashletType + " fragmentValue: " + fragmentValue);

        long startDateExt = startDate - (delta * 60 * 1000L);
        long endDateExt = endDate - (delta * 60 * 1000L);

        String stdt = TimeUtils.getIsoStringFromLong(startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String endt = TimeUtils.getIsoStringFromLong(endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
        String nes = processNetworkElements(networkElements);

        theLog.info("------ getMetricsTableWorstPair stdt= " + stdt + " endt= " + endt);

        TableResponse tableResp = metricsProvider.getTableDataForWorstPairNes(stdt, endt, delta, nes, fragmentValue, dashletType);
        BasicDTO dto = new BasicDTO();
        dto.setData(tableResp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    private String processStartDate(long startDate, int delta) {
        return TimeUtils.getIsoStringFromLong(startDate, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
    }

    private String processEndDate(long endDate, int delta) {
        return TimeUtils.getIsoStringFromLong(endDate, TimeUtils.getGmtStringFromDeltaInMinutes(delta));
    }

    private String processNetworkElements(String initialString) {

        initialString = initialString.replaceAll("\\[", "");
        initialString = initialString.replaceAll("]", "");
        initialString = initialString.replaceAll("\\{", "");
        initialString = initialString.replaceAll("}", "");
        initialString = initialString.replaceAll("\"", "");
        theLog.info("After Processing before split initialString=" + initialString);

        String[] items = initialString.split(",");
        Set<String> hashItems = new HashSet<>();
        for (String item : items) {
            item = item.trim();
            List<String> lstResolved = resolver.resolveNetworkElement(item);
            hashItems.addAll(lstResolved);
        }

        StringBuffer sBuff = new StringBuffer();
        for (String item : hashItems) {
            item = item.trim();
            if (!(item.startsWith("'"))) {
                item = "'" + item;
            }
            if (!(item.endsWith("'"))) {
                item = item + "'";
            }
            sBuff.append(item);
            sBuff.append(", ");
        }

        String szRet = sBuff.toString();
        // REMOVE LAST COMMA AND LAST SPACE
        szRet = szRet.substring(0, szRet.length() - 2);
        theLog.info("------ processNetworkElements result=" + szRet);
        return szRet;
    }


    private String correctionForFragmentValue( String fragmentValue)
    {
        if ( fragmentValue == null)
            return "Success";
        return fragmentValue;
    }
}
