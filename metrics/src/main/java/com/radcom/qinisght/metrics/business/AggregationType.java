package com.radcom.qinisght.metrics.business;

public enum AggregationType {
    AGG_TYPE_PER_5MINS("AGG_TYPE_PER_5MINS"), AGG_TYPE_PER_HOUR("AGG_TYPE_PER_HOUR"), AGG_TYPE_PER_DAY("AGG_TYPE_PER_DAY"),
    AGG_TYPE_PER_WEEK("AGG_TYPE_PER_WEEK"), AGG_TYPE_PER_MONTH("AGG_TYPE_PER_MONTH"), AGG_TYPE_PER_QUARTER("AGG_TYPE_PER_QUARTER"), AGG_TYPE_PER_YEAR("AGG_TYPE_PER_YEAR");

    private final String text;

    AggregationType(final String txt) {
        this.text = txt;
    }

    @Override
    public String toString() {
        return text;
    }
}
