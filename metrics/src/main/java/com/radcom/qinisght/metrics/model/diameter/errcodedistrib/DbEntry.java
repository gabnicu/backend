package com.radcom.qinisght.metrics.model.diameter.errcodedistrib;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DbEntry {
    private String TE0;
    private Integer TE1;
    private String TE2;
    private Integer TE3;
    private Integer TE4;
    private Integer TE5;
    private String HC0E1;

    @JsonProperty("TE0")
    public String getTE0() {
        return TE0;
    }

    @JsonProperty("TE0")
    public void setTE0(String tE0) {
        TE0 = tE0;
    }

    @JsonProperty("TE1")
    public Integer getTE1() {
        return TE1;
    }

    @JsonProperty("TE1")
    public void setTE1(Integer tE1) {
        TE1 = tE1;
    }

    @JsonProperty("TE2")
    public String getTE2() {
        return TE2;
    }

    @JsonProperty("TE2")
    public void setTE2(String tE2) {
        TE2 = tE2;
    }


    @JsonProperty("TE3")
    public Integer getTE3() {
        return TE3;
    }

    @JsonProperty("TE3")
    public void setTE3(Integer tE3) {
        TE3 = tE3;
    }

    @JsonProperty("TE4")
    public Integer getTE4() {
        return TE4;
    }

    @JsonProperty("TE4")
    public void setTE4(Integer tE4) {
        TE4 = tE4;
    }


    @JsonProperty("TE5")
    public Integer getTE5() {
        return TE5;
    }

    @JsonProperty("TE5")
    public void setTE5(Integer tE5) {
        TE5 = tE5;
    }

    @JsonProperty("HC0E1")
    public String getHC0E1() {
        return HC0E1;
    }

    @JsonProperty("HC0E1")
    public void setHC0E1(String hC0E1) {
        HC0E1 = hC0E1;
    }

}
