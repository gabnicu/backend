package com.radcom.qinisght.metrics.dtos;

import java.util.List;

public class TableResponse {

    private List<String> networkElements;
    private LabelObject labels;
    private List<TableLine> groups;
    private TableLineTotal total;

    public List<String> getNetworkElements() {
        return networkElements;
    }

    public void setNetworkElements(List<String> networkElements) {
        this.networkElements = networkElements;
    }

    public LabelObject getLabels() {
        return labels;
    }

    public void setLabels(LabelObject labels) {
        this.labels = labels;
    }

    public List<TableLine> getGroups() {
        return groups;
    }

    public void setGroups(List<TableLine> groups) {
        this.groups = groups;
    }

    public TableLineTotal getTotal() {
        return total;
    }

    public void setTotal(TableLineTotal total) {
        this.total = total;
    }


}
