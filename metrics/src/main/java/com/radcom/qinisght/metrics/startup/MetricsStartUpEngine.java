package com.radcom.qinisght.metrics.startup;

import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class MetricsStartUpEngine implements ServletContextListener {
    private static final String TIMER_NAME = "TIMER_NAME_NES_FROM_METRICS";

    private static final Logger theLog = Logger.getLogger(MetricsStartUpEngine.class);

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        theLog.info("MetricsStartUpEngine StartUp");

//		sched = Executors.newSingleThreadScheduledExecutor();
//        sched.scheduleWithFixedDelay(new ServiceNetworkElements(), 0, 10, TimeUnit.SECONDS);
//		listTimersAndCancelPersisted();
//		execAndRestartTimer();

    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        theLog.info("MetricsStartUpEngine Destroyed");
//		sched.shutdownNow();
    }

    /**********************************************************************************************************************
     private void listTimersAndCancelPersisted()
     {
     Collection<Timer> lstTimers	=	timerService.getTimers();
     for ( Timer tim : lstTimers)	{
     if ( TIMER_NAME.equals( tim.getInfo()))	{
     tim.cancel();
     continue;
     }
     }
     }
     private void execAndRestartTimer()
     {
     resolver.incrementNumber();
     setTheTimer( 7 * 1000);
     }
     public void setTheTimer(long intervalDuration) {
     Timer timer = timerService.createTimer(intervalDuration, TIMER_NAME);
     }

     @Timeout public void programmaticTimeout(Timer timer) {

     theLog.info("----------------------------------- Programmatic timeout occurred. Timer: " + timer.toString() + " --- info " + timer.getInfo());
     timer.cancel();
     execAndRestartTimer();
     }
     **********************************************************************************************************************/

}
