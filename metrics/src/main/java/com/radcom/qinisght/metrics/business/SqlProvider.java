package com.radcom.qinisght.metrics.business;

import com.radcom.qinsight.utils.file.FileUtil;
import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import java.util.Properties;

@ApplicationScoped
public class SqlProvider {

    private static final Logger theLog = Logger.getLogger(SqlProvider.class);

    private Properties sqlProperties;

    public String getQuery(String propertyName) {
        sqlProperties = FileUtil.readPropertiesFile("diameter.dashlets.properties");
        theLog.info("Reading SQL query");
        if (sqlProperties == null) {
            theLog.info("sqlProperties is null");
            return null;
        }
        return sqlProperties.getProperty(propertyName);
    }

    public String getQueryForSourceTable(String propertyName) {
        sqlProperties = FileUtil.readPropertiesFile("diameter.table.source.properties");
        theLog.info("Reading SQL query");
        if (sqlProperties == null) {
            theLog.info("sqlProperties is null");
            return null;
        }
        return sqlProperties.getProperty(propertyName);
    }

    public String getQueryForDestinationTable(String propertyName) {
        sqlProperties = FileUtil.readPropertiesFile("diameter.table.destination.properties");
        theLog.info("Reading SQL query");
        if (sqlProperties == null) {
            theLog.info("sqlProperties is null");
            return null;
        }
        return sqlProperties.getProperty(propertyName);
    }

    public String getQueryForPairTable(String propertyName) {
        sqlProperties = FileUtil.readPropertiesFile("diameter.table.pair.properties");
        theLog.info("Reading SQL query");
        if (sqlProperties == null) {
            theLog.info("sqlProperties is null");
            return null;
        }
        return sqlProperties.getProperty(propertyName);
    }

    public String getQueryForWorstSourceTable(String propertyName) {
        sqlProperties = FileUtil.readPropertiesFile("diameter.table.worst.source.properties");
        theLog.info("Reading SQL query");
        if (sqlProperties == null) {
            theLog.info("sqlProperties is null");
            return null;
        }
        return sqlProperties.getProperty(propertyName);
    }

    public String getQueryForWorstDestinationTable(String propertyName) {
        sqlProperties = FileUtil.readPropertiesFile("diameter.table.worst.destination.properties");
        theLog.info("Reading SQL query");
        if (sqlProperties == null) {
            theLog.info("sqlProperties is null");
            return null;
        }
        return sqlProperties.getProperty(propertyName);
    }

    public String getQueryForWorstPairTable(String propertyName) {
        sqlProperties = FileUtil.readPropertiesFile("diameter.table.worst.pair.properties");
        theLog.info("Reading SQL query");
        if (sqlProperties == null) {
            theLog.info("sqlProperties is null");
            return null;
        }
        return sqlProperties.getProperty(propertyName);
    }
}
