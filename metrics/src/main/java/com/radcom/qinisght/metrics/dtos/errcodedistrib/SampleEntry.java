package com.radcom.qinisght.metrics.dtos.errcodedistrib;

import java.util.ArrayList;
import java.util.List;

public class SampleEntry {

    private String label;

    private List<IntValueEntry> values;

    private String startDateAsString;
    private String endDateAsString;

    private String date;

    private Long startDate;
    private Long endDate;

    private Boolean hasDrilldown = true;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStartDateAsString() {
        return startDateAsString;
    }

    public void setStartDateAsString(String startDateAsString) {
        this.startDateAsString = startDateAsString;
    }

    public String getEndDateAsString() {
        return endDateAsString;
    }

    public void setEndDateAsString(String endDateAsString) {
        this.endDateAsString = endDateAsString;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public List<IntValueEntry> getValues() {
        return values;
    }

    public void setValues(List<IntValueEntry> values) {
        this.values = values;
    }

    public void addSeriesEntry(IntValueEntry ent) {
        if (values == null) {
            values = new ArrayList<>();
        }
        values.add(ent);
    }

    public Boolean getHasDrilldown() {
        return hasDrilldown;
    }

    public void setHasDrilldown(Boolean hasDrilldown) {
        this.hasDrilldown = hasDrilldown;
    }
}
