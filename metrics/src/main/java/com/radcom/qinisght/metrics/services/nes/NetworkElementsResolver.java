package com.radcom.qinisght.metrics.services.nes;


import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import com.radcom.qinsight.utils.rest.dtos.nes.DTONetworkElements;
import com.radcom.qinsight.utils.rest.dtos.nes.NetworkElementEntry;
import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@ApplicationScoped
public class NetworkElementsResolver {

    private static final Logger theLog = Logger.getLogger(NetworkElementsResolver.class);

    @Inject
    @EnvVarServices
    private ServiceDiscovery serviceDiscovery;

    private int number = 0;
    private List<NetworkElementEntry> lstData = null;

    public int getNumber() {
        return number;
    }

    public void incrementNumber() {
        this.number++;
        theLog.info("---------------------- number incremented. Value is: " + getNumber());
    }


    public void initiateNetworkElementsLoad() {
        try {
            DTONetworkElements dto = serviceDiscovery.getNetworkElementsDbEntriesService().
                    register(DTONetworkElements.class).
                    request().get(DTONetworkElements.class);

            lstData = dto.getData();
            theLog.info("Called microservices NEs. Loaded " + lstData.size() + " elements");

//	    	String resp	=	serviceDiscovery.getNetworkElementsDbEntriesService().
//    				register(String.class).
//    				request().get(String.class);
//	    	theLog.info("Called microservices NEs. String is: " + resp);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<String> resolveNetworkElement(String neName) {

        List<String> lstRet = new ArrayList<>();
        if (lstData == null)
            return null;
        for (NetworkElementEntry nee : lstData) {
            if (neName.equals(nee.getNeName())) {
                lstRet.add("'" + neName + "'");
                return lstRet;
            }
            if (neName.equals(nee.getLocation())) {
                lstRet.add("'" + nee.getNeName() + "'");
            }
            if (neName.equals(nee.getNeGroup())) {
                lstRet.add("'" + nee.getNeName() + "'");
            }
        }
        return lstRet;
    }
}
