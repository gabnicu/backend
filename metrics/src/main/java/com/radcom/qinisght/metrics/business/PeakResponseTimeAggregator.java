package com.radcom.qinisght.metrics.business;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.IntValueEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntryRetransmissionRatio;
import com.radcom.qinsight.utils.time.TimeUtils;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import java.util.*;

@RequestScoped
public class PeakResponseTimeAggregator {

    private static final Logger theLog = Logger.getLogger(PeakResponseTimeAggregator.class);

    public List<SampleEntry> aggregateData(List<DbEntryRetransmissionRatio> lstDbEntries, AggregationType aggType, int delta) {

        if (aggType.equals(AggregationType.AGG_TYPE_PER_5MINS))
            return aggregateDataPer5Mins(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_HOUR))
            return aggregateDataPerHour(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_DAY))
            return aggregateDataPerDay(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_MONTH))
            return aggregateDataPerMonth(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_QUARTER))
            return aggregateDataPerQuarter(lstDbEntries, delta);
        if (aggType.equals(AggregationType.AGG_TYPE_PER_YEAR))
            return aggregateDataPerYear(lstDbEntries, delta);

        return aggregateDataPerYear(lstDbEntries, delta);
    }

    private List<SampleEntry> sortMapByDate(Map<String, SampleEntry> map) {
        theLog.error("sortMapByDate");
        List<SampleEntry> lstRet = new ArrayList<>();
        SampleEntryAggregator theSamplesAggregator = new SampleEntryAggregator();
        // SORT entries in list by DATE
        SortedSet<String> keys = new TreeSet<>(map.keySet());
        for (String key : keys) {
            SampleEntry smpl = map.get(key);
            // Do aggregation by fragmentValue
            theSamplesAggregator.aggregateDataByMax(smpl);
            lstRet.add(smpl);
        }
        return lstRet;
    }

    private List<SampleEntry> aggregateDataPer5Mins(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPer5Mins");
        String fragmentValue = "";
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio DbEntryRetransmissionRatio : lstDbEntries) {

            // Key is of type "2018-05-29 00:00"
            String date = DbEntryRetransmissionRatio.getTE0().substring(0, 16);
            date = date.replaceAll("T", " ");
            SampleEntry val = map.get(date);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
                // Label on chart is of type 19:15, 19:20, 19:25
                val.setLabel(date.substring(11, 16));

                // Date is of type "05/29/2018 19:15"
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());

                val.setStartDateAsString(date + ":00");
                String szStart = val.getStartDateAsString();
                long lgStart = TimeUtils.fromISO8601ToUTCAsMillisec(szStart);
                lgStart += delta * 60 * 1000L;
                long lgEnd = lgStart + 5 * 60 * 1000L;
                val.setEndDate(lgEnd);
                val.setStartDate(lgStart);

                val.setHasDrilldown(false);

                map.put(date, val);
            } else {
                Integer newVal = DbEntryRetransmissionRatio.getTE4();
                if (newVal == null) {
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerHour(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerHour");
        String fragmentValue = "";
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio DbEntryRetransmissionRatio : lstDbEntries) {
            // Key is of type 2018-05-29 00, 2018-05-29 01, 2018-05-29 11
            String date = DbEntryRetransmissionRatio.getTE0().substring(0, 13);
            date = date.replaceAll("T", " ");
            SampleEntry val = map.get(date);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
                // Label on chart is of type 19:00, 20:00, 21:00


                val.setLabel(date.substring(11, 13) + ":00");

                // Date is of type 05/29/2018 19:00, 05/29/2018 20:00
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
                val.setEndDateAsString(date + ":59:59");
                val.setStartDateAsString(date + ":00:00");

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(date + ":00:00", TimeUtils.getGmtStringFromDeltaInMinutes(delta)) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(date + ":59:59", TimeUtils.getGmtStringFromDeltaInMinutes(delta)) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(date, val);
            } else {
                Integer newVal = DbEntryRetransmissionRatio.getTE4();
                if (newVal == null) {
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerDay(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerDay");
        String fragmentValue = "";
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio DbEntryRetransmissionRatio : lstDbEntries) {
            String date = DbEntryRetransmissionRatio.getTE0().substring(0, 10);
            SampleEntry val = map.get(date);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);

                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMDD(date));
                val.setDate(TimeUtils.generateAngloSaxonDate(date));
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfDay(date));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfDay(date));

                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), TimeUtils.getGmtStringFromDeltaInMinutes(delta)) + 1000L;
                long endDate = startDate + 24 * 3600 * 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(date, val);
            } else {
                Integer newVal = DbEntryRetransmissionRatio.getTE4();
                if (newVal == null) {
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerMonth(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerMonth");
        String fragmentValue = "";
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio DbEntryRetransmissionRatio : lstDbEntries) {
            String szYearAndMonth = DbEntryRetransmissionRatio.getTE0().substring(0, 7);
            String szYearAndMonthAndDay = DbEntryRetransmissionRatio.getTE0().substring(0, 10);
            SampleEntry val = map.get(szYearAndMonth);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);

                // Label must be of type "May 2018"
                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMYYYY(DbEntryRetransmissionRatio.getTE0().substring(0, 11)));

                theLog.info("label: " + val.getLabel());

                // Date must be of type "05/01/2018", 06/01/2018, 07/01/2018, etc. Always first day of month
                val.setDate(TimeUtils.generateAngloSaxonDate(szYearAndMonth + "-01"));

                theLog.info("szYearAndMonth: " + szYearAndMonth + " date: " + val.getDate());

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfMonth(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfMonth(szYearAndMonthAndDay));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(szYearAndMonth, val);
            } else {
                Integer newVal = DbEntryRetransmissionRatio.getTE4();
                if (newVal == null) {
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerQuarter(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerQuarter");
        String fragmentValue = "";
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio DbEntryRetransmissionRatio : lstDbEntries) {
            String szYearAndMonthAndDay = DbEntryRetransmissionRatio.getTE0().substring(0, 10);
            String quarter = TimeUtils.getQuarterFromDate(szYearAndMonthAndDay);
            SampleEntry val = map.get(quarter);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);

                // Label must be of type "Quarter 1, 2018"
                val.setLabel(quarter);

                // Date must be of type "01/01/2018", "04/01/2018"
                val.setDate(TimeUtils.generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(szYearAndMonthAndDay));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfQuarter(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfQuarter(szYearAndMonthAndDay));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(quarter, val);
            } else {
                Integer newVal = DbEntryRetransmissionRatio.getTE4();
                if (newVal == null) {
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }

    private List<SampleEntry> aggregateDataPerYear(List<DbEntryRetransmissionRatio> lstDbEntries, int delta) {
        theLog.error("aggregateDataPerYear");
        String fragmentValue = "";
        Map<String, SampleEntry> map = new HashMap<>();
        for (DbEntryRetransmissionRatio DbEntryRetransmissionRatio : lstDbEntries) {
            String szYear = DbEntryRetransmissionRatio.getTE0().substring(0, 4);
            theLog.info("szYear:" + szYear);
            SampleEntry val = map.get(szYear);
            if (val == null) {
                val = new SampleEntry();
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);

                val.setLabel(szYear);

                val.setDate("01/01/" + szYear);
                val.setEndDateAsString(TimeUtils.generateIsoDateEndOfYear(szYear));
                val.setStartDateAsString(TimeUtils.generateIsoDateStartOfYear(szYear));

                long longDelta = delta * 60 * 1000L;
                long startDate = TimeUtils.getLongFromIsoString(val.getStartDateAsString(), null) + longDelta;
                long endDate = TimeUtils.getLongFromIsoString(val.getEndDateAsString(), null) + longDelta + 1000L;

                val.setEndDate(endDate);
                val.setStartDate(startDate);

                map.put(szYear, val);
            } else {
                Integer newVal = DbEntryRetransmissionRatio.getTE4();
                if (newVal == null) {
                    continue;
                }
                IntValueEntry ent = new IntValueEntry(DbEntryRetransmissionRatio.getTE4(), fragmentValue);
                val.addSeriesEntry(ent);
            }
        }
        return sortMapByDate(map);
    }
}
