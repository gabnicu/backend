package com.radcom.qinsight.network.elements.business;

import com.radcom.qinisght.metrics.business.AggregationType;
import com.radcom.qinisght.metrics.business.DashletType;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl.DaoDataFromDb;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl.ProviderVertica;
import com.radcom.qinsight.nes.business.NetworkElementsDataAgreggatorExt;
import com.radcom.qinsight.nes.dtos.SampleNetworkElementsEntry;
import com.radcom.qinsight.nes.model.DbEntryNetworkElements;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RequestScoped
public class NetworkElementsProvider {

    private static final Logger theLog = Logger.getLogger(NetworkElementsProvider.class);

    @Inject
    @ProviderVertica
    private DaoDataFromDb getDbEntries;

    @Inject
    private NetworkElementsDataAgreggatorExt aggregNetworkElements;

    public List<SampleNetworkElementsEntry> getSourceNetworkElementsSummaryTable(String startDate, String endDate, int deltaUTC,
                                                                                 String nes, DashletType dashletType) {
        AggregationType aggType = getAggregationType(startDate, endDate, deltaUTC, nes);
        List<DbEntryNetworkElements> lstDbEntries = getDbEntries.getDataForNes(startDate, endDate, deltaUTC, nes, isMoreThanTwoDays(startDate, endDate), dashletType);
        return aggregNetworkElements.aggregateData(lstDbEntries, aggType);
    }

    // 2018-05-27 00:00:02
    private String processDate(String initialString) {
        if (initialString.length() < 19)
            return null;
        if (initialString.length() > 19)
            initialString = initialString.substring(0, 19);
        String sz = initialString.replaceAll("T", " ");
        return sz;
    }

    private boolean isMoreThanTwoDays(String startDate, String endDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date dtStart = sdf.parse(startDate);
            Date dtEnd = sdf.parse(endDate);
            long delta = dtEnd.getTime() - dtStart.getTime();

            if (delta < (3600L + 10) * 1000L) {
                return false;
            } else if (delta < 2 * 24 * 3600L * 1000L) {
                return false;
            } else {
                return true;
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    private AggregationType getAggregationType(String startDate, String endDate, int deltaUTC, String nes) {
        startDate = processDate(startDate);
        endDate = processDate(endDate);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // boolean isMoreThan2days = false;
        AggregationType aggType;
        try {
            Date dtStart = sdf.parse(startDate);
            Date dtEnd = sdf.parse(endDate);

            long delta = dtEnd.getTime() - dtStart.getTime();
            theLog.info("delta time: " + delta);
            if (delta < (3600L + 10) * 1000L) {
                // isMoreThan2days = false;
                aggType = AggregationType.AGG_TYPE_PER_5MINS;
            } else if (delta < 2 * 24 * 3600L * 1000L) {
                // isMoreThan2days = false;
                aggType = AggregationType.AGG_TYPE_PER_HOUR;
            } else {
                // isMoreThan2days = true;
                if (delta < 60 * 24 * 3600L * 1000L)
                    aggType = AggregationType.AGG_TYPE_PER_DAY;
                else if (delta < 6 * 31 * 24 * 3600L * 1000L)
                    aggType = AggregationType.AGG_TYPE_PER_MONTH;
                else if (delta < 367 * 24 * 3600L * 1000L)
                    aggType = AggregationType.AGG_TYPE_PER_QUARTER;
                else
                    aggType = AggregationType.AGG_TYPE_PER_YEAR;
            }
            // theLog.info("isMoreThan2days: " + isMoreThan2days + " aggType: " + aggType);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return aggType;
    }

}
