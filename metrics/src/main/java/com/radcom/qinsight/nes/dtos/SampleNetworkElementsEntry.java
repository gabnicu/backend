package com.radcom.qinsight.nes.dtos;

import java.util.ArrayList;
import java.util.List;

public class SampleNetworkElementsEntry {

//	private static final Logger theLog = Logger.getLogger(SampleDoubleEntry.class);

    private String label;

    List<NetworkElementsValueEntry> values;

    private String drilldownStartDateAsString;
    private String drilldownEndDateAsString;

    private String date;

    private Long drilldownStartDate;
    private Long drilldownEndDate;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDrilldownStartDateAsString() {
        return drilldownStartDateAsString;
    }

    public void setDrilldownStartDateAsString(String drilldownStartDate) {
        this.drilldownStartDateAsString = drilldownStartDate;
    }

    public String getDrilldownEndDateAsString() {
        return drilldownEndDateAsString;
    }

    public void setDrilldownEndDateAsString(String drilldownEndDate) {
        this.drilldownEndDateAsString = drilldownEndDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getDrilldownStartDate() {
        return drilldownStartDate;
    }

    public void setDrilldownStartDate(Long drilldownStartDate) {
        this.drilldownStartDate = drilldownStartDate;
    }

    public Long getDrilldownEndDate() {
        return drilldownEndDate;
    }

    public void setDrilldownEndDate(Long drilldownEndDate) {
        this.drilldownEndDate = drilldownEndDate;
    }

    public List<NetworkElementsValueEntry> getValues() {
        return values;
    }

    public void setValues(List<NetworkElementsValueEntry> value) {
        this.values = value;
    }

    public void addSeriesNetworkElementEntry(NetworkElementsValueEntry ent) {
        if (values == null) {
            values = new ArrayList<NetworkElementsValueEntry>();
        }
        values.add(ent);
    }
}
