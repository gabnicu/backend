package com.radcom.qinsight.nes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DbEntryNetworkElements {

    private String TE0;
    private String TE1;
    private Integer TE2;
    private Integer TE3;
    private Integer TE4;
    private Integer TE5;
    private Integer TE6;
    private Integer TE7;
    private Integer TE8;
    private Integer TE9;
    private Integer TE10;

    @JsonProperty("TE0")
    public String getTE0() {
        return TE0;
    }

    @JsonProperty("TE0")
    public void setTE0(String tE0) {
        TE0 = tE0;
    }

    @JsonProperty("TE1")
    public String getTE1() {
        return TE1;
    }

    @JsonProperty("TE1")
    public void setTE1(String tE1) {
        TE1 = tE1;
    }

    @JsonProperty("TE2")
    public Integer getTE2() {
        return TE2;
    }

    @JsonProperty("TE2")
    public void setTE2(Integer tE2) {
        TE2 = tE2;
    }

    @JsonProperty("TE3")
    public Integer getTE3() {
        return TE3;
    }

    @JsonProperty("TE3")
    public void setTE3(Integer tE3) {
        TE3 = tE3;
    }

    @JsonProperty("TE4")
    public Integer getTE4() {
        return TE4;
    }

    @JsonProperty("TE4")
    public void setTE4(Integer tE4) {
        TE4 = tE4;
    }

    @JsonProperty("TE5")
    public Integer getTE5() {
        return TE5;
    }

    @JsonProperty("TE5")
    public void setTE5(Integer tE5) {
        TE5 = tE5;
    }

    @JsonProperty("TE6")
    public Integer getTE6() {
        return TE6;
    }

    @JsonProperty("TE6")
    public void setTE6(Integer tE6) {
        TE6 = tE6;
    }

    @JsonProperty("TE7")
    public Integer getTE7() {
        return TE7;
    }

    @JsonProperty("TE7")
    public void setTE7(Integer tE7) {
        TE7 = tE7;
    }

    @JsonProperty("TE8")
    public Integer getTE8() {
        return TE8;
    }

    @JsonProperty("TE8")
    public void setTE8(Integer tE8) {
        TE8 = tE8;
    }

    @JsonProperty("TE9")
    public Integer getTE9() {
        return TE9;
    }

    @JsonProperty("TE9")
    public void setTE9(Integer tE9) {
        TE9 = tE9;
    }

    @JsonProperty("TE10")
    public Integer getTE10() {
        return TE10;
    }

    @JsonProperty("TE10")
    public void setTE10(Integer tE10) {
        TE10 = tE10;
    }

}
