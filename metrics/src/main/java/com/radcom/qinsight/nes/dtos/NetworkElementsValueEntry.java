package com.radcom.qinsight.nes.dtos;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.AbsSeriesEntry;

public class NetworkElementsValueEntry extends AbsSeriesEntry {

    private String destName;
    private String sourceName;
    private int requestCount;
    private int responseCount;
    private int successResponseCount;
    private int failedResponseCount;
    private int timeouts;
    private int retransmissionCount;
    private int totalDuration;
    private int averageCounter;
    private int peakResponseTime;

    public NetworkElementsValueEntry(String destName, String sourceName, int requestCount, int responseCount,
                                     int successResponseCount, int failedResponseCount, int timeouts, int retransmissionCount, int totalDuration,
                                     int averageCounter, int peakResponseTime) {
        setFragmentValue("");
        this.destName = destName;
        this.sourceName = sourceName;
        this.requestCount = requestCount;
        this.responseCount = responseCount;
        this.successResponseCount = successResponseCount;
        this.failedResponseCount = failedResponseCount;
        this.timeouts = timeouts;
        this.retransmissionCount = retransmissionCount;
        this.totalDuration = totalDuration;
        this.averageCounter = averageCounter;
        this.peakResponseTime = peakResponseTime;
    }

    public String getDestName() {
        return destName;
    }

    public void setDestName(String destName) {
        this.destName = destName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public int getResponseCount() {
        return responseCount;
    }

    public void setResponseCount(int responseCount) {
        this.responseCount = responseCount;
    }

    public int getSuccessResponseCount() {
        return successResponseCount;
    }

    public void setSuccessResponseCount(int successResponseCount) {
        this.successResponseCount = successResponseCount;
    }

    public int getFailedResponseCount() {
        return failedResponseCount;
    }

    public void setFailedResponseCount(int failedResponseCount) {
        this.failedResponseCount = failedResponseCount;
    }

    public int getTimeouts() {
        return timeouts;
    }

    public void setTimeouts(int timeouts) {
        this.timeouts = timeouts;
    }

    public int getRetransmissionCount() {
        return retransmissionCount;
    }

    public void setRetransmissionCount(int retransmissionCount) {
        this.retransmissionCount = retransmissionCount;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(int totalDuration) {
        this.totalDuration = totalDuration;
    }

    public int getAverageCounter() {
        return averageCounter;
    }

    public void setAverageCounter(int averageCounter) {
        this.averageCounter = averageCounter;
    }

    public int getPeakResponseTime() {
        return peakResponseTime;
    }

    public void setPeakResponseTime(int peakResponseTime) {
        this.peakResponseTime = peakResponseTime;
    }

}
