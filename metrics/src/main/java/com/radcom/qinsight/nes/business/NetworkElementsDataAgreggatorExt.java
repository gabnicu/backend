package com.radcom.qinsight.nes.business;

import com.radcom.qinisght.metrics.business.AggregationType;
import com.radcom.qinsight.nes.dtos.NetworkElementsValueEntry;
import com.radcom.qinsight.nes.dtos.SampleNetworkElementsEntry;
import com.radcom.qinsight.nes.model.DbEntryNetworkElements;
import com.radcom.qinsight.utils.time.TimeUtils;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import java.util.*;

@RequestScoped
public class NetworkElementsDataAgreggatorExt {

    private static final Logger theLog = Logger.getLogger(NetworkElementsDataAgreggatorExt.class);

    public List<SampleNetworkElementsEntry> aggregateData(List<DbEntryNetworkElements> lstDbEntries, AggregationType aggType) {

//		if (aggType.equals(AggregationType.AGG_TYPE_PER_5MINS)) {
//			return aggregateDataPer5Mins(lstDbEntries);
//		}
//		if (aggType.equals(AggregationType.AGG_TYPE_PER_HOUR)) {
//			return aggregateDataPerHour(lstDbEntries);
//		}
//		if (aggType.equals(AggregationType.AGG_TYPE_PER_DAY)) {
//			return aggregateDataPerDay(lstDbEntries);
//		}
//		if (aggType.equals(AggregationType.AGG_TYPE_PER_MONTH)) {
//			return aggregateDataPerMonth(lstDbEntries);
//		}
//		if (aggType.equals(AggregationType.AGG_TYPE_PER_QUARTER)) {
//			return aggregateDataPerQuarter(lstDbEntries);
//		}
//		if (aggType.equals(AggregationType.AGG_TYPE_PER_YEAR)) {
//			return aggregateDataPerYear(lstDbEntries);
//		}

        return aggregateDataPerYear(lstDbEntries);
    }

    private List<SampleNetworkElementsEntry> sortMapByDate(Map<String, SampleNetworkElementsEntry> map) {
        theLog.error("sortMapByDate");
        List<SampleNetworkElementsEntry> lstRet = new ArrayList<>();
//		SampleEntryAggregator theSamplesAggregator = new SampleEntryAggregator();
        // SORT entries in list by DATE
        SortedSet<String> keys = new TreeSet<>(map.keySet());
        for (String key : keys) {
            SampleNetworkElementsEntry smpl = map.get(key);
            lstRet.add(smpl);
        }
        theLog.error("lstRet size: " + lstRet.size());
        return lstRet;
    }

    public List<SampleNetworkElementsEntry> aggregateDataPer5Mins(List<DbEntryNetworkElements> lstDbEntries) {
        theLog.error("aggregateDataPer5Mins");
        Map<String, SampleNetworkElementsEntry> map = new HashMap<>();
        for (DbEntryNetworkElements dbEntry : lstDbEntries) {

            // Key is of type "2018-05-29 00:00"
            String date = dbEntry.getTE0().substring(0, 16);
            date = date.replaceAll("T", " ");
            SampleNetworkElementsEntry val = map.get(date);
            if (val == null) {
                val = new SampleNetworkElementsEntry();
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);
                // Label on chart is of type 19:15, 19:20, 19:25
                val.setLabel(date.substring(11, 16));

                // Date is of type "05/29/2018 19:15"
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
                val.setDrilldownEndDateAsString(null);
                val.setDrilldownStartDateAsString(null);
                val.setDrilldownEndDate(null);
                val.setDrilldownStartDate(null);

                map.put(date, val);
            } else {
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();
                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);
            }

        }
        return sortMapByDate(map);
    }

    public List<SampleNetworkElementsEntry> aggregateDataPerHour(List<DbEntryNetworkElements> lstDbEntries) {
        theLog.error("aggregateDataPerHour");
        Map<String, SampleNetworkElementsEntry> map = new HashMap<>();
        for (DbEntryNetworkElements dbEntry : lstDbEntries) {
            // Key is of type 2018-05-29 00, 2018-05-29 01, 2018-05-29 11
            String date = dbEntry.getTE0().substring(0, 13);
//			String isoDateYYYYMMDD = dbEntry.getTE0().substring(0, 10);
            date = date.replaceAll("T", " ");
            SampleNetworkElementsEntry val = map.get(date);
            if (val == null) {
                val = new SampleNetworkElementsEntry();

                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);

                val.addSeriesNetworkElementEntry(ent);
                // Label on chart is of type 19:00, 20:00, 21:00
                val.setLabel(date.substring(11, 13) + ":00");

                // Date is of type 05/29/2018 19:00, 05/29/2018 20:00
                val.setDate(TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
                val.setDrilldownEndDateAsString(date + ":59:59");
                val.setDrilldownStartDateAsString(date + ":00:00");

                val.setDrilldownEndDate(TimeUtils.getLongFromIsoString(date + ":59:59", "GMT+0:00"));
                val.setDrilldownStartDate(TimeUtils.getLongFromIsoString(date + ":00:00", "GMT+0:00"));
                map.put(date, val);
            } else {
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
//				if (te1Value == null || te8Value == null) {
//					theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
//					continue;
//				}

                val.addSeriesNetworkElementEntry(ent);
            }

        }
        return sortMapByDate(map);
    }

    public List<SampleNetworkElementsEntry> aggregateDataPerDay(List<DbEntryNetworkElements> lstDbEntries) {
        theLog.error("aggregateDataPerDay");
        Map<String, SampleNetworkElementsEntry> map = new HashMap<>();
        for (DbEntryNetworkElements dbEntry : lstDbEntries) {
            String date = dbEntry.getTE0().substring(0, 10);
            SampleNetworkElementsEntry val = map.get(date);
            if (val == null) {
                val = new SampleNetworkElementsEntry();
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);
//				theLog.error("date: " + date + " retransmissionRatio " + retransmissionRatio);

                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMDD(date));
                val.setDate(TimeUtils.generateAngloSaxonDate(date));
                val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfDay(date));
                val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfDay(date));
                val.setDrilldownEndDate(TimeUtils.getLongFromIsoString(val.getDrilldownEndDateAsString(), "GMT+0:00"));
                val.setDrilldownStartDate(
                        TimeUtils.getLongFromIsoString(val.getDrilldownStartDateAsString(), "GMT+0:00"));

                map.put(date, val);
            } else {
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
//				if (te1Value == null || te8Value == null) {
//					theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
//					continue;
//				}
                val.addSeriesNetworkElementEntry(ent);
//				theLog.error("date: " + date + " retransmissionRatio " + retransmissionRatio);
            }

        }
        return sortMapByDate(map);
    }

    public List<SampleNetworkElementsEntry> aggregateDataPerMonth(List<DbEntryNetworkElements> lstDbEntries) {
        theLog.error("aggregateDataPerMonth");
        Map<String, SampleNetworkElementsEntry> map = new HashMap<>();
        for (DbEntryNetworkElements dbEntry : lstDbEntries) {
            String szYearAndMonth = dbEntry.getTE0().substring(0, 7);
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            SampleNetworkElementsEntry val = map.get(szYearAndMonth);
            if (val == null) {
                val = new SampleNetworkElementsEntry();
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);

                // Label must be of type "May 2018"
                val.setLabel(TimeUtils.convertDateFromYYYYMMDDtoMMMYYYY(dbEntry.getTE0().substring(0, 11)));

                // Date must be of type "05/01/2018", 06/01/2018, 07/01/2018, etc. Always first
                // day of month
                val.setDate(TimeUtils.generateAngloSaxonDate(dbEntry.getTE0().substring(0, 7) + "-01"));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfMonth(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfMonth(szYearAndMonthAndDay));

                val.setDrilldownEndDate(TimeUtils.getLongFromIsoString(val.getDrilldownEndDateAsString(), "GMT+0:00"));
                val.setDrilldownStartDate(
                        TimeUtils.getLongFromIsoString(val.getDrilldownStartDateAsString(), "GMT+0:00"));

                map.put(szYearAndMonth, val);
            } else {
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
//				if (te1Value == null || te8Value == null) {
//					theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
//					continue;
//				}
                val.addSeriesNetworkElementEntry(ent);
            }

        }
        return sortMapByDate(map);
    }

    public List<SampleNetworkElementsEntry> aggregateDataPerQuarter(List<DbEntryNetworkElements> lstDbEntries) {
        theLog.error("aggregateDataPerQuarter");
        Map<String, SampleNetworkElementsEntry> map = new HashMap<>();
        for (DbEntryNetworkElements dbEntry : lstDbEntries) {
//			String szYearAndMonth = dbEntry.getTE0().substring(0, 7);
            String szYearAndMonthAndDay = dbEntry.getTE0().substring(0, 10);
            String quarter = TimeUtils.getQuarterFromDate(szYearAndMonthAndDay);
            SampleNetworkElementsEntry val = map.get(quarter);
            if (val == null) {
                val = new SampleNetworkElementsEntry();
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);

                // Label must be of type "Quarter 1, 2018"
                val.setLabel(quarter);

                // Date must be of type "01/01/2018", "04/01/2018"
                val.setDate(TimeUtils.generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(szYearAndMonthAndDay));

                // Drill Down End date must be of type "2018-05-31 23:59:59"
                val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfQuarter(szYearAndMonthAndDay));
                // Drill Down Start date must be of type "2018-05-01 00:00:00"
                val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfQuarter(szYearAndMonthAndDay));

                val.setDrilldownEndDate(TimeUtils.getLongFromIsoString(val.getDrilldownEndDateAsString(), "GMT+0:00"));
                val.setDrilldownStartDate(
                        TimeUtils.getLongFromIsoString(val.getDrilldownStartDateAsString(), "GMT+0:00"));

//				theLog.info("aggregateDataPerQuarter NEW PUT quarter=" + quarter + " val=" + val);

                map.put(quarter, val);
            } else {
//				if (te1Value == null || te8Value == null) {
//					theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
//					continue;
//				}
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);
//				theLog.info("aggregateDataPerQuarter NEW PUT quarter=" + quarter + " val=" + val);
            }

        }
        return sortMapByDate(map);
    }

    private List<SampleNetworkElementsEntry> aggregateDataPerYear(List<DbEntryNetworkElements> lstDbEntries) {
        theLog.error("aggregateDataPerYear");
        Map<String, SampleNetworkElementsEntry> map = new HashMap<>();
        for (DbEntryNetworkElements dbEntry : lstDbEntries) {
//			String szYear = dbEntry.getTE0().substring(0, 4);
            String destNameKey = dbEntry.getTE0();
            SampleNetworkElementsEntry val = map.get(destNameKey);
//			SampleNetworkElementsEntry val = new SampleNetworkElementsEntry();
            if (val == null) {
                val = new SampleNetworkElementsEntry();
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);
//				theLog.error("szYear " + szYear + " retransmissionRatio " + retransmissionRatio);

                val.setLabel(destName);

//				val.setDate("01/01/" + szYear);
//				val.setEndDateAsString(TimeUtils.generateIsoDateEndOfYear(szYear));
//				val.setStartDateAsString(TimeUtils.generateIsoDateStartOfYear(szYear));

//				val.setEndDate(TimeUtils.getLongFromIsoString(val.getEndDateAsString(), "GMT+0:00"));
//				val.setStartDate(TimeUtils.getLongFromIsoString(val.getStartDateAsString(), "GMT+0:00"));

                map.put(destName, val);
            } else {
//				if (te1Value == null || te8Value == null) {
//					theLog.error("TE1 " + dbEntry.getTE1() + " TE8 " + dbEntry.getTE8());
//					continue;
//				}
                String destName = dbEntry.getTE0();
                String sourceName = dbEntry.getTE1();
                int requestCount = dbEntry.getTE2();
                int responseCount = dbEntry.getTE3();
                int successResponseCount = dbEntry.getTE4();
                int failedResponseCount = dbEntry.getTE5();
                int timeouts = dbEntry.getTE6();
                int retransmissionCount = dbEntry.getTE7();
                int totalDuration = dbEntry.getTE8();
                int averageCounter = dbEntry.getTE9();
                int responseTime = dbEntry.getTE10();

                NetworkElementsValueEntry ent = new NetworkElementsValueEntry(destName, sourceName, requestCount, responseCount, successResponseCount,
                        failedResponseCount, timeouts, retransmissionCount, totalDuration, averageCounter, responseTime);
                val.addSeriesNetworkElementEntry(ent);

//				theLog.error("szYear " + szYear + " retransmissionRatio " + retransmissionRatio);
            }
        }

        Set<String> keys = map.keySet();
        theLog.error("keys size: " + keys.size());
        for (String destName : keys) {
            SampleNetworkElementsEntry val = map.get(destName);
            theLog.error("destName " + destName + " entries " + val.getValues().size());
        }
        return sortMapByDate(map);
    }

}
