package com.radcom.qinsight.users.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(
                name = User.QUERY_USERS_BY_USERNAME,
                query = "SELECT u FROM User u WHERE u.username = :username"
        ),
        @NamedQuery(
                name = User.QUERY_USERS_BY_ID,
                query = "SELECT u FROM User u WHERE u.id = :id"
        ),
        @NamedQuery(
                name = User.QUERY_DASHBOARDS_IN_USER_BY_ID,
                query = "SELECT DISTINCT d.id FROM User u JOIN u.dashboards d WHERE u.id = :usrId AND d.id = :dashbId"
        )
})

public class User {

    public static final String QUERY_USERS_BY_USERNAME = "QUERY_USERS_BY_USERNAME";
    public static final String QUERY_USERS_BY_ID = "QUERY_USERS_BY_ID";
    public static final String QUERY_DASHBOARDS_IN_USER_BY_ID = "QUERY_DASHBOARDS_IN_USER_BY_ID";


    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String username;

//	@ElementCollection(fetch = FetchType.EAGER)
//	private List<Dashboard> dashboards;

    @OneToMany(fetch = FetchType.EAGER)
    private List<DashboardExt> dashboards;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<DashboardExt> getDashboards() {
        return dashboards;
    }

    public void setDashboards(List<DashboardExt> dashboards) {
        this.dashboards = dashboards;
    }

    public void addDashboard(DashboardExt dashbd) {
        if (dashboards == null)
            dashboards = new ArrayList<>();
        dashboards.add(dashbd);
    }


}
