package com.radcom.qinsight.users.model.accessors;

import com.radcom.qinsight.users.model.DashboardExt;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Stateless
public class ManagerDashboardExts {

    private static final Logger theLog = Logger.getLogger(ManagerDashboardExts.class);

    @PersistenceContext(unitName = "mongo-ogm-users")
    private EntityManager em;

    public String createNew(DashboardExt dashbd) {
        long utcNow = (new Date()).getTime();
        dashbd.setCreatedOn(utcNow);
        dashbd.setLastModified(utcNow);
        em.persist(dashbd);
        System.out.println("DashboardExt New Id: " + dashbd.getId());
        return dashbd.getId();
    }

    @SuppressWarnings("unchecked")
    public List<DashboardExt> getDashboardById(String theId) {
        Query q = em.createNamedQuery(DashboardExt.QUERY_DASHBOARDS_BY_ID);
        q.setParameter("id", theId);
        List<DashboardExt> resLst = q.getResultList();
        System.out.println("getDashboardById, num results = " + resLst.size());
        return resLst;
    }

    public List<DashboardExt> getAll() {
        Query query = em.createQuery("FROM DashboardExt dashbExt");
        List<DashboardExt> list = query.getResultList();
        return list;
    }

    public boolean delete(String id) {
        DashboardExt toDelete = em.find(DashboardExt.class, id);

        if (toDelete == null)
            return false;
        theLog.info("ManagerDashboardExts delete, dashboard.id = " + toDelete.getId());
        em.remove(toDelete);
        return true;
    }

    public void deleteAll() {
        em.createQuery("DELETE FROM " + DashboardExt.class.getName()).executeUpdate();
    }

}
