package com.radcom.qinsight.users.model.accessors;

import com.radcom.qinsight.users.model.DashboardExt;
import com.radcom.qinsight.users.model.User;
import com.radcom.qinsight.users.rest.response.QueryResponseUsers;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class ManagerUsers {
    private static final Logger theLog = Logger.getLogger(ManagerUsers.class);

    @PersistenceContext(unitName = "mongo-ogm-users")
    private EntityManager em;

    public String save(User newUser) {
        List<DashboardExt> lstExistingDashboards = newUser.getDashboards();
        if (lstExistingDashboards == null) {
            lstExistingDashboards = new ArrayList<>();
            newUser.setDashboards(lstExistingDashboards);
        }
        for (DashboardExt dashb : lstExistingDashboards) {
            em.persist(dashb);
        }

        em.persist(newUser);
        theLog.info("Id for newly created user: " + newUser.getId() + " name=" + newUser.getUsername());
        return newUser.getId();
    }


    public QueryResponseUsers getAll() {
        Query query = em.createQuery("FROM User usr");
        List<User> list = query.getResultList();
        QueryResponseUsers resp = new QueryResponseUsers();
        for (User dashTempl : list) {
            resp.addEntry(dashTempl);
        }
        return resp;
    }


    public User getById(String id) {
        return em.find(User.class, id);
    }

    public boolean delete(String id) {
        System.out.println("ManagerUsers delete, id = " + id);
        User templ = em.find(User.class, id);

        if (templ == null)
            return false;
        theLog.info("ManagerUsers delete, user.id = " + templ.getId());
        em.remove(templ);
        return true;
    }

    @SuppressWarnings("unchecked")
    public List<User> getUserByUsername(String theUsername) {
        Query q = em.createNamedQuery(User.QUERY_USERS_BY_USERNAME);
        q.setParameter("username", theUsername);
        List<User> resLst = q.getResultList();
        System.out.println("getUserByUsername, num results = " + resLst.size());
        return resLst;


//    	Query query = new Query();
//    	query.addCriteria(Criteria.where("name").is("Eric"));
//    	List<User> users = mongoTemplate.find(query, User.class);
    }

    @SuppressWarnings("unchecked")
    public List<User> getUserById(String theId) {
        Query q = em.createNamedQuery(User.QUERY_USERS_BY_ID);
        q.setParameter("id", theId);
        List<User> resLst = q.getResultList();
        System.out.println("getUserById, num results = " + resLst.size());
        return resLst;
    }

    public boolean addDashboardsToUser(User usr) {
//    	User usrFromDB	=	em.find( User.class, usr.getId());
//    	if ( usrFromDB == null)
//    		return false;
//    	
//    	List<Dashboard> lstDashboardsToAdd	=	usr.getDashboards();
//    	if ( lstDashboardsToAdd == null)
//    		return false;
//    	if ( lstDashboardsToAdd.size() == 0)
//    		return false;
//    	List<Dashboard> lstExistingDashboards	=	usrFromDB.getDashboards();
//    	if ( lstExistingDashboards == null)		{
//    		lstExistingDashboards	=	new ArrayList<Dashboard>();
//    		usrFromDB.setDashboards(lstExistingDashboards);
//    	}
//    	for ( Dashboard dashb: lstDashboardsToAdd)	{
//    		dashb.setId( UUID.randomUUID().toString());
//    		lstExistingDashboards.add(dashb);
//    	}
        return true;

    }

    public void deleteAll() {
        em.createQuery("DELETE FROM " + User.class.getName()).executeUpdate();
    }


    @SuppressWarnings("unchecked")
    public boolean verifyDashboardBelongsToUser(String usrId, String dashbId) {
        theLog.info("---------------------- verifyDashboardBelongsToUser: usrId=" + usrId + " dashbId=" + dashbId);
        Query q = em.createNamedQuery(User.QUERY_DASHBOARDS_IN_USER_BY_ID);
        q.setParameter("usrId", usrId);
        q.setParameter("dashbId", dashbId);
        List<String> resLst = q.getResultList();
        if (resLst == null) {
            theLog.info("---------------------- verifyDashboardBelongsToUser: resLst=null");
            return false;
        }
        if (resLst.size() < 1) {
            theLog.info("---------------------- verifyDashboardBelongsToUser: resLst size = 0");
            return false;
        }
        theLog.info("---------------------- verifyDashboardBelongsToUser: resLst size: " + resLst.size() + ". Returning true");
        return true;
    }


}
