package com.radcom.qinsight.users.business;

import com.radcom.qinsight.users.model.DashboardExt;
import com.radcom.qinsight.users.model.Dashlet;
import com.radcom.qinsight.users.model.User;
import com.radcom.qinsight.users.model.accessors.ManagerDashboardExts;
import com.radcom.qinsight.users.model.accessors.ManagerUsers;
import com.radcom.qinsight.utils.constants.AppConstants;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.util.*;

@Stateless
public class EjbUsersBusiness {

    @EJB
    private ManagerDashboardExts mgrDashboards;
    @EJB
    private ManagerUsers mgrUsers;

    private static final Logger theLog = Logger.getLogger( EjbUsersBusiness.class);
//	public Response updateLastDisplayedForDashboard( String userId, String dashboardId, BasicDTO dto)
//	{
//		boolean bret	=	verifyDashboardBelongsToUser( userId, dashboardId);
//		if ( bret == false)	{
//			return Response.status(Response.Status.UNAUTHORIZED).build();
//		}
//		List<DashboardExt> lstDashboards	=	mgrDashboards.getDashboardById( dashboardId);
//		if ( lstDashboards == null)
//			return Response.status(Response.Status.NOT_FOUND).build();
//		if ( lstDashboards.size() < 1)
//			return Response.status(Response.Status.NOT_FOUND).build();
//		
//		DashboardExt dashb	=	lstDashboards.get( 0);
//		String theDate	=	(String) dto.getData();
//		long lastDisplayed	=	Long.parseLong(theDate);
//		dashb.setLastDisplayed( new Long(lastDisplayed));
//		return Response.status(Response.Status.OK).build();
//	}

    public Response updateLastDisplayedForDashboard(String userId, String dashboardId) {
        boolean bret = verifyDashboardBelongsToUser(userId, dashboardId);
        if (!bret) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        List<DashboardExt> lstDashboards = mgrDashboards.getDashboardById(dashboardId);
        if (lstDashboards == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        if (lstDashboards.size() < 1)
            return Response.status(Response.Status.NOT_FOUND).build();

        DashboardExt dashb = lstDashboards.get(0);
        long utcTime = (new Date()).getTime();
        dashb.setLastDisplayed(utcTime);

        Map<String, Long> mp = new HashMap<>();
        mp.put("lastDisplayed", utcTime);
        BasicDTO dto = new BasicDTO();
        dto.setData(mp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    private boolean verifyDashboardBelongsToUser(String userId, String dashboardId) {
        User usr = getUserById(userId);
        if (usr == null)
            return false;
        List<DashboardExt> lst = usr.getDashboards();
        for (DashboardExt dashb : lst) {
            if (dashb.getId().equals(dashboardId))
                return true;
        }
        return false;
    }

    public Response getAllDashboardsForUserId(String userId) {
        User usr = getUserById(userId);
        if (usr == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        BasicDTO dto = new BasicDTO();
        dto.setData(usr.getDashboards());
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    public Response getDashboardByIdForUserId(String userId, String dashboardId) {
        User usr = getUserById(userId);
        if (usr == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        List<DashboardExt> lst = usr.getDashboards();
        if (lst == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        for (DashboardExt dashb : lst) {
            if (dashb.getId().equals(dashboardId)) {
                BasicDTO dto = new BasicDTO();
                dto.setData(dashb);
                return Response.status(Response.Status.OK).entity(dto).build();
            }
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }


    public Response getLatestViewedDashboardsForUserId(String userId) {
        User usr = getUserById(userId);
        if (usr == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        List<DashboardExt> dashboards = usr.getDashboards();
        List<DashboardExt> toReturn = new ArrayList<>();
        BasicDTO dto = new BasicDTO();
        dto.setData(toReturn);
        if (dashboards == null) {
            return Response.status(Response.Status.OK).entity(dto).build();
        }
        long nowTime = (new Date()).getTime();
        nowTime -= ((long) 3) * 24 * 3600 * 1000;
        for (DashboardExt dashb : dashboards) {
            if (dashb.getLastDisplayed() == null)
                continue;
            if (dashb.getLastDisplayed() > nowTime) {
                toReturn.add(dashb);
            }
        }
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    private User loadUserByName(String usrName) {
        List<User> lstUsers = mgrUsers.getUserByUsername(usrName);
        if (lstUsers == null)
            return null;
        if (lstUsers.size() < 1)
            return null;
        return lstUsers.get(0);
    }

    public void setupDemoUser() {
        User demoUser = loadUserByName(AppConstants.DEMO_USERNANE);
        if (demoUser != null)
            return;
        demoUser = new User();
        demoUser.setUsername(AppConstants.DEMO_USERNANE);

        mgrUsers.save(demoUser);
    }

    public Response addDashboardToUser(String userId, BasicDTO dto) {
        User usr = mgrUsers.getById(userId);
        if (usr == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        DashboardExt dashbd = (DashboardExt) dto.getData();
        mgrDashboards.createNew(dashbd);
        usr.addDashboard(dashbd);
        return Response.status(Response.Status.OK).build();
    }

    public Response addDashboardToUser(String userId, DashboardExt dashbd) {
        User usr;

        if (AppConstants.DEMO_USER_ID.equals(userId)) {
            List<User> lstUsers = mgrUsers.getUserByUsername(AppConstants.DEMO_USERNANE);
            if (lstUsers == null)
                return Response.status(Response.Status.NOT_FOUND).build();
            if (lstUsers.isEmpty())
                return Response.status(Response.Status.NOT_FOUND).build();
            usr = lstUsers.get(0);
        } else {
            usr = mgrUsers.getById(userId);
            if (usr == null)
                return Response.status(Response.Status.NOT_FOUND).build();
        }

        // add ids to dashlets
        List<Dashlet> lstDashlets = dashbd.getDashlets();
        if (lstDashlets != null) {
            for (Dashlet dshlt : lstDashlets) {
                if (dshlt.getId() == null)
                    dshlt.setId(UUID.randomUUID().toString());
            }
        }
        mgrDashboards.createNew(dashbd);
        usr.addDashboard(dashbd);

        Map<String, String> mp = new HashMap<>();
        mp.put("id", dashbd.getId());
        BasicDTO dto = new BasicDTO();
        dto.setData(mp);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    public Response editDashboardForUser(String userId, DashboardExt dashbd) {
        User usr;
        theLog.info( "====================22222222222222222");
        if (AppConstants.DEMO_USER_ID.equals(userId)) {
            List<User> lstUsers = mgrUsers.getUserByUsername(AppConstants.DEMO_USERNANE);
            if (lstUsers == null)
                return Response.status(Response.Status.NOT_FOUND).build();
            if (lstUsers.isEmpty())
                return Response.status(Response.Status.NOT_FOUND).build();
            usr = lstUsers.get(0);
            theLog.info( "====================3333333333333333");
        } else {
            usr = mgrUsers.getById(userId);
            theLog.info( "====================4444");
            if (usr == null)
                return Response.status(Response.Status.NOT_FOUND).build();
        }
        // add ids to dashlets
        List<Dashlet> lstDashlets = dashbd.getDashlets();
        if (lstDashlets != null) {
            for (Dashlet dshlt : lstDashlets) {
                if (dshlt.getId() == null)
                    dshlt.setId(UUID.randomUUID().toString());
            }
        }
        theLog.info( "====================55555555555555555");

        // check dashboard belongs to user
        List<DashboardExt> lst = usr.getDashboards();
        for (DashboardExt db : lst) {
            if (db.getId().equals(dashbd.getId())) {
                theLog.info( "====================AAAAAAAAAAAAAAAA");
                db.replicate(dashbd);
                theLog.info( "====================BBBBBBBBBBBBBBBBBBBBBB");
                return Response.status(Response.Status.OK).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    public void deleteAllUsers() {
        mgrUsers.deleteAll();
    }

    public void deleteAllDashboards() {
        mgrDashboards.deleteAll();
    }

    private User getUserById(String userId) {
        User usr = null;
        if (AppConstants.DEMO_USER_ID.equals(userId)) {
            List<User> lstUsers = mgrUsers.getUserByUsername(AppConstants.DEMO_USERNANE);
            if (lstUsers == null)
                return null;
            if (lstUsers.isEmpty())
                return null;
            return lstUsers.get(0);
        } else {
            return mgrUsers.getById(userId);
        }
    }

}
