package com.radcom.qinsight.users.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@NamedQueries({
        @NamedQuery(
                name = DashboardExt.QUERY_DASHBOARDS_BY_ID,
                query = "SELECT dashb FROM DashboardExt dashb WHERE dashb.id = :id"
        )
})
public class DashboardExt {

    public static final String QUERY_DASHBOARDS_BY_ID = "QUERY_DASHBOARDS_BY_ID";


    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;


    private String name;
    private Integer numRows;
    private Integer numColumns;

//    @Temporal(TemporalType.TIMESTAMP)
//    private Date lastDisplayed;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date createdOn;

    private Long lastDisplayed;
    private Long lastModified;
    private Long createdOn;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<Dashlet> dashlets;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Integer getNumRows() {
        return numRows;
    }


    public void setNumRows(Integer numRows) {
        this.numRows = numRows;
    }


    public Integer getNumColumns() {
        return numColumns;
    }


    public void setNumColumns(Integer numColumns) {
        this.numColumns = numColumns;
    }


    public List<Dashlet> getDashlets() {
        return dashlets;
    }


    public void setDashlets(List<Dashlet> dashlets) {
        this.dashlets = dashlets;
    }


    public String getId() {
        return id;
    }


    public Long getLastDisplayed() {
        return lastDisplayed;
    }


    public void setLastDisplayed(Long lastDisplayed) {
        this.lastDisplayed = lastDisplayed;
    }


    public Long getCreatedOn() {
        return createdOn;
    }


    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }


    public Long getLastModified() {
        return lastModified;
    }


    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }


    public void setId(String id) {
        this.id = id;
    }


    public void replicate(DashboardExt dashbd) {
        this.name = dashbd.getName();
        this.numRows = dashbd.getNumRows();
        this.numColumns = dashbd.getNumColumns();
        this.lastModified = (new Date()).getTime();
        // this.createdOn is left unchanged.
        // this.lastDisplayed is left unchanged.
        this.dashlets = dashbd.getDashlets();

    }
}
