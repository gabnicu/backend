package com.radcom.qinsight.users.rest;

import com.radcom.qinsight.users.business.EjbUsersBusiness;
import com.radcom.qinsight.users.model.DashboardExt;
import com.radcom.qinsight.users.model.User;
import com.radcom.qinsight.users.model.accessors.ManagerUsers;
import com.radcom.qinsight.users.rest.response.QueryResponseUsers;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;


@Transactional
@Path("users")
public class RestUsers {

    @EJB
    private ManagerUsers mgr;

    @EJB
    private EjbUsersBusiness usrBusiness;

    private static final Logger theLog = Logger.getLogger( RestUsers.class);


    @GET
    @Produces({"application/json"})
    public Response findAll(@QueryParam("name") String name) {
        if (name == null) {
            System.out.println("RestUsers GET name=null");
            return Response.status(Response.Status.OK).entity(mgr.getAll()).build();
        }
        System.out.println("RestUsers GET name=" + name);
        List<User> lst = mgr.getUserByUsername(name);
        if (lst.size() == 0)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.status(Response.Status.OK).entity(lst).build();
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Response findById(@PathParam("id") String id) {

        if (id == null) {
            System.out.println("RestUsers GET id=null");
            QueryResponseUsers resp = mgr.getAll();

            return Response.status(Response.Status.OK).entity(resp).build();

        }
        User usr = mgr.getById(id);
        if (usr == null) {
            System.out.println("----------------------------------NOT FOUND");
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        System.out.println("----------------------------------Has user entity");
        return Response.status(Response.Status.OK).entity(usr).build();
    }

    @GET
    @Path("{id}/dashboards")
    @Produces({"application/json"})
    public Response getAllDashboardsForUser(@PathParam("id") String id) {

        return usrBusiness.getAllDashboardsForUserId(id);
    }

    @GET
    @Path("{id}/dashboards/{dashboardId}")
    @Produces({"application/json"})
    public Response getDashboardByIdForUser(@PathParam("id") String id, @PathParam("dashboardId") String dashboardId) {

        return usrBusiness.getDashboardByIdForUserId(id, dashboardId);
    }

    @GET
    @Path("{id}/dashboards/recent")
    @Produces({"application/json"})
    public Response getLastViewedDashboardsForUser(@PathParam("id") String id) {

        return usrBusiness.getLatestViewedDashboardsForUserId(id);
    }


    @DELETE
    @Path("{id}")
    @Produces({"application/json"})
    public Response removeUser(@PathParam("id") String id) {
        System.out.println("RestUsers removeUser, id = " + id);
        boolean bret = mgr.delete(id);
        if (!bret)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.status(Response.Status.OK).build();

    }

    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addUser(User usr) {
        String id = mgr.save(usr);
        return Response.status(Response.Status.OK).entity(id).build();
    }


    @PUT
    @Path("{id}/dashboards/{dashboardId}")
    @Consumes({"application/json"})
    public Response modify(User usr) {
        boolean bret = mgr.addDashboardsToUser(usr);
        if (!bret)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.status(Response.Status.OK).build();
    }

//    @PUT
//    @Path("{userId}/dashboards/{dashboardId}/lastDisplayed")
//    @Consumes({"application/json"})
//    public Response modify( @PathParam("userId") String userId, @PathParam("dashboardId") String dashboardId, BasicDTO dto)
//    {
//    	return usrBusiness.updateLastDisplayedForDashboard(userId, dashboardId, dto);
//    }

    @POST
    @Path("{userId}/dashboards/{dashboardId}/lastDisplayed")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response modify(@PathParam("userId") String userId, @PathParam("dashboardId") String dashboardId) {
        return usrBusiness.updateLastDisplayedForDashboard(userId, dashboardId);
    }

    @POST
    @Path("{userId}/dashboards")
    @Consumes({"application/json"})
    public Response addDashboardToUser(@PathParam("userId") String userId, DashboardExt dto) {

        return usrBusiness.addDashboardToUser(userId, dto);
    }

    @PUT
    @Path("{userId}/dashboards")
    @Consumes({"application/json"})
    public Response editDashboardForUser(@PathParam("userId") String userId, DashboardExt dto) {
//        try {
//            theLog.info("------- editDashboardForUser dto=" + dto.toString());
//            return usrBusiness.editDashboardForUser(userId, dto);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return Response.status(Response.Status.OK).build();
        theLog.info("------- editDashboardForUser dto=" + dto.toString());

        return usrBusiness.editDashboardForUser(userId, dto);
    }
}
