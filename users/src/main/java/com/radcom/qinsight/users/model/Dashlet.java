package com.radcom.qinsight.users.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class Dashlet {


    @Column(unique = true)
    private String id;

    @Enumerated(EnumType.STRING)
    private DashletType type;

//    @Temporal(TemporalType.TIMESTAMP)
//    private Date startDate;
//    
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date endDate;

    private Long startDate;
    private Long endDate;

    private String networkElements;

    private Integer columnPosition;
    private Integer rowPosition;

    private String procedureType = "All";
    private String procedureSubtype = "All";
    private String vlanId = "All";
    private String applicationId = "All";
    private String transportLayerProtocol = "All";
    private String releaseType = "All";
    private String cause = "All";
    private Boolean isCauseExclusion = true;

    private String source = "All";
    private String destination = "All";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DashletType getType() {
        return type;
    }

    public void setType(DashletType type) {
        this.type = type;
    }

    public String getNetworkElements() {
        return networkElements;
    }

    public void setNetworkElements(String networkElements) {
        this.networkElements = networkElements;
    }

    public Integer getColumnPosition() {
        return columnPosition;
    }

    public void setColumnPosition(Integer columnPosition) {
        this.columnPosition = columnPosition;
    }

    public Integer getRowPosition() {
        return rowPosition;
    }

    public void setRowPosition(Integer rowPosition) {
        this.rowPosition = rowPosition;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public String getProcedureType() {
        return procedureType;
    }

    public void setProcedureType(String procedureType) {
        this.procedureType = procedureType;
    }

    public String getProcedureSubtype() {
        return procedureSubtype;
    }

    public void setProcedureSubtype(String procedureSubtype) {
        this.procedureSubtype = procedureSubtype;
    }

    public String getVlanId() {
        return vlanId;
    }

    public void setVlanId(String vlanId) {
        this.vlanId = vlanId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getTransportLayerProtocol() {
        return transportLayerProtocol;
    }

    public void setTransportLayerProtocol(String transportLayerProtocol) {
        this.transportLayerProtocol = transportLayerProtocol;
    }

    public String getReleaseType() {
        return releaseType;
    }

    public void setReleaseType(String releaseType) {
        this.releaseType = releaseType;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public Boolean getIsCauseExclusion() {
        return isCauseExclusion;
    }

    public void setIsCauseExclusion(Boolean causeExclusion) {
        isCauseExclusion = causeExclusion;
    }


}
