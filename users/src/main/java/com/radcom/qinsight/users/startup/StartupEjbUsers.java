package com.radcom.qinsight.users.startup;

import com.radcom.qinsight.users.business.EjbUsersBusiness;
import com.radcom.qinsight.users.model.DashboardExt;
import com.radcom.qinsight.users.model.Dashlet;
import com.radcom.qinsight.users.model.DashletType;
import com.radcom.qinsight.users.model.User;
import com.radcom.qinsight.users.model.accessors.ManagerUsers;
import com.radcom.qinsight.users.rest.response.QueryResponseUsers;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Startup
@Singleton
public class StartupEjbUsers {

    private static final Logger theLog = Logger.getLogger(StartupEjbUsers.class);

    @EJB
    private ManagerUsers mgr;
    //	@EJB
//	ManagerDashboardExts mgrDashboards;
    @EJB
    private EjbUsersBusiness ejbUsers;

    @PostConstruct
    void init() {
//    	ejbUsers.deleteAllDashboards();
//    	ejbUsers.deleteAllUsers();
        ejbUsers.setupDemoUser();
    }

//    private void addDefaultUsers()
//    {
//    	
//		List<Dashboard> lst = new ArrayList<Dashboard>();
//		Dashboard dsb	=	new Dashboard();
//		dsb.setName("Dashboard 1");
//		dsb.setNumRows(3);
//		dsb.setNumColumns(3);
//		lst.add( dsb);
//		
//		dsb	=	new Dashboard();
//		dsb.setName("Dashboard 2");
//		dsb.setNumRows(2);
//		dsb.setNumColumns(2);
//		lst.add( dsb);
//    	
//		dsb	=	new Dashboard();
//		dsb.setName("Dashboard 3");
//		dsb.setNumRows(4);
//		dsb.setNumColumns(4);
//		lst.add( dsb);
//    	
//    	
//		User dt	=	new User();
//		dt.setUsername("User 1"); 	
//		dt.setDashboards(lst);
//		mgr.save(dt);
//		
//
//		
////		dt	=	new User();
////		dt.setUsername("User 2"); 		
////		mgr.save(dt);
////		
////		dt	=	new User();
////		dt.setUsername("User 3"); 		
////		mgr.save(dt);
////		
////		dt	=	new User();
////		dt.setUsername("User 4"); 		
////		mgr.save(dt);
//		
//
//    }
//    

    private void deleteAllUsers() {
        QueryResponseUsers quResp = mgr.getAll();
        List<User> lstDels = quResp.getData();
        for (User usr : lstDels) {
            mgr.delete(usr.getId());
        }
    }


    private void addUserWithDashboardAndDashlets() {
        List<Dashlet> lstDashlets = new ArrayList<>();
        Dashlet dh = new Dashlet();
        dh.setId(UUID.randomUUID().toString());
        dh.setType(DashletType.DIAMETER_ERROR_CODE_DISTRIBUTION);
        dh.setStartDate(new Date().getTime());
        dh.setEndDate(new Date().getTime());
        dh.setNetworkElements("NE1;NE2;NE3");
        dh.setColumnPosition(0);
        dh.setRowPosition(0);
        lstDashlets.add(dh);

        dh = new Dashlet();
        dh.setId(UUID.randomUUID().toString());
        dh.setType(DashletType.DIAMETER_ERROR_CODE_DISTRIBUTION);
        dh.setStartDate((new Date()).getTime());
        dh.setEndDate(new Date().getTime());
        dh.setNetworkElements("NE7;NE7;NE777");
        dh.setColumnPosition(2);
        dh.setRowPosition(2);
        lstDashlets.add(dh);

        List<DashboardExt> lst = new ArrayList<>();
        DashboardExt dshbrdExt = new DashboardExt();
        dshbrdExt.setName("Dashboard Ext 1");
        dshbrdExt.setNumRows(3);
        dshbrdExt.setNumColumns(3);
        lst.add(dshbrdExt);

        dshbrdExt.setDashlets(lstDashlets);


        User usr = new User();
        usr.setUsername("User Ext 1");
        usr.setDashboards(lst);
        mgr.save(usr);

    }
}
