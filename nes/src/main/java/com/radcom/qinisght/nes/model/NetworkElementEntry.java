package com.radcom.qinisght.nes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NE_CONFIGURATION", schema = "omniq")
public class NetworkElementEntry {

    @Id
    @Column(name = "NE_NAME", columnDefinition = "VARCHAR(50)", unique = true)
    private String neName;

    @Column(name = "NE_TYPE", columnDefinition = "VARCHAR(50)")
    private String neType;

    @Column(name = "TECHNOLOGY", columnDefinition = "VARCHAR(50)")
    private String technology;

    @Column(name = "LOCATION", columnDefinition = "VARCHAR(50)")
    private String location;

    @Column(name = "NE_GROUP", columnDefinition = "VARCHAR(50)")
    private String neGroup;

    @Column(name = "INTERFACE", columnDefinition = "VARCHAR(50)")
    private String interfaceAddr;


    public String getNeName() {
        return neName;
    }

    public void setNeName(String neName) {
        this.neName = neName;
    }

    public String getNeType() {
        return neType;
    }

    public void setNeType(String neType) {
        this.neType = neType;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNeGroup() {
        return neGroup;
    }

    public void setNeGroup(String neGroup) {
        this.neGroup = neGroup;
    }

    public String getInterfaceAddr() {
        return interfaceAddr;
    }

    public void setInterfaceAddr(String interfaceAddr) {
        this.interfaceAddr = interfaceAddr;
    }
}
