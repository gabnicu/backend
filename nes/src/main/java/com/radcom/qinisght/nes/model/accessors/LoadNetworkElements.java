package com.radcom.qinisght.nes.model.accessors;

import com.radcom.qinisght.nes.model.NetworkElementEntry;

import javax.enterprise.context.RequestScoped;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


@RequestScoped
public class LoadNetworkElements {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private String dbUrl;
    private String dbUsername;
    private String dbPassword;

    private void setup() {
        dbUrl = System.getenv("VERTICA_DB_IP");
        if (dbUrl == null)
            dbUrl = "localhost:3306/omniq";

        dbUsername = System.getenv("VERTICA_DB_USERNAME");
        if (dbUsername == null)
            dbUsername = "root";

        dbPassword = System.getenv("VERTICA_DB_PASSWORD");
        if (dbPassword == null)
            dbPassword = "root";
    }

    public List<NetworkElementEntry> loadFromMySQL() {
        setup();
        String connectionURL = "jdbc:mysql://" + dbUrl;

        Connection conn = null;
        Statement stmt = null;
        List<NetworkElementEntry> theList = new ArrayList<>();
        try {
            // STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(connectionURL, dbUsername, dbPassword);

            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM omniq.ne_configuration";
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                NetworkElementEntry nee = new NetworkElementEntry();

                String neName = rs.getString("NE_NAME");
                String neType = rs.getString("NE_TYPE");
                String technology = rs.getString("TECHNOLOGY");
                String location = rs.getString("LOCATION");
                String neGroup = rs.getString("NE_GROUP");
                String interfaceAddr = rs.getString("INTERFACE");

                nee.setNeName(neName);
                nee.setNeType(neType);
                nee.setTechnology(technology);
                nee.setLocation(location);
                nee.setNeGroup(neGroup);
                nee.setInterfaceAddr(interfaceAddr);
                theList.add(nee);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        return theList;
    }


    public List<NetworkElementEntry> load() {
        Properties myProp = new Properties();
        myProp.put("user", "dbadmin");
        myProp.put("password", "dbadmin");
        myProp.put("loginTimeout", "35");
        myProp.put("binaryBatchInsert", "true");
        myProp.put("currentSchema", "omniq");
        Connection conn = null;
        Statement stmt = null;
        List<NetworkElementEntry> theList = new ArrayList<>();
        try {
            conn = DriverManager.getConnection("jdbc:vertica://172.29.17.65:5433/vdb", myProp);

            // STEP 4: Execute a query
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM omniq.ne_configuration";
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                NetworkElementEntry nee = new NetworkElementEntry();

                String neName = rs.getString("NE_NAME");
                String neType = rs.getString("NE_TYPE");
                String technology = rs.getString("TECHNOLOGY");
                String location = rs.getString("LOCATION");
                String neGroup = rs.getString("NE_GROUP");
                String interfaceAddr = rs.getString("INTERFACE");

                nee.setNeName(neName);
                nee.setNeType(neType);
                nee.setTechnology(technology);
                nee.setLocation(location);
                nee.setNeGroup(neGroup);
                nee.setInterfaceAddr(interfaceAddr);
                theList.add(nee);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        return theList;
    }
}
