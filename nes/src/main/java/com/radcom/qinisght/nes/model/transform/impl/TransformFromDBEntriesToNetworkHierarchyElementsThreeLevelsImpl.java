package com.radcom.qinisght.nes.model.transform.impl;

import com.radcom.qinisght.nes.model.NetworkElementEntry;
import com.radcom.qinisght.nes.model.NetworkHierarchyElement;
import com.radcom.qinisght.nes.model.NetworkHierarchyType;
import com.radcom.qinisght.nes.model.transform.TransformFromDBEntriesToNetworkHierarchyElements;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import java.util.List;


@Default
//@ApplicationScoped
@RequestScoped
public class TransformFromDBEntriesToNetworkHierarchyElementsThreeLevelsImpl implements TransformFromDBEntriesToNetworkHierarchyElements {

    @Override
    public NetworkHierarchyElement transformFromDbRepresentation(List<NetworkElementEntry> lst, boolean recursive) {
        if (lst == null)
            return null;

        NetworkHierarchyElement root = new NetworkHierarchyElement();
        root.setNeName(NetworkHierarchyType.ALL.toString());
        root.setNeType(NetworkHierarchyType.ALL);

        for (NetworkElementEntry nee : lst) {
            NetworkHierarchyElement location = getOrCreateNetworkHierarcyElementByNameAndType(root, nee.getLocation(), NetworkHierarchyType.LOCATION);
            if (recursive) {
                NetworkHierarchyElement group = getOrCreateNetworkHierarcyElementByNameAndType(location, nee.getNeGroup(), NetworkHierarchyType.GROUP);
                NetworkHierarchyElement networkElement = getOrCreateNetworkHierarcyElementByNameAndType(group, nee.getNeName(), NetworkHierarchyType.NETWORK_ELEMENT);
            }
        }
        return root;
    }

    @Override
    public List<NetworkHierarchyElement> transformAsListFromDbRepresentation(List<NetworkElementEntry> lst, boolean recursive) {

        NetworkHierarchyElement root = transformFromDbRepresentation(lst, recursive);
        return root.getListOfChildren();
    }

//	@Override
//	public NetworkHierarchyElement transformFromDbRepresentation(List<NetworkElementEntry> lst) {
//		if ( lst == null)
//			return null;
//		
//		NetworkHierarchyElement root	=	new NetworkHierarchyElement();
//		root.setNeName(NetworkHierarchyType.ALL.toString());
//		
//		for ( NetworkElementEntry nee : lst)	{
//			NetworkHierarchyElement location	=	getOrCreateNetworkHierarcyElementByNameAndType( root, nee.getLocation(), NetworkHierarchyType.LOCATION);
//			if ( location == null)	{
//				location	=	createNetworkHierarcyElementByNameAndType( nee.getLocation(), NetworkHierarchyType.LOCATION);
//				root.addChild(location);
//			}
//			NetworkHierarchyElement group	=	getOrCreateNetworkHierarcyElementByNameAndType( location, nee.getNeGroup(), NetworkHierarchyType.GROUP);
//			NetworkHierarchyElement networkElement	=	getOrCreateNetworkHierarcyElementByNameAndType( group, nee.getNeName(), NetworkHierarchyType.NETWORK_ELEMENT);			
//		}		
//		return root;			
//	}

    private NetworkHierarchyElement getOrCreateLocationByName(NetworkHierarchyElement root, String nameOfLocation) {
        for (NetworkHierarchyElement nhe : root.getListOfChildren()) {
            if (nameOfLocation.equals(nhe.getNeName()))
                return nhe;
        }
        NetworkHierarchyElement nhe = new NetworkHierarchyElement();
        nhe.setNeName(nameOfLocation);
        nhe.setNeType(NetworkHierarchyType.LOCATION);
        root.addChild(nhe);
        return nhe;
    }


    private NetworkHierarchyElement getOrCreateNetworkHierarcyElementByNameAndType(NetworkHierarchyElement parent, String nameOfLocation, NetworkHierarchyType theType) {
        if (parent.getListOfChildren() == null) {
            NetworkHierarchyElement nhe = new NetworkHierarchyElement();
            nhe.setNeName(nameOfLocation);
            nhe.setNeType(theType);
            parent.addChild(nhe);
            return nhe;
        }
        for (NetworkHierarchyElement nhe : parent.getListOfChildren()) {
            if (nameOfLocation.equals(nhe.getNeName()))
                return nhe;
        }
        NetworkHierarchyElement nhe = new NetworkHierarchyElement();
        nhe.setNeName(nameOfLocation);
        nhe.setNeType(theType);
        parent.addChild(nhe);
        return nhe;
    }

    private NetworkHierarchyElement getNetworkHierarcyElementByNameAndType(NetworkHierarchyElement parent, String nameOfLocation, NetworkHierarchyType theType) {
        if (parent.getListOfChildren() == null) {
            return null;
        }
        for (NetworkHierarchyElement nhe : parent.getListOfChildren()) {
            if (nameOfLocation.equals(nhe.getNeName()))
                return nhe;
        }
        return null;
    }

    private NetworkHierarchyElement createNetworkHierarcyElementByNameAndType(String nameOfLocation, NetworkHierarchyType theType) {
        NetworkHierarchyElement nhe = new NetworkHierarchyElement();
        nhe.setNeName(nameOfLocation);
        nhe.setNeType(theType);
        return nhe;
    }


}
