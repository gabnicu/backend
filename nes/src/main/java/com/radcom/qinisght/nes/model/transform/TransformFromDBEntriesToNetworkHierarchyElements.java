package com.radcom.qinisght.nes.model.transform;

import com.radcom.qinisght.nes.model.NetworkElementEntry;
import com.radcom.qinisght.nes.model.NetworkHierarchyElement;

import java.util.List;

public interface TransformFromDBEntriesToNetworkHierarchyElements {

    public NetworkHierarchyElement transformFromDbRepresentation(List<NetworkElementEntry> lst, boolean recursive);

    public List<NetworkHierarchyElement> transformAsListFromDbRepresentation(List<NetworkElementEntry> lst, boolean recursive);

}
