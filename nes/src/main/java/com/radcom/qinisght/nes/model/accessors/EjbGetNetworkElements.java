package com.radcom.qinisght.nes.model.accessors;

import com.radcom.qinisght.nes.model.NetworkElementEntry;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class EjbGetNetworkElements {

    private static final Logger theLog = Logger.getLogger(EjbGetNetworkElements.class);

    @Resource(mappedName = "java:jboss/VerticaDS")
    private DataSource ds;

    private static final String SQL_GET_ALL = "SELECT * FROM omniq.ne_configuration";


    public List<NetworkElementEntry> getAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<NetworkElementEntry> theList = new ArrayList<>();
        try {

            conn = ds.getConnection();

            stmt = conn.createStatement();

            rs = stmt.executeQuery(SQL_GET_ALL);

            while (rs.next()) {
                NetworkElementEntry nee = new NetworkElementEntry();

                String neName = rs.getString("NE_NAME");
                String neType = rs.getString("NE_TYPE");
                String technology = rs.getString("TECHNOLOGY");
                String location = rs.getString("LOCATION");
                String neGroup = rs.getString("NE_GROUP");
                String interfaceAddr = rs.getString("INTERFACE");

                nee.setNeName(neName);
                nee.setNeType(neType);
                nee.setTechnology(technology);
                nee.setLocation(location);
                nee.setNeGroup(neGroup);
                nee.setInterfaceAddr(interfaceAddr);
                theList.add(nee);

            }

        } catch (SQLException sqlEx) {
            theLog.error("SQL Exception", sqlEx);
            return null;
        } catch (Exception ex) {
            theLog.error("General Exception", ex);
            return null;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex2) {
                theLog.error("Exception closing connection", ex2);
            }
        }

        theLog.info("Read Network Elements From Verica. Number of network elements is: " + theList.size());
        return theList;

    }
}
