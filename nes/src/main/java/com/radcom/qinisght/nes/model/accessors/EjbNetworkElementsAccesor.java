package com.radcom.qinisght.nes.model.accessors;

import com.radcom.qinisght.nes.model.NetworkElementEntry;
import com.radcom.qinsight.envvars.EnvVarConstants;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Startup
@Singleton
public class EjbNetworkElementsAccesor {

    private static final Logger theLog = Logger.getLogger(EjbNetworkElementsAccesor.class);

    private static final String TIMER_NAME_LOAD_NES_VERTICA = "TIMER_NAME_LOAD_NES_VERTICA";
    private static final String TIMER_NAME_DUMMY = "Created new programmatic timer";

    @EJB
    private EjbGetNetworkElements networkElementsAccessor;

    @Resource
    private TimerService timerService;


    private List<NetworkElementEntry> theList;
    private long timeIntervalForReadingNetworkElements;

    @PostConstruct
    void init() {
        String sz = System.getenv(EnvVarConstants.NES_REPETITON_TIME_SECONDS);
        if (sz == null) {
            timeIntervalForReadingNetworkElements = EnvVarConstants.DEFAULT_NES_REPETITON_TIME_SECONDS * 1000;
        } else {
            timeIntervalForReadingNetworkElements = Long.parseLong(sz) * 1000;
        }
        theList = new ArrayList<>();
        listTimersAndCancelPersisted();
        execJdbcAndRestartTimer();
    }


    public List<NetworkElementEntry> getTheList() {
        List<NetworkElementEntry> copyList;
        synchronized (theList) {
            copyList = new ArrayList<>(theList);
        }
        return copyList;
    }


    private void setTheTimer(long intervalDuration) {
//		theLog.info("Setting a programmatic timeout for " + intervalDuration + " milliseconds from now.");
        Timer timer = timerService.createTimer(intervalDuration, TIMER_NAME_LOAD_NES_VERTICA);
//		theLog.info("timer is persistent " + timer.isPersistent());
    }

    @Timeout
    public void programmaticTimeout(Timer timer) {

        theLog.info("Programmatic timeout occurred. Timer: " + timer.toString() + " --- info " + timer.getInfo());
        timer.cancel();
        execJdbcAndRestartTimer();
    }

    private void execJdbcAndRestartTimer() {
        List<NetworkElementEntry> copyList = networkElementsAccessor.getAll();
        synchronized (theList) {
            theList.clear();
            theList.addAll(copyList);
        }
        setTheTimer(timeIntervalForReadingNetworkElements);
    }

    private void listTimersAndCancelPersisted() {
        Collection<Timer> lstTimers = timerService.getTimers();
        for (Timer tim : lstTimers) {
            if (TIMER_NAME_LOAD_NES_VERTICA.equals(tim.getInfo())) {
                tim.cancel();
                continue;
            }
            if (TIMER_NAME_DUMMY.equals(tim.getInfo())) {
                tim.cancel();
            }
        }
    }

}
