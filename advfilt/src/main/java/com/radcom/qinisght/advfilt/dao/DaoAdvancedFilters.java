package com.radcom.qinisght.advfilt.dao;

import com.radcom.qinisght.advfilt.business.SqlProvider;
import com.radcom.qinisght.advfilt.rest.dto.AdvancedFilterTypes;
import com.radcom.qinisght.advfilt.rest.dto.DtoAdvancedFilter;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.List;


@RequestScoped
public class DaoAdvancedFilters {
    private static final Logger theLog = Logger.getLogger(DaoAdvancedFilters.class);

    @Resource(mappedName = "java:jboss/VerticaDS")
    private DataSource ds;

    @Inject
    private SqlProvider sqlProvider;

    public List<DtoAdvancedFilter> getFilters(AdvancedFilterTypes advancedFilterType) {
        theLog.info("advancedFilterType: " + advancedFilterType);

        String theSQL = null;
        String theParamName = null;
        if (advancedFilterType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_TYPE)) {
            theSQL = sqlProvider.getProperty("DIAMETER_PROCEDURE_TYPE_SQL");
            theParamName = sqlProvider.getProperty("DIAMETER_PROCEDURE_TYPE_PARAM");
        }
        if (advancedFilterType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_SUBTYPE)) {
            theSQL = sqlProvider.getProperty("DIAMETER_PROCEDURE_SUBTYPE_SQL");
            theParamName = sqlProvider.getProperty("DIAMETER_PROCEDURE_SUBTYPE_PARAM");
        }
        if (advancedFilterType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_VLAN_ID)) {
            theSQL = sqlProvider.getProperty("DIAMETER_VLAN_ID_SQL");
            theParamName = sqlProvider.getProperty("DIAMETER_VLAN_ID_PARAM");
        }
        if (advancedFilterType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_APPLICATION_ID)) {
            theSQL = sqlProvider.getProperty("DIAMETER_APPLICATION_ID_SQL");
            theParamName = sqlProvider.getProperty("DIAMETER_APPLICATION_ID_PARAM");
        }
        if (advancedFilterType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_TRANSPORT_LAYER_PROTOCOL)) {
            theSQL = sqlProvider.getProperty("DIAMETER_TRANSPORT_LAYER_PROTOCOL_SQL");
            theParamName = sqlProvider.getProperty("DIAMETER_TRANSPORT_LAYER_PROTOCOL_PARAM");
        }

        if (advancedFilterType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_RELEASE_TYPE)) {
            theSQL = sqlProvider.getProperty("DIAMETER_RELEASE_TYPE_SQL");
            theParamName = sqlProvider.getProperty("DIAMETER_RELEASE_TYPE_PARAM");
        }
        if (advancedFilterType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_CAUSE)) {
            theSQL = sqlProvider.getProperty("DIAMETER_CAUSE_SQL");
            theParamName = sqlProvider.getProperty("DIAMETER_CAUSE_PARAM");
        }

        theLog.info("Executing SQL: " + theSQL + ", theParamName=" + theParamName);
        IExecSql sqlExec = FactorySqlExecutors.getSqlExecutor(advancedFilterType);

        return sqlExec.getAdvancedFilters(ds, theSQL, theParamName);
    }
}
