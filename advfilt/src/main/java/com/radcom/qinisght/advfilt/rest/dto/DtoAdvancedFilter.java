package com.radcom.qinisght.advfilt.rest.dto;

import java.util.List;

public class DtoAdvancedFilter {

    private String id;
    private String name;
    private List<DtoFilterEntry> listOfChildren;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DtoFilterEntry> getListOfChildren() {
        return listOfChildren;
    }

    public void setListOfChildren(List<DtoFilterEntry> listOfChildren) {
        this.listOfChildren = listOfChildren;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
