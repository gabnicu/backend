package com.radcom.qinisght.advfilt.business;

import com.radcom.qinsight.utils.file.FileUtil;
import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import java.util.Properties;

@ApplicationScoped
public class SqlProvider {

    private static final Logger theLog = Logger.getLogger(SqlProvider.class);

    private Properties sqlProperties;


    public String getProperty(String propertyName) {
//        if (sqlProperties == null) {
            sqlProperties = FileUtil.readPropertiesFile("advanced.filter.diameter.properties");
            theLog.info("Reading SQL properties file for Advanced Filters");
//        }
        if (sqlProperties == null)
            return null;
        return sqlProperties.getProperty(propertyName);
    }

}
