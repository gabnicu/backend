package com.radcom.qinisght.advfilt.dao;

import com.radcom.qinisght.advfilt.rest.dto.DtoAdvancedFilter;

import javax.sql.DataSource;
import java.util.List;

public interface IExecSql {
    List<DtoAdvancedFilter> getAdvancedFilters(DataSource ds, String theSQL, String theParamName);
}
