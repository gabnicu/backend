package com.radcom.qinisght.advfilt.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("advfilt")
public interface RestAdvancedFilters {

    @GET
    @Path("procedureType")
    @Produces({"application/json"})
    Response getAllProcedureTypes();

}
