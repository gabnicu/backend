package com.radcom.qinisght.advfilt.rest;

import com.radcom.qinisght.advfilt.dao.DaoAdvancedFilters;
import com.radcom.qinisght.advfilt.rest.dto.AdvancedFilterTypes;
import com.radcom.qinisght.advfilt.rest.dto.DtoAdvancedFilter;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

@Transactional
@Path("advfilt")
public class Rest {
    private static final Logger theLog = Logger.getLogger(Rest.class);

    @Inject
    private DaoAdvancedFilters daoAdvFilt;

    @GET
    @Path("procType")
    @Produces({"application/json"})
    public Response findAll() {
        theLog.info("procType findAll()");
        List<DtoAdvancedFilter> lst = daoAdvFilt.getFilters(AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_TYPE);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("procSubtype")
    @Produces({"application/json"})
    public Response findProcSubtype() {
        theLog.info("findProcSubtype ");
        List<DtoAdvancedFilter> lst = daoAdvFilt.getFilters(AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_SUBTYPE);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("vlanId")
    @Produces({"application/json"})
    public Response findVlanId() {
        List<DtoAdvancedFilter> lst = daoAdvFilt.getFilters(AdvancedFilterTypes.ADV_FILTER_DIAMETER_VLAN_ID);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("applicationId")
    @Produces({"application/json"})
    public Response findApplicationId() {
        List<DtoAdvancedFilter> lst = daoAdvFilt.getFilters(AdvancedFilterTypes.ADV_FILTER_DIAMETER_APPLICATION_ID);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("transportLayerProtocol")
    @Produces({"application/json"})
    public Response findTransportLayerProtocol() {
        List<DtoAdvancedFilter> lst = daoAdvFilt.getFilters(AdvancedFilterTypes.ADV_FILTER_DIAMETER_TRANSPORT_LAYER_PROTOCOL);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("releaseType")
    @Produces({"application/json"})
    public Response findReleaseType() {
        List<DtoAdvancedFilter> lst = daoAdvFilt.getFilters(AdvancedFilterTypes.ADV_FILTER_DIAMETER_RELEASE_TYPE);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("cause")
    @Produces({"application/json"})
    public Response findCause() {
        List<DtoAdvancedFilter> lst = daoAdvFilt.getFilters(AdvancedFilterTypes.ADV_FILTER_DIAMETER_CAUSE);
        BasicDTO dto = new BasicDTO();
        dto.setData(lst);
        return Response.status(Response.Status.OK).entity(dto).build();
    }
}
