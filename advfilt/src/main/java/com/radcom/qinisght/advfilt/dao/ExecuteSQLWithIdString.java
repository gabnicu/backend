package com.radcom.qinisght.advfilt.dao;

import com.radcom.qinisght.advfilt.rest.dto.DtoAdvancedFilter;
import com.radcom.qinisght.advfilt.rest.dto.DtoFilterEntry;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ExecuteSQLWithIdString implements IExecSql {

    private static final Logger theLog = Logger.getLogger(ExecuteSQLWithIdString.class);

    @Override
    public List<DtoAdvancedFilter> getAdvancedFilters(DataSource ds, String theSQL, String theParamName) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        theLog.info("Executing SQL: " + theSQL);
        long index = 1;

        List<DtoAdvancedFilter> advancedFilters = new ArrayList<>();
        DtoAdvancedFilter filter = new DtoAdvancedFilter();
        filter.setName("All");
        filter.setId("All");
        List<DtoFilterEntry> entries = new ArrayList<>();
        try {

            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(theSQL);

            while (rs.next()) {
                DtoFilterEntry filterEntry = new DtoFilterEntry();
                String name = rs.getString(theParamName);
                name = name.replaceAll("DIAMETER: ", "");
//                theLog.info("getAdvancedFilters name after replace: " + name);

                filterEntry.setId(index);
                index++;
                filterEntry.setName(name);
                entries.add(filterEntry);
                filter.setListOfChildren(entries);
            }
            advancedFilters.add(filter);

            theLog.error("=========== Advanced Filters read entries #" + advancedFilters.size());
        } catch (SQLException sqlEx) {
            theLog.error("SQL Exception", sqlEx);
            return null;
        } catch (Exception ex) {
            theLog.error("General Exception", ex);
            return null;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex2) {
                theLog.error("Exception closing connection", ex2);
            }
        }
        theLog.info("Read Advance Filters From Verica. Number of lines read is: " + advancedFilters.size());
        return advancedFilters;
    }
}
