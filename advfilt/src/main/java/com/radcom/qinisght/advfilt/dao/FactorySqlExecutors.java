package com.radcom.qinisght.advfilt.dao;

import com.radcom.qinisght.advfilt.rest.dto.AdvancedFilterTypes;

public class FactorySqlExecutors {

    public static IExecSql getSqlExecutor(AdvancedFilterTypes theType) {
        if (AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_SUBTYPE.equals(theType)) {
            return new ExecuteSQLWithIdString();
        }
        return new ExecuteSQLWithIdInteger();
    }

}
