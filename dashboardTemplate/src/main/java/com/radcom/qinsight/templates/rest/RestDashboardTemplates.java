package com.radcom.qinsight.templates.rest;

import com.radcom.qinsight.templates.model.accessors.ManagerDashboardTemplates;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Stateless
@Path("dashboardTemplates")
public class RestDashboardTemplates {

    @EJB
    private ManagerDashboardTemplates mgr;

    //    @GET
//    @Produces({"application/json"})
//    public QueryResponseDashboardTemplates findAll( ) {
//    	return mgr.getAll();
//    }
    @GET
    @Produces({"application/json"})
    public Response findAll() {
        return Response.status(Response.Status.OK).entity(mgr.getAll()).build();
    }
}
