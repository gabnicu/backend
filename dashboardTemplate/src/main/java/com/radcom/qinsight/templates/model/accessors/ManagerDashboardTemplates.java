package com.radcom.qinsight.templates.model.accessors;

import com.radcom.qinsight.templates.model.DashboardTemplate;
import com.radcom.qinsight.templates.rest.response.QueryResponseDashboardTemplates;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ManagerDashboardTemplates {

    @PersistenceContext(unitName = "mongo-ogm")
    private EntityManager em;

    public void save(DashboardTemplate dt) {
        em.persist(dt);
    }

    public QueryResponseDashboardTemplates getAll() {
        Query query = em.createQuery("FROM DashboardTemplate dashTempl");
        List<DashboardTemplate> list = query.getResultList();
        QueryResponseDashboardTemplates resp = new QueryResponseDashboardTemplates();
        for (DashboardTemplate dashTempl : list) {
            resp.addEntry(dashTempl);
        }
        return resp;
    }

    public void delete(String id) {
        DashboardTemplate templ = em.find(DashboardTemplate.class, id);
        em.remove(templ);
    }

}
