package com.radcom.qinsight.templates.startup;

import com.radcom.qinsight.templates.model.DashboardTemplate;
import com.radcom.qinsight.templates.model.accessors.ManagerDashboardTemplates;
import com.radcom.qinsight.templates.rest.response.QueryResponseDashboardTemplates;
import org.apache.log4j.Logger;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;


@Startup
@Singleton
public class StartupEjbDashboardTemplates {

    private static final Logger theLog = Logger.getLogger(StartupEjbDashboardTemplates.class);

    @EJB
    private ManagerDashboardTemplates mgr;

    @PostConstruct
    void init() {

        QueryResponseDashboardTemplates resp = mgr.getAll();
        if ((resp == null) || (resp.getData() == null) || (resp.getData().size() == 0)) {
            theLog.error("Found zero templates in MongoDB. Adding the needed ones.");
            addDefaultTemplated();
        } else if (resp.getData().size() == 1) {
            theLog.error("Found one templats in MongoDB. Deleting it. Adding the needed ones.");
            String idToDel = resp.getData().get(0).getId();
            mgr.delete(idToDel);
            addDefaultTemplated();
        }
        correction( resp.getData());
    }

    private void addDefaultTemplated() {
        DashboardTemplate dt = new DashboardTemplate();
        dt.setDisplayName("Template 3x3");
        dt.setNumColumns(3);
        dt.setNumRows(3);
        mgr.save(dt);

        dt = new DashboardTemplate();
        dt.setDisplayName("Template 3x4");
        dt.setNumColumns(3);
        dt.setNumRows(4);
        mgr.save(dt);

        dt = new DashboardTemplate();
        dt.setDisplayName("Template 4x4");
        dt.setNumColumns(4);
        dt.setNumRows(4);
        mgr.save(dt);

        dt = new DashboardTemplate();
        dt.setDisplayName("Template 4x5");
        dt.setNumColumns(4);
        dt.setNumRows(5);
        mgr.save(dt);

        dt = new DashboardTemplate();
        dt.setDisplayName("Template 2x20");
        dt.setNumColumns(2);
        dt.setNumRows(20);
        mgr.save(dt);
    }
    
    private void correction( List<DashboardTemplate> lstTemplates)
    {
    	for ( DashboardTemplate dashtmpl : lstTemplates)	{

    		if ( (dashtmpl.getNumColumns() == 4) && ( dashtmpl.getNumRows() == 5))	{
    			String id	=	dashtmpl.getId();
    			mgr.delete(id);
    			DashboardTemplate dt = new DashboardTemplate();
    	        dt.setDisplayName("Template 4x5");
    	        dt.setNumColumns(5);
    	        dt.setNumRows(4);
    	        mgr.save(dt);
    		}
    		if ( (dashtmpl.getNumColumns() == 3) && ( dashtmpl.getNumRows() == 4))	{
    			String id	=	dashtmpl.getId();
    			mgr.delete(id);
    			DashboardTemplate dt = new DashboardTemplate();
    	        dt.setDisplayName("Template 4x5");
    	        dt.setNumColumns(4);
    	        dt.setNumRows(3);
    	        mgr.save(dt);
    		}
    		
    	}
    }
}
