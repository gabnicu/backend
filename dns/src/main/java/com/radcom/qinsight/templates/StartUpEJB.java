package com.radcom.qinsight.templates;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton
public class StartUpEJB {

    @EJB
    PropertyManager ejb;

    @PostConstruct
    void init() {

        Property p = new Property();
        p.setKey("key 2");
        p.setValue("value 2");
        ejb.save(p);
        ejb.queryCache();
    }
}
