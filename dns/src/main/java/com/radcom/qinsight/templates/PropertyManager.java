package com.radcom.qinsight.templates;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class PropertyManager {

    @PersistenceContext(unitName = "mongo-ogm")
    private EntityManager em;


    public void save(Property p) {
        em.persist(p);
    }

    public List<Property> queryCache() {
        Query query = em.createQuery("FROM Property p");

        List<Property> list = query.getResultList();
        return list;
    }
}
