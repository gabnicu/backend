package com.radcom.qinsight.dns;

import java.util.ArrayList;
import java.util.List;

public class DnsQueryResponse {
    private List<DnsEntry> data = null;

    public void addEntry(DnsEntry ent) {
        if (ent == null)
            return;
        if (data == null)
            data = new ArrayList<DnsEntry>();
        data.add(ent);

    }

    public List<DnsEntry> getData() {
        return data;
    }
}
