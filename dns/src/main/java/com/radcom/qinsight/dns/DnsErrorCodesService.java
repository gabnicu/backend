package com.radcom.qinsight.dns;

import com.radcom.qinsight.services.ConsulServices;
import com.radcom.qinsight.services.definitions.registration.ServiceRegistry;
import com.radcom.qinsight.utils.WildFlyUtil;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Startup
@Singleton
public class DnsErrorCodesService {

    @Inject
    @ConsulServices
    ServiceRegistry services;

    @Inject
    WildFlyUtil util;

    private static final String serviceName = "dns/errorCodes";

    @PostConstruct
    public void registerService() {
        services.registerService(serviceName, getEndpoint());
    }

    @PreDestroy
    public void unregisterService() {
        services.unregisterService(serviceName, getEndpoint());
    }

    private String getEndpoint() {
        String endPoint = "http://" + util.getHostName() + ":" + util.getHostPort() + "/dns/resources/dns/errorCodes";
        return endPoint;
//        return "http://" + util.getHostName()+ ":" + util.getHostPort() + "/dns/resources/dns/errorCodes";
    }
}
