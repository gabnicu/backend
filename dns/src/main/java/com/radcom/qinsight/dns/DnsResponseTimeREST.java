package com.radcom.qinsight.dns;

import com.radcom.qinsight.services.ConsulServices;
import com.radcom.qinsight.services.definitions.registration.ServiceRegistry;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;


@Stateless
@Path("dns/responseTime")
public class DnsResponseTimeREST {
//    @PersistenceContext
//    private EntityManager em;

    @Inject
    @ConsulServices
    ServiceRegistry servicesRegistry;

//    @GET
//    @Produces({"application/json"})
//    public List<DnsEntry> findAll( @QueryParam("type") final String type, @QueryParam("year") final int year, @QueryParam("month") final String month, @QueryParam("week") final String week ) {
//
//    	List<DnsEntry> lst	=	new ArrayList<DnsEntry>();
//    	
//    	if ( year == 0)	{
//	    	
//	    	DnsEntry ent	=	new DnsEntry();
//	    	ent.setTheDate("2015");
//	    	ent.setTheValue( 77);
//	    	lst.add( ent);
//	    	
//	    	ent	=	new DnsEntry();
//	    	ent.setTheDate("2016");
//	    	ent.setTheValue( 69);
//	    	lst.add( ent);
//	    	
//	    	ent	=	new DnsEntry();
//	    	ent.setTheDate("2017");
//	    	ent.setTheValue( 73);
//	    	lst.add( ent);
//	    	
//	    	return lst;
//		}
//    	else	{
//    		
//    		if ( month == null)	{
//    	    	
//       	    	DnsEntry ent	=	new DnsEntry();
//    	    	ent.setTheDate("Jan");
//    	    	ent.setTheValue( 58);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Feb");
//    	    	ent.setTheValue( 62);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Mar");
//    	    	ent.setTheValue( 67);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Apr");
//    	    	ent.setTheValue( 75);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("May");
//    	    	ent.setTheValue( 63);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Jun");
//    	    	ent.setTheValue( 49);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Jul");
//    	    	ent.setTheValue( 51);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Aug");
//    	    	ent.setTheValue( 52);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Sep");
//    	    	ent.setTheValue( 39);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Oct");
//    	    	ent.setTheValue( 44);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Nov");
//    	    	ent.setTheValue( 52);
//    	    	lst.add( ent);
//    	    	
//    	    	ent	=	new DnsEntry();
//    	    	ent.setTheDate("Dec");
//    	    	ent.setTheValue( 37);
//    	    	lst.add( ent);
//    	    	
//    	    	return lst;
//    		}
//    		else	{
//            	if ( week == null)	{
//        	    	DnsEntry ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week 1");
//        	    	ent.setTheValue( 44);
//        	    	lst.add( ent);
//        	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week 2");
//        	    	ent.setTheValue( 55);
//        	    	lst.add( ent);
//        	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week 3");
//        	    	ent.setTheValue( 42);
//        	    	lst.add( ent);
//        	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week 4");
//        	    	ent.setTheValue( 52);
//        	    	lst.add( ent);
//        	    	
//        	    	return lst;
//            	}
//            	else	{
//        	    	DnsEntry ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week Day 1");
//        	    	ent.setTheValue( 51);
//        	    	lst.add( ent);
//        	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week Day 2");
//        	    	ent.setTheValue( 49);
//        	    	lst.add( ent);
//        	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week Day 3");
//        	    	ent.setTheValue( 47);
//        	    	lst.add( ent);
//
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week Day 4");
//        	    	ent.setTheValue( 44);
//        	    	lst.add( ent);
//        	    	
//        	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week Day 5");
//        	    	ent.setTheValue( 51);
//        	    	lst.add( ent);
//        	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week Day 6");
//        	    	ent.setTheValue( 48);
//        	    	lst.add( ent);
//       	    	
//        	    	ent	=	new DnsEntry();
//        	    	ent.setTheDate("Week Day 7");
//        	    	ent.setTheValue( 52);
//        	    	lst.add( ent);
//        	    	       	    	
//        	    	return lst;
//            	}
//    		}   		
//
//    	}
//    }

    @GET
    @Produces({"application/json"})
    public DnsQueryResponse findAll(@QueryParam("type") final String type, @QueryParam("year") final int year, @QueryParam("month") final String month, @QueryParam("week") final String week) {

        DnsQueryResponse resp = new DnsQueryResponse();

        if (year == 0) {

            DnsEntry ent = new DnsEntry();
            ent.setTheDate("2015");
            ent.setTheValue(77);
            resp.addEntry(ent);

            ent = new DnsEntry();
            ent.setTheDate("2016");
            ent.setTheValue(69);
            resp.addEntry(ent);

            ent = new DnsEntry();
            ent.setTheDate("2017");
            ent.setTheValue(73);
            resp.addEntry(ent);

            return resp;
        } else {

            if (month == null) {

                DnsEntry ent = new DnsEntry();
                ent.setTheDate("Jan");
                ent.setTheValue(58);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Feb");
                ent.setTheValue(62);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Mar");
                ent.setTheValue(67);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Apr");
                ent.setTheValue(75);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("May");
                ent.setTheValue(63);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Jun");
                ent.setTheValue(49);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Jul");
                ent.setTheValue(51);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Aug");
                ent.setTheValue(52);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Sep");
                ent.setTheValue(39);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Oct");
                ent.setTheValue(44);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Nov");
                ent.setTheValue(52);
                resp.addEntry(ent);

                ent = new DnsEntry();
                ent.setTheDate("Dec");
                ent.setTheValue(37);
                resp.addEntry(ent);

                return resp;
            } else {
                if (week == null) {
                    DnsEntry ent = new DnsEntry();
                    ent.setTheDate("Week 1");
                    ent.setTheValue(44);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week 2");
                    ent.setTheValue(55);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week 3");
                    ent.setTheValue(42);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week 4");
                    ent.setTheValue(52);
                    resp.addEntry(ent);

                    return resp;
                } else {
                    DnsEntry ent = new DnsEntry();
                    ent.setTheDate("Week Day 1");
                    ent.setTheValue(51);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 2");
                    ent.setTheValue(49);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 3");
                    ent.setTheValue(47);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 4");
                    ent.setTheValue(44);
                    resp.addEntry(ent);


                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 5");
                    ent.setTheValue(51);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 6");
                    ent.setTheValue(48);
                    resp.addEntry(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 7");
                    ent.setTheValue(52);
                    resp.addEntry(ent);

                    return resp;
                }
            }

        }
    }
}
