package com.radcom.qinsight.dns;

import com.radcom.qinsight.services.ConsulServices;
import com.radcom.qinsight.services.definitions.registration.ServiceRegistry;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.ArrayList;
import java.util.List;


@Stateless
@Path("dns/errorCodes")
public class DnsErrorCodesREST {

//    @PersistenceContext
//    private EntityManager em;

    @Inject
    @ConsulServices
    ServiceRegistry servicesRegistry;

    @GET
    @Produces({"application/json"})
    public List<DnsEntry> findAll(@QueryParam("year") final int year, @QueryParam("month") final String month, @QueryParam("week") final String week) {

        List<DnsEntry> lst = new ArrayList<>();

        if (year == 0) {

            DnsEntry ent = new DnsEntry();
            ent.setTheDate("2015");
            ent.setTheValue(77000000);
            lst.add(ent);

            ent = new DnsEntry();
            ent.setTheDate("2016");
            ent.setTheValue(78000000);
            lst.add(ent);

            ent = new DnsEntry();
            ent.setTheDate("2017");
            ent.setTheValue(79000000);
            lst.add(ent);

            return lst;
        } else {

            if (month == null) {

                DnsEntry ent = new DnsEntry();
                ent.setTheDate("Jan");
                ent.setTheValue(7000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Feb");
                ent.setTheValue(8000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Mar");
                ent.setTheValue(9000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Apr");
                ent.setTheValue(1000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("May");
                ent.setTheValue(2000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Jun");
                ent.setTheValue(3000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Jul");
                ent.setTheValue(4000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Aug");
                ent.setTheValue(5000000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Sep");
                ent.setTheValue(5200000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Oct");
                ent.setTheValue(5400000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Nov");
                ent.setTheValue(5700000);
                lst.add(ent);

                ent = new DnsEntry();
                ent.setTheDate("Dec");
                ent.setTheValue(5900000);
                lst.add(ent);

                return lst;
            } else {
                if (week == null) {
                    DnsEntry ent = new DnsEntry();
                    ent.setTheDate("Week 1");
                    ent.setTheValue(27000000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week 2");
                    ent.setTheValue(28000000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week 3");
                    ent.setTheValue(29000000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week 4");
                    ent.setTheValue(24000000);
                    lst.add(ent);

                    return lst;
                } else {
                    DnsEntry ent = new DnsEntry();
                    ent.setTheDate("Week Day 1");
                    ent.setTheValue(270000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 2");
                    ent.setTheValue(210000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 3");
                    ent.setTheValue(230000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 4");
                    ent.setTheValue(240000);
                    lst.add(ent);


                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 5");
                    ent.setTheValue(250000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 6");
                    ent.setTheValue(260000);
                    lst.add(ent);

                    ent = new DnsEntry();
                    ent.setTheDate("Week Day 7");
                    ent.setTheValue(270000);
                    lst.add(ent);

                    return lst;
                }
            }
        }
    }
}
