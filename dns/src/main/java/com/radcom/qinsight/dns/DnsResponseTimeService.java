package com.radcom.qinsight.dns;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.registration.ServiceRegistry;
import com.radcom.qinsight.utils.WildFlyUtil;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Startup
@Singleton
public class DnsResponseTimeService {

    @Inject
    @EnvVarServices
    private ServiceRegistry services;

    @Inject
    private WildFlyUtil util;

//    @Inject 
//    @AnnotationRuntime
//    @ApplicationScoped
//    Logger theLog;

    private static final Logger theLog = Logger.getLogger(DnsResponseTimeService.class);
    private static final String serviceName = "dns/responseTime";

    @PostConstruct
    public void registerService() {
        services.registerService(serviceName, getEndpoint());
    }

    @PreDestroy
    public void unregisterService() {
        services.unregisterService(serviceName, getEndpoint());
    }

    private String getEndpoint() {
        String endPoint = "http://" + util.getHostName() + ":" + util.getHostPort() + "/dns/resources/dns/responseTime";
        return endPoint;
//        return "http://" + util.getHostName()+ ":" + util.getHostPort() + "/dns/resources/dns/errorCodes";
    }
}
