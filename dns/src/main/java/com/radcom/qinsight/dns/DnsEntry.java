package com.radcom.qinsight.dns;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DnsEntry {

    private String theDate = null;
    private double theValue = 0.0;

    public String getTheDate() {
        return theDate;
    }

    public void setTheDate(String theDate) {
        this.theDate = theDate;
    }

    public double getTheValue() {
        return theValue;
    }

    public void setTheValue(double theValue) {
        this.theValue = theValue;
    }

}
