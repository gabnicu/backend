package com.radcom.qinsight.dns;

import javax.ws.rs.core.Application;

/**
 * @author arungupta
 */
//@EnableSnoopClient
@javax.ws.rs.ApplicationPath("resources")
public class ApplicationConfig extends Application {
}
